//
//  RatingView.swift
//
//  Created by Peter Prokop on 18/10/15.
//  Copyright © 2015 Peter Prokop. All rights reserved.
//

import UIKit

@objc public protocol RatingViewDelegate {
    /**
     Called when user's touch ends
     
     - parameter ratingView: Rating view, which calls this method
     - parameter didChangeRating newRating: New rating
    */
    func ratingView(ratingView: RatingView, didChangeRating newRating: Float)
}

/**
 Rating bar, fully customisable from Interface builder
*/
@IBDesignable
public class RatingView: UIView {
   
    /// Total number of stars
    @IBInspectable public var starCount: Int = 5
    
    /// Image of unlit star, if nil "starryStars_off" is used
    @IBInspectable public var offImage: UIImage?
    
    /// Image of fully lit star, if nil "starryStars_on" is used
    @IBInspectable public var onImage: UIImage?
    
    /// Image of half-lit star, if nil "starryStars_half" is used
    @IBInspectable public var halfImage: UIImage?
    
    /// Current rating, updates star images after setting
    @IBInspectable public var rating: Float = Float(0) {
        didSet {
            // If rating is more than starCount simply set it to starCount
            rating = min(Float(starCount), rating)
            
            updateRating()
        }
    }
    
    /// If set to "false" only full stars will be lit
    @IBInspectable public var halfStarsAllowed: Bool = false
    
    /// If set to "false" user will not be able to edit the rating
    @IBInspectable public var editable: Bool = true
    
    
    /// Delegate, must confrom to *RatingViewDelegate* protocol
    public weak var delegate: RatingViewDelegate?
    
    var stars = [UIImageView]()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        customInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        customInit()
    }
    
    override public func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        customInit()
    }
    
    func customInit() {
        let bundle = Bundle(for: RatingView.self)

        if offImage == nil {
            offImage = UIImage(named: "starryStars_off", in: bundle, compatibleWith: self.traitCollection)
            offImage = resizeImage(image: offImage!, newWidth: (offImage?.size.width)! * 0.75)
        }
        if onImage == nil {
            onImage = UIImage(named: "starryStars_on", in: bundle, compatibleWith: self.traitCollection)
            onImage = resizeImage(image: onImage!, newWidth: (onImage?.size.width)! * 0.75)
        }
        if halfImage == nil {
            halfImage = UIImage(named: "starryStars_half", in: bundle, compatibleWith: self.traitCollection)
            halfImage = resizeImage(image: halfImage!, newWidth: (halfImage?.size.width)! * 0.75)
        }
        
        guard let offImage = offImage else {
            assert(false, "offImage is not set")
            return
        }
        
        for _ in 1...starCount {
            let iv = UIImageView(image: offImage)
            addSubview(iv)
            stars.append(iv)
        }
        
        layoutStars()
        updateRating()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        layoutStars()
    }
    
    func layoutStars() {
        if stars.count != 0,
            let offImage = stars.first?.image {
                let halfWidth = offImage.size.width/2
                let distance = (bounds.size.width - (offImage.size.width * CGFloat(starCount))) / CGFloat(starCount + 1) + halfWidth
                
                var i = 1
                for iv in stars {
                    iv.frame = CGRect(x: 0, y: 0, width: offImage.size.width, height: offImage.size.height)
                    
                    iv.center = CGPoint(x: CGFloat(i) * distance + halfWidth * CGFloat(i - 1), y: self.frame.size.height/2)
                    i = i + 1
                }
        }
    }
    
    /**
     Compute and adjust rating when user touches begin/move/end
    */
    func handleTouches(touches: Set<UITouch>) {
        let touch = touches.first!
        let touchLocation = touch.location(in: self)
        
        var i = starCount - 1
        print("handleTouches", touchLocation)
        while i >= 0 {
            let imageView = stars[i]
            
            let x = touchLocation.x
            print(x, imageView.center)
            
            if x >= imageView.center.x {
                rating = Float(i) + 1
                print("1", rating)
                return
            } else if x >= imageView.frame.minX && halfStarsAllowed {
                rating = Float(i) + 0.5
                print("2", rating)
                return
            }
            i = i - 1
        }
        rating = 0
        print("3", rating)
    }

    /**
     Adjust images on image views to represent new rating
     */
    func updateRating() {
        // To avoid crash when using IB
        print("updateRating", rating)
        if stars.count == 0 {
            return
        }
        
        // Set every full star
        var i = 1
        if Int(rating) >= i {
            for _ in 1...Int(rating) {
                let star = stars[i-1]
                star.image = onImage
                i = i + 1
            }
        }
        print(i, starCount)
        if i > starCount {
            return
        }
/*
        print(rating - Float(i) + 1)
        // Now add a half star
        if rating - Float(i) + 1 >= 0.5 {
            let star = stars[i-1]
            star.image = halfImage
            i = i + 1
        }
*/
        
        for _ in i...starCount {
            let star = stars[i-1]
            star.image = offImage
            i = i + 1
        }
    }
}

func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {

    let scale = newWidth / image.size.width
    let newHeight = image.size.height * scale
    UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
    image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
    
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage
}

// MARK: Override UIResponder methods

extension RatingView {
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard editable else { return }
        handleTouches(touches: touches)
    }
    
    override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard editable else { return }
        handleTouches(touches: touches)
    }
    
    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard editable else { return }
        handleTouches(touches: touches)
        guard let delegate = delegate else { return }
        delegate.ratingView(ratingView: self, didChangeRating: rating)
    }
}
