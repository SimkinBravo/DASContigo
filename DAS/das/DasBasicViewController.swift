//
//  DasBasicViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 02/02/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit

class DasBasicViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func setupUnloggedUserScreen(container: UIView) {
        let image = UIImage(named: "no-membership.png")
        let imageVw = UIImageView(image: image)
        imageVw.center = container.center
        imageVw.frame = CGRect(origin: CGPoint(x: imageVw.frame.origin.x, y: imageVw.frame.origin.y - 90), size: imageVw.frame.size)
        container.addSubview(imageVw)
        let indent:CGFloat = 30.0
        let text = UILabel(frame: CGRect(x: indent, y: imageVw.frame.origin.y + imageVw.frame.size.height + 10, width: container.frame.size.width - (indent * 2), height: 100))
        text.text = "Inicia sesión para acceder a esta sección"
        text.numberOfLines = 0
        text.lineBreakMode = .byWordWrapping
        text.textAlignment = .center
        text.textColor = Theme.shared.colors.blueButton
        container.addSubview(text)
        let loginButton = UIButton(type: .custom)
        loginButton.tintColor = .black
        loginButton.frame = CGRect(x: 15, y: text.frame.origin.y + text.frame.size.height + 10, width: self.view.frame.size.width - 30, height: 45)
        loginButton.setTitle("Iniciar sesión", for: .normal)
        loginButton.backgroundColor = Theme.shared.colors.blueButton
        loginButton.layer.cornerRadius = 8.0
        loginButton.clipsToBounds = true
        loginButton.addTarget(self, action: #selector(unloginPressed), for: .touchUpInside)
        container.addSubview(loginButton)

        let contactButton = UIButton(type: .custom)
        contactButton.tintColor = .black
        contactButton.frame = CGRect(x: 15, y: loginButton.frame.origin.y + loginButton.frame.size.height + 20, width: self.view.frame.size.width - 30, height: 45)
        contactButton.setTitle("Contáctanos", for: .normal)
        contactButton.backgroundColor = .white
        contactButton.setTitleColor(Theme.shared.colors.blueButton, for: .normal)
        contactButton.layer.cornerRadius = 8.0
        contactButton.layer.borderColor = Theme.shared.colors.blueButton.cgColor
        contactButton.layer.borderWidth = 1.0
        contactButton.clipsToBounds = true
        contactButton.addTarget(self, action: #selector(contactUsPressed), for: .touchUpInside)
        container.addSubview(contactButton)
    }

    @objc func contactUsPressed() {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        self.navigationController?.pushViewController(vc, animated:true)
    }

    @objc func unloginPressed() {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController")
        self.tabBarController?.navigationController?.setViewControllers([vc], animated: true)
    }

    func addSeparator(margin: CGPoint = CGPoint(x: 0.0, y: 0.0), at: UIView! = nil,  height: CGFloat = 1.0) {
        let separator = UIView.init(frame: CGRect(x: margin.x, y: margin.y, width: self.view.frame.size.width - (margin.x * 2), height: height))
        separator.backgroundColor = Theme.shared.colors.cellSeparatorColor
        if at != nil {
            at.addSubview(separator)
        } else {
            self.view.addSubview(separator)
        }
    }
    
    func addLabelWithTag(tag:Int! = 0, font:UIFont, textColor:UIColor, container:UIView, multiline:Bool = false) -> UILabel {
        let label = UILabel.init()
        if tag != 0 {
            container.viewWithTag(tag)?.removeFromSuperview()
            label.tag = tag
        }
        label.font = font
        label.textColor = textColor
        if multiline {
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping
        }
        container.addSubview(label)
        return label
    }

}
