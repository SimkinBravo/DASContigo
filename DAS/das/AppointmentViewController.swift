//
//  AppointmentViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 16/02/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import DropDown
import MapKit
import DatePickerDialog
import UserNotifications

class AppointmentViewController: DasBasicViewController, UITextFieldDelegate, CLLocationManagerDelegate {

    let formatter = DateFormatter()

    let serviceName = UITextField()
    let place = UITextField()
    var startAtA: UILabel!
    var endAtA: UILabel!
    var alertA: UILabel!
    var placeAt: UILabel!
    var activityIndicator: NVActivityIndicatorView!
    let dropDown = DropDown()
    var searchResults: [BasicCellData]!
    var selectedProvider: BasicCellData!
    var userLocationState: String!
    var userLocationRegion: String!
    var searchState: String!
    var selected: UILabel!
    let notificationOptions = ["Sin notificación", "1 hora antes", "2 horas antes", "1 día antes"]
    var notification: Int = 0

    private var locationManager: CLLocationManager = CLLocationManager()
    private var currentLocation: CLLocation?

    override func viewDidLoad() {
        super.viewDidLoad()
        formatter.dateFormat = "dd/MM/yyyy HH:mm"

        var colors = [UIColor]()
        colors.append(Theme.shared.colors.navigationBarColor1)
        colors.append(Theme.shared.colors.navigationBarColor2)
        colors.append(Theme.shared.colors.navigationBarColor3)
        navigationController?.navigationBar.setGradientBackground(colors: colors)

        self.title = "Nuevo Evento"
        self.navigationController?.navigationBar.tintColor = Theme.shared.colors.blueButton
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Agregar", style: .plain, target: self, action: #selector(addButtonAction))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(cancelButtonAction))
        self.navigationItem.rightBarButtonItem?.isEnabled = false

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        serviceName.frame = CGRect(x: 15, y: (self.navigationController?.navigationBar.frame.height)! + 25, width: self.view.frame.size.width - 30, height: 45)
        serviceName.placeholder = "Nombre de médico o servicio"
        serviceName.font = UIFont.systemFont(ofSize: 15)
        serviceName.borderStyle = .none
        serviceName.keyboardType = .alphabet
        serviceName.clearButtonMode = .whileEditing
        serviceName.contentVerticalAlignment = .center
        serviceName.delegate = self
        serviceName.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        view.addSubview(serviceName)
        addSeparator(margin: CGPoint(x: 0.0, y: serviceName.frame.origin.y + serviceName.frame.height + 5), at: view)

        placeAt = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        placeAt.numberOfLines = 0
        placeAt.lineBreakMode = .byWordWrapping
        placeAt.frame = CGRect(x: 15.0, y: serviceName.frame.origin.y + serviceName.frame.height + 10, width: view.frame.size.width - 30, height: 45)
        placeAt.text = "Lugar"
/*
        place.frame = CGRect(x: 15, y: serviceName.frame.origin.y + serviceName.frame.height + 10, width: self.view.frame.size.width - 30, height: 45)
        place.placeholder = "Lugar"
        place.font = UIFont.systemFont(ofSize: 15)
        place.borderStyle = .none
        place.keyboardType = .alphabet
        place.clearButtonMode = .whileEditing
        place.contentVerticalAlignment = .center
        place.delegate = self
        place.isEnabled = false
        place.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        view.addSubview(place)
*/
        addSeparator(margin: CGPoint(x: 0.0, y: placeAt.frame.origin.y + placeAt.frame.height + 5), at: view)

        let startAt = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        startAt.frame = CGRect(x: 15.0, y: placeAt.frame.origin.y + placeAt.frame.height + 5, width: view.frame.size.width - 30, height: 50)
        startAt.text = "Inicia"
        startAtA = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: Theme.shared.colors.textGray, container: view)
        startAtA.frame = CGRect(x: 85.0, y: startAt.frame.origin.y, width: view.frame.size.width - 100, height: 50)
        startAtA.textAlignment = .right
        startAtA.text = "Elegir Hora"

        let startAtButton = UIButton(type: .custom)
        startAtButton.frame = CGRect(x: 15, y: startAt.frame.origin.y, width: self.view.frame.size.width - 30, height: 50)
        startAtButton.clipsToBounds = true
        startAtButton.addTarget(self, action: #selector(startAtButtonAction), for: .touchUpInside)
        view.addSubview(startAtButton)

        addSeparator(margin: CGPoint(x: 0.0, y: startAt.frame.origin.y + startAt.frame.height), at: view)

        let endAt = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        endAt.frame = CGRect(x: 15.0, y: startAt.frame.origin.y + startAt.frame.height, width: view.frame.size.width - 30, height: 50)
        endAt.text = "Termina"
        endAtA = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: Theme.shared.colors.textGray, container: view)
        endAtA.frame = CGRect(x: 50.0, y: endAt.frame.origin.y, width: view.frame.size.width - 65, height: 50)
        endAtA.textAlignment = .right
        endAtA.text = "Elegir Hora"

        let endAtButton = UIButton(type: .custom)
        endAtButton.frame = CGRect(x: 15, y: endAt.frame.origin.y, width: self.view.frame.size.width - 30, height: 50)
        endAtButton.clipsToBounds = true
        endAtButton.addTarget(self, action: #selector(endAtButtonAction), for: .touchUpInside)
        view.addSubview(endAtButton)

        addSeparator(margin: CGPoint(x: 0.0, y: endAt.frame.origin.y + endAt.frame.height), at: view)

        let alert = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        alert.frame = CGRect(x: 15.0, y: endAt.frame.origin.y + endAt.frame.height, width: view.frame.size.width - 30, height: 50)
        alert.text = "Notificación"
        alertA = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: Theme.shared.colors.textGray, container: view)
        alertA.frame = CGRect(x: 50.0, y: alert.frame.origin.y, width: view.frame.size.width - 65, height: 50)
        alertA.textAlignment = .right
        alertA.text = notificationOptions[notification]

        let alertButton = UIButton(type: .custom)
        alertButton.frame = CGRect(x: 15, y: alert.frame.origin.y, width: self.view.frame.size.width - 30, height: 50)
        alertButton.clipsToBounds = true
        alertButton.addTarget(self, action: #selector(alertButtonAction), for: .touchUpInside)
        view.addSubview(alertButton)
        
        addSeparator(margin: CGPoint(x: 0.0, y: endAt.frame.origin.y + endAt.frame.height), at: view)

        setupActivityIndicator()

        dropDown.anchorView = placeAt
        view.addSubview(dropDown)

        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectedProvider = self.searchResults[index]
            self.serviceName.text = self.selectedProvider.title
            self.placeAt.text = self.selectedProvider.author
        }

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest

        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }

    }

    @objc func alertButtonAction() {
        let actionSheetController: UIAlertController = UIAlertController(title: "Configuración de notificación", message: "Selecciona una opción", preferredStyle: .actionSheet)
        for (index, option) in notificationOptions.enumerated() {
            let actionButton = UIAlertAction(title: option, style: .default) { _ in
                self.alertA.text = option
                self.notification = index
            }
            actionSheetController.addAction(actionButton)
        }
        present(actionSheetController, animated: true, completion: nil)
    }

    func addAppointment() {
        if selectedProvider == nil {
            let refreshAlert = UIAlertController(title: "Error", message: "Se debe elegir un proveedor válido.", preferredStyle: .alert)
            refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            serviceName.text = ""
            present(refreshAlert, animated: true, completion: nil)
            return
        }
        print("\(Urls.base)citaNueva")
        let formParams = ["idAfiliacion": Theme.shared.user.affiliateNumber!, "id_proveedor": selectedProvider.slug, "hora_inicio": startAtA.text!, "hora_final": endAtA.text!, "calificacion": "0", "autoevaluacion" : "0", "recomendarias": "0", "comentarios": "", "comoSeEntero": []] as [String : Any]
        print(formParams)
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false

        let ulr =  NSURL(string:"\(Urls.base)citaNueva" as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let data = try! JSONSerialization.data(withJSONObject: formParams, options: JSONSerialization.WritingOptions.prettyPrinted)

        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        
        Alamofire.request(request).responseJSON { response in
            if response.result.value != nil {
                print(response.result.value)
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                if self.notification == 0 {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.registerUserNotificationSettings()
                }
            } else {
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)
            }
        }

    }

    @objc func startAtButtonAction() {
        selected = startAtA
        datePickerTapped()
    }

    @objc func endAtButtonAction() {
        selected = endAtA
        datePickerTapped()
    }

    @objc func addButtonAction() {
        addAppointment()
    }

    func datePickerTapped() {
        let defaultDate = formatter.date(from: selected.text!) ?? Date()
        DatePickerDialog(locale: Locale(identifier: "es_MX")).show("DatePicker", doneButtonTitle: "Aceptar", cancelButtonTitle: "Cancelar", defaultDate: defaultDate, datePickerMode: .dateAndTime) {
            (date) -> Void in
            if let dt = date {
                self.selected.text = self.formatter.string(from: dt)
                if self.selected == self.startAtA {
                    self.endAtA.text = self.formatter.string(from: dt.addingTimeInterval(60 * 60))
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                }
            }
        }
    }

    @objc func registerUserNotificationSettings() {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            print(granted, error)
            self.addLocalNotification()
        }
    }

    func addLocalNotification() {

        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: "Cita con: \(serviceName.text!)", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "En: \(placeAt.text!)", arguments: nil)
        let unitFlags = Set<Calendar.Component>([.minute, .hour, .day, .month, .year])
        let components = NSCalendar.current.dateComponents(unitFlags, from: (formatter.date(from: startAtA.text!)?.addingTimeInterval(getTimeIntervalForNotification()))!)
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
        let request = UNNotificationRequest(identifier: "appointment\(selectedProvider.id)", content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error : Error?) in
            if let theError = error {
                print(theError.localizedDescription)
            }
            self.dismiss(animated: true, completion: nil)
        }
    }

    func getTimeIntervalForNotification() -> TimeInterval {
        if notification == 1 {
            return -60 * 60
        } else if notification == 2 {
            return -60 * 60 * 2
        } else if notification == 3 {
            return -60 * 60 * 24
        }
        return 0
    }

    @objc func cancelButtonAction() {
        dismiss(animated: true, completion: nil)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            print("lala")
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            print("lala")
        }
    }

    var isSearching: Bool = false
    var pendantText: String!

    @objc func textFieldDidChange(textField: UITextField) {
        if let text = textField.text, text.count > 4 {
            if !isSearching {
                var formParams = ["keyword": textField.text!]
                if searchState != nil {
                    formParams["stateId"] = searchState
                }
                postSearch(formParams: formParams)
            } else {
                pendantText = textField.text
            }
        }
/*
        let textIsEmpty = textField.text?.isEmpty
        searchButton.alpha = textIsEmpty! ? 0.6 : 1.0
        searchButton.isEnabled = !textIsEmpty!
*/
    }

    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }

    func setupActivityIndicator() {
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 60, height: 60), type: NVActivityIndicatorType.ballClipRotate, color: Theme.shared.colors.navigationBarColor3, padding: 0)
        activityIndicator.center = view.center
        activityIndicator.frame = CGRect(x: activityIndicator.frame.origin.x, y: activityIndicator.frame.origin.y + 100, width: activityIndicator.frame.width, height: activityIndicator.frame.height)
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
    }

    func postSearch(formParams: [String: String]) {
        print(formParams)
        Alamofire.request("\(Urls.base)search", method: .post, parameters: formParams, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.value != nil {
                let rawResponse = response.result.value as! [String: AnyObject]
                let content = rawResponse["content"]
                self.searchResults = []
                var ddContent: [String] = []
                for rawItem in content as! Array<Any> {
                    self.searchResults.append(BasicCellData(index: self.searchResults.count, searchRawItem: rawItem as! [String : AnyObject]))
                    ddContent.append((self.searchResults.last?.title)!)
                }
                self.dropDown.dataSource = ddContent
                self.dropDown.show()
            } else {
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)
            }
        }
    }

    func getAdressName(coords: CLLocation) {
        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
            if error != nil {
                print("Hay un error")
            } else {
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    var adressString : String = ""
                    if place.locality != nil {
                        self.userLocationState = place.locality?.folding(options: .diacriticInsensitive, locale: .current).lowercased()
                        adressString = adressString + place.locality! + " - "
                    }
                    if place.subAdministrativeArea != nil {
                        self.userLocationRegion = place.subAdministrativeArea
                        adressString = adressString + place.subAdministrativeArea!
                    }
                    self.postCatalog()
                }
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        if currentLocation == nil {
            if let userLocation = locations.last {
                getAdressName(coords: userLocation)
            }
        }
    }

    
    func postCatalog() {
        print("\(Urls.base)catalog")
        Alamofire.request("\(Urls.base)catalog", method: .post, parameters: ["catalog": "states"], encoding: JSONEncoding.default).responseJSON { response in
            if response.result.value != nil {
                let rawResponse = response.result.value as! [String: AnyObject]
                let parsed = self.parseCatalogRawResponse(raw: rawResponse)
                for row in parsed {
                    if self.userLocationState == row.title.lowercased() {
                        self.searchState = row.slug
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.isHidden = true
                        return
                    }
                }
            }
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }
    }

    func parseCatalogRawResponse(raw: [String: AnyObject])-> [BasicCellData] {
        var parsed = [BasicCellData]()
        for key in raw.keys {
            parsed.append(BasicCellData(slug: key, title: raw[key] as! String, subtitle: "").configCatalogCell())
        }
        return parsed
    }

}

