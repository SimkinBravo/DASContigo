//
//  AccidentInsuranceViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 15/02/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit

class AccidentInsuranceViewController: DasBasicViewController {

    let scroll: UIScrollView = UIScrollView()
    var nextY: CGFloat = 0.0
    var margin: CGPoint = CGPoint(x: 17.0, y: 25.0)
    var visibleTextA: Bool = false
    var visibleTextB: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialDataDisplay()
    }

    func setInitialDataDisplay() {
        self.scroll.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(self.scroll)
        

        nextY = margin.y
        let titleLabel = self.addLabelWithTag(font: Theme.shared.fonts.boldFont.withSize(28), textColor: .black, container: self.scroll, multiline: true)
        titleLabel.frame = CGRect(x: margin.x, y: nextY, width: view.frame.width - (margin.x * 2), height: 250)
        titleLabel.text = "Seguro de Accidentes Personales para el Titular"
        titleLabel.sizeToFit()
        nextY = titleLabel.frame.origin.y + titleLabel.frame.size.height + margin.y

        let image = UIImage(named: "seguro-accidentes.jpg")
        let imageVw = UIImageView(image: image)
        imageVw.frame = CGRect(x: 0, y: nextY, width: view.frame.width, height: 250)
        scroll.addSubview(imageVw)
        nextY = imageVw.frame.origin.y + imageVw.frame.size.height + margin.y

        addThreeColumnLabel("Cobertura", secondary: "Suma asegurada", third: "Deducible", container: scroll)
        nextY = nextY + margin.y
        addThreeColumnLabel("Muerte accidental", secondary: "$10,000", third: "N/A", container: scroll, color: .gray)
        addSeparator()
        addThreeColumnLabel("Pérdidas orgánicas por accidentes escala B", secondary: "Hasta $10,000", third: "N/A", container: scroll, color: .gray)
        addSeparator()
        addThreeColumnLabel("Gastos médicos por accidente", secondary: "$10,000", third: "$750", container: scroll, color: .gray)
        addSeparator()
        nextY = nextY + margin.y

        let iconImage = UIImage(named: "collapsible-icon.png")
        let iconImageVw = UIImageView(image: iconImage)
        iconImageVw.frame = CGRect(x: scroll.frame.width - 40, y: nextY - 8, width: (iconImage?.size.width)!, height: (iconImage?.size.height)!)
        scroll.addSubview(iconImageVw)
        let button = UIButton(frame: CGRect(x: 0, y: nextY - 15, width: scroll.frame.width, height: 40))
//        button.backgroundColor = .gray
        addLabel("TABLA DE INDEMNIZACIÓN PÉRDIDAS ORGÁNICAS", container: scroll, deltaW: 25)
        button.addTarget(self, action: #selector(onButtonActionA), for: .touchUpInside)
        scroll.addSubview(button)
        
        if visibleTextA {
            addTwoColumnGrayLabel("Ambas manos o ambos pies o la vista de ambos ojos", secondary: "100%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("Una mano y un pie", secondary: "100%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("Una mano, un pie y la vista de un ojo", secondary: "100%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("Una mano o un pie", secondary: "50%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("La vista de un ojo", secondary: "30%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("El pulgar de cualquier mano", secondary: "15%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("El índice de cualquier mano", secondary: "10%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("Amputación parcial de un pie, comprendiendo todos los dedos", secondary: "30%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("Tres dedos de una mano, comprendiendo pulgar y el índice", secondary: "30%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("Tres dedos de una mano, que no sean el pulgar o el índice", secondary: "25%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("El pulgar de una mano y otro que no sea el índice", secondary: "25%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("La audición total e irreversible en ambos oídos", secondary: "25%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("El índice de cualquier mano y otro que no sea el pulgar", secondary: "20%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("Acortamiento de por lo menos 5 cm., de un miembro inferior", secondary: "15%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("El dedo medio, anular o meñique", secondary: "6%", container: scroll)
            addSeparator()
            addTwoColumnGrayLabel("Cualquier dedo del pie", secondary: "6%", container: scroll)
            addSeparator()
            nextY = nextY + margin.y
        } else {
            iconImageVw.transform = CGAffineTransform(rotationAngle: CGFloat.pi / -2)
        }

        let iconImageVwA = UIImageView(image: iconImage)
        iconImageVwA.frame = CGRect(x: scroll.frame.width - 40, y: nextY - 8, width: (iconImage?.size.width)!, height: (iconImage?.size.height)!)
        scroll.addSubview(iconImageVwA)

        let buttonA = UIButton(frame: CGRect(x: 0, y: nextY - 15, width: scroll.frame.width, height: 40))
//        buttonA.backgroundColor = .gray
        addLabel("EXCLUSIONES", container: scroll)
        buttonA.addTarget(self, action: #selector(onButtonActionB), for: .touchUpInside)
        scroll.addSubview(buttonA)

        if visibleTextB {
            addGrayLabel("El contrato del seguro contenido en esta póliza no cubre por concepto de accidentes lo siguiente:\n\n1. Enfermedad corporal o mental resultado de una lesión accidental.\n\n2. Hernias y eventraciones, salvo que sean a consecuencia de un accidente.\n\n3. Abortos y legrados, salvo que sean a consecuencia de un accidente.\n\n4. Envenenamiento e intoxicación de cualquier origen o naturaleza, salvo los accidentales.\n\n5. Suicidio o cualquier intento del mismo.\n\n6. Mutilación voluntaria, aun cuando se cometa en estado de enajenación mental y lesiones.\n\n7. Lesiones que el asegurado sufra en servicio militar de cualquier clase, en actos de guerra, rebelión, alborotos populares o insurrección.\n\n8. Riña, homicidio intencional y actos delictivos intencionales en que participe el asegurado.\n\n9. Lesiones ocurridas por culpa grave del asegurado por encontrarse bajo los efectos de alcohol (con un máximo de 0.2% o de 50 a 100 mg. / 100 ml. de alcohol en la sangre) o drogas no prescritas médicamente.\n\n10. Gastos realizados para acompañantes del asegurado durante la interacción de éste en un sanatorio u hospital.\n\n11. Infecciones con excepción de la que acontezcan como resultado de una lesión accidental.\n\n12. Riesgos nucleares o atómicos.\n\n13. Participar en Servicio militar, actos de guerra, rebelión e insurrección.\n\n14. Prestación de servicios del asegurado en las fuerzas armadas o fuerzas policiales de cualquier tipo.\n\n15. Guerra declarada o no, civil o internacional. Actos de guerrilla, rebelión, sedición, motín, Terrorismo o Actos de Terrorismo, huelga o tumulto popular, cuando el asegurado hubiera participado como elemento activo.\n\n16. Práctica profesional de cualquier deporte.\n\n17. Accidentes originados por hacer uso de enervantes, estimulantes o cualquier droga ilegal u otra sustancia similar, salvo que se demuestre prescripción médica.", container: scroll)
        } else {
            iconImageVwA.transform = CGAffineTransform(rotationAngle: CGFloat.pi / -2)
        }
        addLabel("Para conocer más información del servicio, comunícate al (0155) 91490727.\n\nEn cumplimiento a lo dispuesto en el artículo 202 de la Ley de Instituciones de Seguros y de Fianzas, la documentación contractual y la nota técnica que integran este producto de seguro, quedaron registradas ante la Comisión Nacional de Seguros y Fianzas, a partir del día 18 de Septiembre del 2017, con el número CNSF-S0120-0452-2017/CONDUSEF-002736-03. Operado por Thona Seguros", container: scroll, deltaW: 25)

        self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: nextY)
    }

    @objc func onButtonActionA() {
        visibleTextA = !visibleTextA
        self.scroll.subviews.forEach { $0.removeFromSuperview() }
        setInitialDataDisplay()
    }

    @objc func onButtonActionB() {
        visibleTextB = !visibleTextB
        self.scroll.subviews.forEach { $0.removeFromSuperview() }
        setInitialDataDisplay()
    }

    func addSeparator(height: CGFloat = 1.0) {
        let separator = UIView.init(frame: CGRect(x: margin.x, y: nextY - (margin.y * 0.5), width: self.view.frame.size.width - (margin.x * 2), height: height))
        separator.backgroundColor = Theme.shared.colors.cellSeparatorColor
        self.scroll.addSubview(separator)
    }

    func addTitleLabel(container:UIView, bigFontSize:Bool = true, under:CGRect? = nil, indent:CGFloat = 0, maxHeight:CGFloat? = 65) -> UILabel {
        let title = self.addLabelWithTag(tag: 101, font: Theme.shared.fonts.detailTitleFont, textColor: .black, container: container, multiline:true)
        if under != nil {
            let finalFrame = CGRect(origin: CGPoint(x: (under?.origin.x)!, y: (under?.origin.y)! + (under?.size.height)! + indent), size: CGSize(width:container.frame.size.width - ((under?.origin.x)! * 2), height: maxHeight! * BasicCellSize.sizeFactor))
            title.frame = finalFrame
            title.fitFontForSize(constrainedSize: title.frame.size, maxFontSize: 80.0, minFontSize: 14.0)
        }
        return title
    }

    func addGrayLabel(_ text: String, container: UIView) {
        let textLayer = UILabel(frame: CGRect(x: 16, y: nextY, width: self.view.frame.size.width - (margin.x * 2.5), height: 600))
        textLayer.lineBreakMode = .byWordWrapping
        textLayer.numberOfLines = 0
        textLayer.textAlignment = .justified
        textLayer.textColor = UIColor.gray
        let textContent = text
        let textString = NSMutableAttributedString(string: textContent, attributes: [
            NSAttributedStringKey.font: Theme.shared.fonts.regularFont.withSize(16.0)
            ])
        textLayer.attributedText = textString
        textLayer.sizeToFit()
        container.addSubview(textLayer)
        setNextY(view: textLayer)
    }

    func addThreeColumnLabel(_ text: String, secondary: String, third: String, container: UIView, color: UIColor = .black) {
        let textLayer = UILabel(frame: CGRect(x: margin.x, y: nextY, width: (self.view.frame.size.width * 0.38) - margin.x, height: 200))
        textLayer.lineBreakMode = .byWordWrapping
        textLayer.numberOfLines = 0
        textLayer.textColor = color
        let textContent = text
        let textString = NSMutableAttributedString(string: textContent, attributes: [
            NSAttributedStringKey.font: Theme.shared.fonts.regularFont.withSize(16.0)
            ])
        textLayer.attributedText = textString
        textLayer.sizeToFit()
        container.addSubview(textLayer)

        let textLayer2 = UILabel(frame: CGRect(x: (self.view.frame.size.width * 0.38) + margin.x, y: nextY, width: (self.view.frame.size.width * 0.3) - (margin.x), height: 200))
        textLayer2.lineBreakMode = .byWordWrapping
        textLayer2.numberOfLines = 0
        textLayer2.textAlignment = .center
        textLayer2.textColor = color
        let textContent2 = secondary
        let textString2 = NSMutableAttributedString(string: textContent2, attributes: [
            NSAttributedStringKey.font: Theme.shared.fonts.regularFont.withSize(16.0)
            ])
        textLayer2.attributedText = textString2
        textLayer2.sizeToFit()
        textLayer2.frame.size.width = (self.view.frame.size.width * 0.3) - (margin.x)
        container.addSubview(textLayer2)

        let textLayer3 = UILabel(frame: CGRect(x: (self.view.frame.size.width * 0.68) + margin.x, y: nextY, width: (self.view.frame.size.width * 0.3) - (margin.x), height: 200))
        textLayer3.lineBreakMode = .byWordWrapping
        textLayer3.numberOfLines = 0
        textLayer3.textAlignment = .center
        textLayer3.textColor = color
        let textContent3 = third
        let textString3 = NSMutableAttributedString(string: textContent3, attributes: [
            NSAttributedStringKey.font: Theme.shared.fonts.regularFont.withSize(16.0)
            ])
        textLayer3.attributedText = textString3
        textLayer3.sizeToFit()
        textLayer3.frame.size.width = (self.view.frame.size.width * 0.3) - (margin.x)
        container.addSubview(textLayer3)

        setNextY(view: textLayer)
    }

    func addTwoColumnGrayLabel(_ text: String, secondary: String, container: UIView) {
        let textLayer = UILabel(frame: CGRect(x: margin.x, y: nextY, width: (self.view.frame.size.width * 0.75) - (margin.x * 2), height: 200))
        textLayer.lineBreakMode = .byWordWrapping
        textLayer.numberOfLines = 0
        textLayer.textColor = UIColor.gray
        let textContent = text
        let textString = NSMutableAttributedString(string: textContent, attributes: [
            NSAttributedStringKey.font: Theme.shared.fonts.regularFont.withSize(16.0)
            ])
        textLayer.attributedText = textString
        textLayer.sizeToFit()
        container.addSubview(textLayer)

        let textLayer2 = UILabel(frame: CGRect(x: (self.view.frame.size.width * 0.75) + margin.x, y: nextY, width: (self.view.frame.size.width * 0.25) - (margin.x), height: 200))
        textLayer2.lineBreakMode = .byWordWrapping
        textLayer2.numberOfLines = 0
        textLayer2.textAlignment = .center
        textLayer2.textColor = UIColor.gray
        let textContent2 = secondary
        let textString2 = NSMutableAttributedString(string: textContent2, attributes: [
            NSAttributedStringKey.font: Theme.shared.fonts.regularFont.withSize(16.0)
            ])
        textLayer2.attributedText = textString2
        textLayer2.sizeToFit()
        container.addSubview(textLayer2)

        setNextY(view: textLayer)
    }

    func addLabel(_ text: String, container: UIView, deltaW: CGFloat! = 0) {
        let textLayer = UILabel(frame: CGRect(x: margin.x, y: nextY, width: self.view.frame.size.width - (margin.x * 2.5) - deltaW, height: 200))
        textLayer.lineBreakMode = .byWordWrapping
        textLayer.numberOfLines = 0
        textLayer.textAlignment = .justified
        textLayer.textColor = UIColor.black
        let textContent = text
        let textString = NSMutableAttributedString(string: textContent, attributes: [
            NSAttributedStringKey.font: Theme.shared.fonts.regularFont.withSize(16.0)
            ])
        textLayer.attributedText = textString
        textLayer.sizeToFit()
        container.addSubview(textLayer)
        setNextY(view: textLayer)
    }

    func setNextY(view: UIView) {
        nextY = view.frame.origin.y + view.frame.height + margin.y
    }

}
