//
//  NewsDetailViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 02/01/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit

class NewsDetailViewController: BasicDetailViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func customNavigationBar() {
        let backImg: UIImage = UIImage(named: "header-back-button")!
        let backButton = UIBarButtonItem(image: backImg, style: .plain, target: self, action: #selector(BasicDetailViewController.backAction))
        let shareImg: UIImage = UIImage(named: "share-icon")!
        let shareButton = UIBarButtonItem(image: shareImg, style: .plain, target: self, action: #selector(BasicDetailViewController.shareAction))
/*
        if Theme.shared.userLogged {
            let chatImg: UIImage = UIImage(named: "chat")!
            let chatButton = UIBarButtonItem(image: chatImg, style: .plain, target: self, action: #selector(openChat))
//        shareButton.setBackgroundVerticalPositionAdjustment(-10.0, for: .default)
//            self.navigationItem.rightBarButtonItems = [chatButton]
        }
*/
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.setBackgroundVerticalPositionAdjustment(-10.0, for: .default)
        if self.ownerData != nil {
            self.navigationController?.navigationBar.tintColor = self.ownerData.color
        }
        self.navigationItem.title = item.sectionTitle
    }

    override func setInitialDataDisplay() {
        if self.item != nil {
            self.title = self.item.sectionTitle
        }
        let scrollY:CGFloat = 0.0
        self.scroll.frame = CGRect(x: 0.0, y: scrollY, width: self.view.frame.size.width, height: self.view.frame.size.height - scrollY)
        self.view.addSubview(self.scroll)
        let betweenMargin = margin.y
        nextY = 0
        titleLabel = self.addTitleLabel(container: self.scroll, under: CGRect(x: margin.x, y: nextY, width: self.view.frame.width - (margin.x * 2), height: 1), indent: betweenMargin, maxHeight: 120.0)
        nextY = titleLabel.frame.origin.y + titleLabel.frame.size.height + betweenMargin

        let desc = self.addLabelWithTag(tag: 10004, font: UIFont.detailDesc, textColor: .black, container: self.scroll, multiline: true)
        desc.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        do {
            desc.text = try self.item.desc.convertHtmlSymbols()
        } catch {}
        desc.sizeToFit()
        nextY = nextY + desc.frame.height + betweenMargin
        setDataContent()
        self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: nextY)
    }

    @objc func openChat() {
        print("chat")
    }

}
