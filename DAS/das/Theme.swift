//
//  BasicTheme.swift
//  GenericNews
//
//  Created by Patricio Bravo Cisneros on 06/12/17.
//  Copyright © 2017 OpenMultimedia. All rights reserved.
//

import UIKit

final class Theme {

    // Can't init is singleton
    private init() { }

    // MARK: Shared Instance
    static let shared = Theme()

    // MARK: Local Variable

    var appTitle: String = "Generic News"

    var userLogged: Bool = false
    var colors: ThemeColors = ThemeColors()
    var fonts: ThemeFonts = ThemeFonts()
    var user = DasUserInfo()
    var app = DasAppInfo()
    var last = DasAppInfo()

    func cleanAppInfo() {
        Theme.shared.user = DasUserInfo()
    }

    var chatVc: DASChatViewController!
}

class DasAppInfo {
    var stateId: String = "0"
    var specialty: String = ""
    var regionId: String = "0"
    var region: String = ""
    var state: String = ""
    var type: String = ""
    var typeId: String = "0"
    var specialtyId: String = "0"
}

class DasUserInfo {
    var affiliateNumber: String!
    var membershipType: MembershipType!
    var name: String!
    var id: String!
    var expiresAt: String!
    var haveFuneralService: Bool = false
    var haveAccidentInsurance: Bool = false
    var aditionals: [[String: AnyObject]]!
    var maxAdditionals: NSNumber = 0
    var isAditional: Bool = false
}

enum MembershipType {

    case master
    case premium

}

class ThemeFonts {

    var chatFontSize: CGFloat = 18.0

    var baseSize: CGFloat = 12.0
    var regularFont: UIFont = UIFont.systemFont(ofSize: 12.0)
    var boldFont: UIFont = UIFont.boldSystemFont(ofSize: 12.0)
    var italicFont: UIFont = UIFont.italicSystemFont(ofSize: 12.0)
    var pageControllerSelectedItemFont: UIFont = UIFont.boldSystemFont(ofSize: 12.0)
    var pageControllerUnselectedItemFont: UIFont = UIFont.boldSystemFont(ofSize: 12.0)
    var cellSectionFont: UIFont = UIFont.systemFont(ofSize: 11.0, weight: UIFont.Weight.medium)
    var cellTitleFont: UIFont = UIFont.systemFont(ofSize: 18.0, weight: UIFont.Weight.heavy)
    var cellTitleWithBackground: UIFont = UIFont.systemFont(ofSize: 18.0, weight: UIFont.Weight.heavy)
    var detailTitleFont: UIFont = UIFont.systemFont(ofSize: 22.0, weight: UIFont.Weight.heavy)
}

class ThemeColors {
    var viewBackgroundColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1.0)
    var cellSeparatorColor = UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1.0)
    var navigationBarBackgroundColor = UIColor(red: 255/255, green: 226/255, blue: 0/255, alpha: 1.0)
    var pageControllerBackgroundColor = UIColor(red: 255/255, green: 226/255, blue: 0/255, alpha: 1.0)

    var cellSectionColor = UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1.0)
    var cellTitleColor = UIColor.black
    var cellTitleWithBackgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)

    var navigationBarColor1 = UIColor(red: 67/255, green: 115/255, blue: 164/255, alpha: 1)
    var navigationBarColor2 = UIColor(red: 50/255, green: 82/255, blue: 129/255, alpha: 1)
    var navigationBarColor3 = UIColor(red: 40/255, green: 59/255, blue: 104/255, alpha: 1)

    var textBlue = UIColor(red: 76/255, green: 102/255, blue: 149/255, alpha: 1.0)
    var textYellow = UIColor(red: 223/255, green: 172/255, blue: 40/255, alpha: 1.0)
    var textGray = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 1.0)
    var inactiveTextGray = UIColor(red: 99/255, green: 99/255, blue: 99/255, alpha: 1.0)
    var textBlueGray = UIColor(red: 196/255, green: 211/255, blue: 224/255, alpha: 1.0)
    var textBrightBlue = UIColor(red: 71/255, green: 124/255, blue: 251/255, alpha: 1.0)
    var redEmergency = UIColor(red: 212/255, green: 76/255, blue: 65/255, alpha: 1.0)
    var graySeparator = UIColor(red: 238/255, green: 238/255, blue: 243/255, alpha: 1.0)

    var blueButton = UIColor(red: 102/255, green: 181/255, blue: 232/255, alpha: 1.0)

    var sectionColors: [UIColor] = [UIColor(red: 73/255,  green: 144/255, blue: 226/255, alpha: 1.0),
                                    UIColor(red: 150/255, green: 67/255,  blue: 171/255, alpha: 1.0),
                                    UIColor(red: 254/255, green: 64/255,  blue: 64/255,  alpha: 1.0),
                                    UIColor(red: 244/255, green: 165/255, blue: 34/255,  alpha: 1.0),
                                    UIColor(red: 44/255, green: 97/255, blue: 130/255, alpha: 1.0),
                                    UIColor(red: 106/255, green: 148/255, blue: 84/255, alpha: 1.0),
                                    UIColor(red: 227/255, green: 73/255, blue: 61/255, alpha: 1.0),
                                    UIColor(red: 162/255, green: 100/255, blue: 173/255, alpha: 1.0),
                                    UIColor(red: 144/255,  green: 73/255, blue: 226/255, alpha: 1.0),
                                    UIColor(red: 171/255, green: 67/255,  blue: 150/255, alpha: 1.0),
                                    UIColor(red: 64/255, green: 254/255,  blue: 64/255,  alpha: 1.0),
                                    UIColor(red: 34/255, green: 165/255, blue: 244/255,  alpha: 1.0),
                                    UIColor(red: 44/255, green: 130/255, blue: 97/255, alpha: 1.0),
                                    UIColor(red: 106/255, green: 84/255, blue: 148/255, alpha: 1.0),
                                    UIColor(red: 61/255, green: 73/255, blue: 227/255, alpha: 1.0),
                                    UIColor(red: 162/255, green: 173/255, blue: 100/255, alpha: 1.0),
                                    UIColor(red: 226/255,  green: 144/255, blue: 73/255, alpha: 1.0),
                                    UIColor(red: 150/255, green: 171/255,  blue: 67/255, alpha: 1.0),
                                    UIColor(red: 244/255, green: 24/255,  blue: 64/255,  alpha: 1.0),
                                    UIColor(red: 204/255, green: 125/255, blue: 34/255,  alpha: 1.0),
                                    UIColor(red: 44/255, green: 117/255, blue: 150/255, alpha: 1.0),
                                    UIColor(red: 106/255, green: 148/255, blue: 84/255, alpha: 1.0),
                                    UIColor(red: 227/255, green: 73/255, blue: 61/255, alpha: 1.0),
                                    UIColor(red: 162/255, green: 100/255, blue: 173/255, alpha: 1.0),
                                    UIColor(red: 144/255,  green: 73/255, blue: 226/255, alpha: 1.0),
                                    UIColor(red: 171/255, green: 67/255,  blue: 150/255, alpha: 1.0),
                                    UIColor(red: 64/255, green: 254/255,  blue: 64/255,  alpha: 1.0),
                                    UIColor(red: 34/255, green: 165/255, blue: 244/255,  alpha: 1.0),
                                    UIColor(red: 44/255, green: 130/255, blue: 97/255, alpha: 1.0),
                                    UIColor(red: 106/255, green: 84/255, blue: 148/255, alpha: 1.0),
                                    UIColor(red: 61/255, green: 73/255, blue: 227/255, alpha: 1.0),
                                    UIColor(red: 162/255, green: 173/255, blue: 100/255, alpha: 1.0)]
}
