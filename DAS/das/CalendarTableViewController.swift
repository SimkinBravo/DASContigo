//
//  CalendarTableViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 11/01/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import Alamofire

class CalendarTableViewController: DASBasicTableViewController {

    var refreshOnViewAppear: Bool = false

    override func viewDidLoad() {
        self.navigationItem.title = "Calendario"
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if refreshOnViewAppear {
            loadCalendar()
        }
    }

    override func loadData() {
        if !Theme.shared.userLogged {
            let back = UIView(frame: self.tableView.frame)
            self.tableView.backgroundView = back
            setupUnloggedUserScreen(container: back)
            super.onLoadDataResponse()
        } else {
            loadCalendar()
        }
    }

    func setupEmptyAgenda(container: UIView) {
        let image = UIImage(named: "no-membership.png")
        let imageVw = UIImageView(image: image)
        imageVw.center = container.center
        imageVw.frame = CGRect(origin: CGPoint(x: imageVw.frame.origin.x, y: imageVw.frame.origin.y + 50), size: imageVw.frame.size)
        container.addSubview(imageVw)
        let indent:CGFloat = 40.0
        let text = UILabel(frame: CGRect(x: indent, y: imageVw.frame.origin.y + imageVw.frame.size.height + 10, width: container.frame.size.width - (indent * 2), height: 100))
        text.text = "Aún no tienes citas agendadas"
        text.numberOfLines = 0
        text.lineBreakMode = .byWordWrapping
        text.textAlignment = .center
        text.textColor = Theme.shared.colors.blueButton
        container.addSubview(text)
        var parsed = [BasicCellData]()
        parsed.append(BasicCellData(title: "Agregar evento").setupRowButtonCell())
        self.elements.append(parsed)
    }

    func loadCalendar() {
        print("\(Urls.base)obtenerCita")
        let formParams:[String: String] = ["idAfiliacion": Theme.shared.user.affiliateNumber]
        print(formParams)
        Alamofire.request("\(Urls.base)obtenerCita", method: .post, parameters: formParams, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.value != nil {
                if let rawResponse = response.result.value as? [[String: AnyObject]] {
                    self.elements = []
                    var sections = [String: [BasicCellData]]()
                    if rawResponse.count == 0 {
                        let back = UIView(frame: self.tableView.frame)
                        self.tableView.backgroundView = back
                        self.setupEmptyAgenda(container: back)
                    } else {
                        self.tableView.backgroundView = nil
                        for rawRow in rawResponse {
                            let data = BasicCellData(index: 0, calendarRawItem: rawRow)
                            if sections[data.appointment.tempDate] == nil {
                                sections[data.appointment.tempDate] = [BasicCellData]()
                            }
                            sections[data.appointment.tempDate]?.append(data)
                        }
                        let newSections = sections.sorted(by: { !($0.value.first?.appointment.older)! && ($1.value.first?.appointment.older)! })
                        self.elements.append([BasicCellData(title: "Agregar evento").setupRowButtonCell()])
                        for section in newSections {
                            var temp = [BasicCellData]()
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateStyle = .long
                            dateFormatter.locale = Locale(identifier: "es_MX")
                            temp.append(BasicCellData(title: dateFormatter.string(from: (section.value.first?.appointment.startD)!)).setupSectionSeparatorCell())
                            self.elements.append(temp)
                            self.elements.append(section.value)
                        }
                        self.onLoadDataResponse()
                    }
                } else {
                    let back = UIView(frame: self.tableView.frame)
                    self.tableView.backgroundView = back
                    self.setupEmptyAgenda(container: back)
                }
                self.onLoadDataResponse()
            } else {
//                self.onLoadDataResponse()
            }
        }
    }

    override func tableViewDidSelectData(data: BasicCellData) {
        if data.slug == "add" {
            refreshOnViewAppear = true
            let vc = storyboard?.instantiateViewController(withIdentifier: "AppointmentViewController")
            present(vc!, animated: true, completion: nil)
        } else {
            if data.appointment.older {
                refreshOnViewAppear = true
                let navVc = storyboard?.instantiateViewController(withIdentifier: "RateAppointmentViewController") as! UINavigationController
                let vc = navVc.topViewController as! RateAppointmentViewController
                vc.item = data
                present(navVc, animated: true, completion: nil)
            } else {
                refreshOnViewAppear = true
                let navVc = storyboard?.instantiateViewController(withIdentifier: "UpdateAppointmetViewController") as! UINavigationController
                let vc = navVc.topViewController as! UpdateAppointmetViewController
                vc.item = data
                present(navVc, animated: true, completion: nil)
            }
        }
    }

}
