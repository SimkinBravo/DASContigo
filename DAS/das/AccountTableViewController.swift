//
//  AccountTableViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 11/01/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit

class AccountTableViewController: DASBasicTableViewController {

    var accountType: Int = 0

    override func viewDidLoad() {
        super.navigationItem.title = "Membresía"
        super.viewDidLoad()
    }

    override func loadData() {
        if Theme.shared.userLogged {
            var fsection: [BasicCellData] = []
            let benefit = BasicCellData(title: "MARCO SOLIS CONTRERAS", subtitle: "Afiliación")
            benefit.contentStyle = .customExtended
            let imageData = BasicImageData(title: "", desc: "", thumbURL: "")
            imageData.imageName = "card"
            imageData.thumbURL = "card"
            imageData.imageTag = benefit.imagesTag
            imageData.roundedCourners = true
            benefit.images = [imageData]
            benefit.backgroundColor = Theme.shared.colors.graySeparator
            benefit.imageMargin = CGPoint(x: 10.0, y: 10.0)
            benefit.titleTextColor = .black
            benefit.cellId = CellId.custom
            benefit.cellSize = BasicCellSize.highlighted
            benefit.placeholderBackgroundColor = Theme.shared.colors.textBlue
            fsection.append(benefit)

            if let aditionals = Theme.shared.user.aditionals {
                if aditionals.count != 0 {
                    fsection.append(BasicCellData(title: "Adicionales").setupSeparatorTitleCell())
                    for aditional in aditionals {
                        print(aditional, "\n---\n")
                        fsection.append(getElementDataWith(title: aditional["nombre"] as! String, desc: String(describing: aditional["idAfiliacion"] as! NSNumber)))
                    }
                    let max = Theme.shared.user.maxAdditionals
                    if max.intValue != aditionals.count {
                        fsection.append(BasicCellData(slug: "contact", title: "Agregar adicionales faltantes").setupLinkedCell())
                    }
                }
            }
            fsection.append(BasicCellData(title: "ACCIONES").setupSectionSeparatorCell())
/*
            if Theme.shared.user.aditionals != nil {
                fsection.append(BasicCellData(slug: "contact", title: "Renovar membresía").setupLinkedCell())
            }
*/
            fsection.append(BasicCellData(slug: "unlog", title: "Cerrar sesión").setupLinkedCell())
            self.elements.append(fsection)
        } else {
            let back = UIView(frame: self.tableView.frame)
            self.tableView.backgroundView = back
            setupUnloggedUserScreen(container: back)
        }
        super.onLoadDataResponse()
    }

    override func tableViewDidSelectData(data: BasicCellData) {
        if data.slug == "unlog" {
            UserDefaults.standard.set(nil, forKey: "member")
            if Theme.shared.chatVc != nil {
                Theme.shared.chatVc.disconnect()
            }
            Theme.shared.chatVc = nil
            pushLoginVc()
        }
        if data.slug == "contact" {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }

    func pushLoginVc() {
        Theme.shared.userLogged = false
        Theme.shared.cleanAppInfo()
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController")
        self.tabBarController?.navigationController?.setViewControllers([vc], animated: true)
    }

    func getElementDataWith(title: String, desc: String = "", icon: Int = 0) -> BasicCellData {
        let data = BasicCellData(title: title, subtitle: desc)
        data.cellSize = BasicCellSize.separator
        data.contentStyle = .horizontal
        data.cellId = .basic
        data.adjustFontSize = false
        data.textMargin = CGPoint(x: 20.0, y: 10.0)
        data.isImageHidden = true
        data.imagePercent = 0.05
        data.titleFont = Theme.shared.fonts.regularFont
        data.descFont = Theme.shared.fonts.boldFont
        data.userInteractionEnabled = false
        data.isSeparatorHidden = false
        return data
    }

    func getTitleDataWith(title: String) -> BasicCellData {
        let data = BasicCellData(title: title, subtitle: "")
        data.cellSize = BasicCellSize.basic
        data.cellId = .highlighted
        data.adjustFontSize = false
        data.contentStyle = .horizontal
        data.imagePercent = 0
        data.textMargin = CGPoint(x: 20.0, y: 10.0)
        data.isImageHidden = true
        data.titleFont = Theme.shared.fonts.boldFont.withSize(24.0)
        data.userInteractionEnabled = false
        data.isSeparatorHidden = false
        return data
    }

    func getSectionDataWith(title: String) -> BasicCellData {
        let data = BasicCellData(title: title, subtitle: "")
        data.cellSize = BasicCellSize.separator
        data.cellId = .highlighted
        data.adjustFontSize = false
        data.contentStyle = .horizontal
        data.imagePercent = 0
        data.textMargin = CGPoint(x: 20.0, y: 10.0)
        data.isImageHidden = true
        data.backgroundColor = Theme.shared.colors.graySeparator
        data.titleTextColor = Theme.shared.colors.textGray
        data.titleFont = Theme.shared.fonts.regularFont
        data.userInteractionEnabled = false
        data.isSeparatorHidden = false
        return data
    }

}
