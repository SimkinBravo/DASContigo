//
//  UpdateAppointmetViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 18/02/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import DatePickerDialog

class UpdateAppointmetViewController: DasBasicViewController {

    let formatter = DateFormatter()

    var startAtA: UILabel!
    var endAtA: UILabel!
    var alertA: UILabel!
    var placeAt: UILabel!
    var selected: UILabel!
    var activityIndicator: NVActivityIndicatorView!
    var item: BasicCellData!
    let notificationOptions = ["Sin notificación", "1 hora antes", "2 horas antes", "1 día antes"]
    var notification: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        formatter.dateFormat = "dd/MM/yyyy HH:mm"
        var colors = [UIColor]()
        colors.append(Theme.shared.colors.navigationBarColor1)
        colors.append(Theme.shared.colors.navigationBarColor2)
        colors.append(Theme.shared.colors.navigationBarColor3)
        navigationController?.navigationBar.setGradientBackground(colors: colors)

        self.navigationController?.navigationBar.tintColor = Theme.shared.colors.blueButton
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Actualizar", style: .plain, target: self, action: #selector(addButtonAction))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(cancelButtonAction))
//        self.navigationItem.rightBarButtonItem?.isEnabled = false

        let serviceName = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        serviceName.frame = CGRect(x: 15, y: (self.navigationController?.navigationBar.frame.height)! + 25, width: self.view.frame.size.width - 30, height: 50)
        serviceName.text = item.appointment.providerName

        addSeparator(margin: CGPoint(x: 0.0, y: serviceName.frame.origin.y + serviceName.frame.height + 5), at: view)
        
        placeAt = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        placeAt.frame = CGRect(x: 15.0, y: serviceName.frame.origin.y + serviceName.frame.height + 10, width: view.frame.size.width - 30, height: 45)
        placeAt.text = item.appointment.providerAdress

        addSeparator(margin: CGPoint(x: 0.0, y: placeAt.frame.origin.y + placeAt.frame.height + 5), at: view)

        let startAt = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        startAt.frame = CGRect(x: 15.0, y: placeAt.frame.origin.y + placeAt.frame.height + 5, width: view.frame.size.width - 30, height: 50)
        startAt.text = "Inicia"
        startAtA = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: Theme.shared.colors.textGray, container: view)
        startAtA.frame = CGRect(x: 85.0, y: startAt.frame.origin.y, width: view.frame.size.width - 100, height: 50)
        startAtA.textAlignment = .right
        startAtA.text = item.appointment.startTime
        
        let startAtButton = UIButton(type: .custom)
        startAtButton.frame = CGRect(x: 15, y: startAt.frame.origin.y, width: self.view.frame.size.width - 30, height: 50)
        startAtButton.clipsToBounds = true
        startAtButton.addTarget(self, action: #selector(startAtButtonAction), for: .touchUpInside)
        view.addSubview(startAtButton)
        
        addSeparator(margin: CGPoint(x: 0.0, y: startAt.frame.origin.y + startAt.frame.height), at: view)
        
        let endAt = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        endAt.frame = CGRect(x: 15.0, y: startAt.frame.origin.y + startAt.frame.height, width: view.frame.size.width - 30, height: 50)
        endAt.text = "Termina"
        endAtA = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: Theme.shared.colors.textGray, container: view)
        endAtA.frame = CGRect(x: 50.0, y: endAt.frame.origin.y, width: view.frame.size.width - 65, height: 50)
        endAtA.textAlignment = .right
        endAtA.text = item.appointment.endTime
        
        let endAtButton = UIButton(type: .custom)
        endAtButton.frame = CGRect(x: 15, y: endAt.frame.origin.y, width: self.view.frame.size.width - 30, height: 50)
        endAtButton.clipsToBounds = true
        endAtButton.addTarget(self, action: #selector(endAtButtonAction), for: .touchUpInside)
        view.addSubview(endAtButton)
        
        addSeparator(margin: CGPoint(x: 0.0, y: endAt.frame.origin.y + endAt.frame.height), at: view)

        let alert = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        alert.frame = CGRect(x: 15.0, y: endAt.frame.origin.y + endAt.frame.height, width: view.frame.size.width - 30, height: 50)
        alert.text = "Notificación"
        alertA = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: Theme.shared.colors.textGray, container: view)
        alertA.frame = CGRect(x: 50.0, y: alert.frame.origin.y, width: view.frame.size.width - 65, height: 50)
        alertA.textAlignment = .right
        alertA.text = notificationOptions[notification]
        
        let alertButton = UIButton(type: .custom)
        alertButton.frame = CGRect(x: 15, y: alert.frame.origin.y, width: self.view.frame.size.width - 30, height: 50)
        alertButton.clipsToBounds = true
        alertButton.addTarget(self, action: #selector(alertButtonAction), for: .touchUpInside)
        view.addSubview(alertButton)
        
        addSeparator(margin: CGPoint(x: 0.0, y: endAt.frame.origin.y + endAt.frame.height), at: view)

        setupActivityIndicator()

    }
    
    /*
     "idAfiliacion": 1,
     "id_proveedor": 1,
     "hora_inicio": "01\/01\/2018 10:00",
     "hora_final": "01\/01\/2018 20:00",
     */
    
    func addAppointment() {
        let formParams = ["idAfiliacion": Theme.shared.user.affiliateNumber!, "id_proveedor": item.appointment.providerId, "hora_inicio": startAtA.text!, "hora_final": endAtA.text!, "calificacion": "0", "autoevaluacion" : "0", "recomendarias": "0", "comentarios": "", "comoSeEntero": []] as [String : Any]
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false

        let uString = "\(Urls.base)actualizarCita?id=\(item.appointment.id!)&idAfiliacion=\(Theme.shared.user.affiliateNumber!)"
        let url =  NSURL(string:uString)
        var request = URLRequest(url: url! as URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let data = try! JSONSerialization.data(withJSONObject: formParams, options: JSONSerialization.WritingOptions.prettyPrinted)

        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        print(uString)
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        Alamofire.request(request).responseJSON { response in
            if response.result.value != nil {
//                print(response.result.value)
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                self.dismiss(animated: true, completion: nil)
            }
        }

    }

    @objc func startAtButtonAction() {
        selected = startAtA
        datePickerTapped()
    }
    
    @objc func endAtButtonAction() {
        selected = endAtA
        datePickerTapped()
    }
    
    @objc func addButtonAction() {
        addAppointment()
    }

    @objc func cancelButtonAction() {
        dismiss(animated: true, completion: nil)
    }

    @objc func alertButtonAction() {
        let actionSheetController: UIAlertController = UIAlertController(title: "Configuración de notificación", message: "Selecciona una opción", preferredStyle: .actionSheet)
        for (index, option) in notificationOptions.enumerated() {
            let actionButton = UIAlertAction(title: option, style: .default) { _ in
                self.alertA.text = option
                self.notification = index
            }
            actionSheetController.addAction(actionButton)
        }
        present(actionSheetController, animated: true, completion: nil)
    }

    func datePickerTapped() {
        let defaultDate = formatter.date(from: selected.text!) ?? Date()
        DatePickerDialog(locale: Locale(identifier: "es_MX")).show("DatePicker", doneButtonTitle: "Aceptar", cancelButtonTitle: "Cancelar", defaultDate: defaultDate, datePickerMode: .dateAndTime) {
            (date) -> Void in
            if let dt = date {
                self.selected.text = self.formatter.string(from: dt)
                if self.selected == self.startAtA {
                    self.endAtA.text = self.formatter.string(from: dt.addingTimeInterval(60 * 60))
                }
                self.navigationItem.rightBarButtonItem?.isEnabled = true
            }
        }
    }

    func setupActivityIndicator() {
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 10, y: (self.navigationController?.navigationBar.frame.height)! + 30, width: 60, height: 60), type: NVActivityIndicatorType.ballClipRotate, color: Theme.shared.colors.navigationBarColor3, padding: 0)
        activityIndicator.center = view.center
        activityIndicator.isHidden = true
        self.view.addSubview(activityIndicator)
    }

}
