//
//  FuneralServicesViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 15/02/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit

class FuneralServicesViewController: DasBasicViewController {

    let scroll: UIScrollView = UIScrollView()
    var nextY: CGFloat = 0.0
    var margin: CGPoint = CGPoint(x: 17.0, y: 25.0)
    var visibleTextA: Bool = false
    var visibleTextB: Bool = false
    var visibleTextC: Bool = false
    var visibleTextD: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialDataDisplay()
    }

    func setInitialDataDisplay() {
        self.scroll.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(self.scroll)

        nextY = margin.y
        let titleLabel = self.addLabelWithTag(font: Theme.shared.fonts.boldFont.withSize(28), textColor: .black, container: self.scroll, multiline: true)
        titleLabel.frame = CGRect(x: margin.x, y: nextY, width: view.frame.width - (margin.x * 2), height: 250)
        titleLabel.text = "Seguro de Asistencia Funeraria"
        titleLabel.sizeToFit()
        nextY = titleLabel.frame.origin.y + titleLabel.frame.size.height + margin.y

        let image = UIImage(named: "servicios-funerarios.jpg")
        let imageVw = UIImageView(image: image)
        imageVw.frame = CGRect(x: 0, y: nextY, width: view.frame.width, height: 250)
        scroll.addSubview(imageVw)
        nextY = imageVw.frame.origin.y + imageVw.frame.size.height + margin.y

//        addGrayLabel("Aplica para el titular, cónyuge y los hijos menores de 21 años.")
        addLabel("Aplica para el Titular, Cónyuge y los Hijos menores de 21 años.")

        let iconImage = UIImage(named: "collapsible-icon.png")
        let iconImageVw = UIImageView(image: iconImage)
        iconImageVw.frame = CGRect(x: scroll.frame.width - 40, y: nextY - 8, width: (iconImage?.size.width)!, height: (iconImage?.size.height)!)
        scroll.addSubview(iconImageVw)
        let button = UIButton(frame: CGRect(x: 0, y: nextY - 15, width: scroll.frame.width, height: 40))
        button.addTarget(self, action: #selector(onButtonActionA), for: .touchUpInside)
        addLabel("El Servicio Funerario Incluye:")
        scroll.addSubview(button)

        if visibleTextA {
            addGrayLabel("1. Atención y asesoría personalizada telefónica las 24 horas, los 365 días del año al TITULAR, CÓNYUGE E HIJOS (estos últimos hasta los 21 años de edad).\n\n2. Primer translado: Del lugar del fallecimiento a la funeraria o domicilio, en caso de que así lo solicite la familia.\n\n3. Segundo translado: De la funeraria al panteón o crematorio.\n\n4. Arreglo estético del cuerpo (maquillar y vestir).\n\n5. Atención y asesoría personalizada ante el MP para la recuperación del finado en territorio nacional.\n\n6. Trámites gubernamentales.\n\n7. Facilidades para celebrar servicios religiosos: Capilla de velación básica con capacidad para 20 personas en circulación o equipo de velación a domicilio.\n\n8. Inhumación en fosa o nicho designado por el titular o familiares.\n\n9. Cremación en sucursales J. García López de la CDMX o en crematorios de la Alianza J. García López, y entrega en una urna básica.\n\n10. Ataúd básico metálico.\n\n11. Cremación urna básica.")
        } else {
            iconImageVw.transform = CGAffineTransform(rotationAngle: CGFloat.pi / -2)
        }

        let iconImageB = UIImage(named: "collapsible-icon.png")
        let iconImageBVw = UIImageView(image: iconImageB)
        iconImageBVw.frame = CGRect(x: scroll.frame.width - 40, y: nextY - 8, width: (iconImageB?.size.width)!, height: (iconImageB?.size.height)!)
        scroll.addSubview(iconImageBVw)
        let buttonB = UIButton(frame: CGRect(x: 0, y: nextY - 15, width: scroll.frame.width, height: 40))
        buttonB.addTarget(self, action: #selector(onButtonActionB), for: .touchUpInside)
        addLabel("El Servicio Funerario No Incluye:")
        scroll.addSubview(buttonB)

        if visibleTextB {
            addGrayLabel("1. Trámites de deceso\n\n2. Esquelas en los periódicos.\n\n3. Costo de trámites y traslados por casos de muerte médico legal (ministerio público y servicio médico forense).\n\n4. Exhumación de restos áridos.\n\n5. Arreglos florales.\n\n6. Honorarios y maniobras del panteón.\n\n7. Traslados foráneos (tramites y fletes terrestres y aéreos).\n\n8. Servicio de cafetería.\n\n9. Nicho, fosa, cripta, gaveta.\n\n10. Pullman para acompañantes.\n\n11. Embalsamamiento.")
        } else {
            iconImageBVw.transform = CGAffineTransform(rotationAngle: CGFloat.pi / -2)
        }

        let iconImageC = UIImage(named: "collapsible-icon.png")
        let iconImageCVw = UIImageView(image: iconImageC)
        iconImageCVw.frame = CGRect(x: scroll.frame.width - 40, y: nextY - 8, width: (iconImageC?.size.width)!, height: (iconImageC?.size.height)!)
        scroll.addSubview(iconImageCVw)
        let buttonC = UIButton(frame: CGRect(x: 0, y: nextY - 15, width: scroll.frame.width, height: 40))
        buttonC.addTarget(self, action: #selector(onButtonActionC), for: .touchUpInside)
        addLabel("Exclusiones del Servicio Funerario")
        scroll.addSubview(buttonC)

        if visibleTextC {
            addGrayLabel("1. Suicidio.\n\n2. Por no haber solicitado el servicio en su momento al Call Center “DAS Contigo”.\n\n3. Por haber solicitado el servicio posterior a las 48 horas de haberse presentado el fallecimiento. No existe reembolso.\n\n4. Fallecimiento a consecuencia de cualquier enfermedad preexistente (CRÓNICO DEGENERATIVA EN FASE TERMINAL O INFECTO CONTAGIOSAS).\n\n5. Si se comprueba que existen falsas declaraciones de los contratantes.\n\n6. Causas de fuerza mayor o caso fortuito que imposibiliten material y humanamente el acceso al lugar del deceso.\n\n7. Catástrofes naturales.")
        } else {
            iconImageCVw.transform = CGAffineTransform(rotationAngle: CGFloat.pi / -2)
        }

        addLabel("ASISTENCIAS ADICIONALES")

        let iconImageD = UIImage(named: "collapsible-icon.png")
        let iconImageDVw = UIImageView(image: iconImageD)
        iconImageDVw.frame = CGRect(x: scroll.frame.width - 40, y: nextY - 8, width: (iconImageD?.size.width)!, height: (iconImageD?.size.height)!)
        scroll.addSubview(iconImageDVw)
        let buttonD = UIButton(frame: CGRect(x: 0, y: nextY - 15, width: scroll.frame.width, height: 40))
        buttonD.addTarget(self, action: #selector(onButtonActionD), for: .touchUpInside)
        addLabel("Asesoría Legal post fallecimiento (Ilimitada vía telefónica).", deltaW: 25)
        scroll.addSubview(buttonD)

        if visibleTextD {
            addGrayLabel("a) Proceso para la cancelación de tarjetas de crédito, debido o departamentales.\n\nb) Proceso para la cancelación de cuentas en redes sociales.\n\nc) Listado de autoridades para iniciar el trámite.\n\nd) Indicaciones para la búsqueda de testamentos.\n\ne) Asesoría para el nombramiento de tutores y albaceas.\n\nf) Asesoría en la supervisión hasta la 1ra etapa del juicio sucesorio.\n\ng) Orientación en la venta o adquisición de bienes heredados, sujetos a copropiedad y/o arrendamiento.")
        } else {
            iconImageDVw.transform = CGAffineTransform(rotationAngle: CGFloat.pi / -2)
        }
        addLabel("Para hacer válido el seguro, debe llamar al Call Center “DAS Contigo” al momento del siniestro, en donde lo canalizarán para recibir la asesoría y soporte necesario.\n\nPara los usuarios que se encuentren activos en la Base de Datos y que hayan pagado las tarifas pactadas durante la vigencia del Contrato.\n\nAsistencia operada por Gavsa.")
        self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: nextY)
    }

    @objc func onButtonActionA() {
        visibleTextA = !visibleTextA
        self.scroll.subviews.forEach { $0.removeFromSuperview() }
        setInitialDataDisplay()
    }

    @objc func onButtonActionB() {
        visibleTextB = !visibleTextB
        self.scroll.subviews.forEach { $0.removeFromSuperview() }
        setInitialDataDisplay()
    }

    @objc func onButtonActionC() {
        visibleTextC = !visibleTextC
        self.scroll.subviews.forEach { $0.removeFromSuperview() }
        setInitialDataDisplay()
    }

    @objc func onButtonActionD() {
        visibleTextD = !visibleTextD
        self.scroll.subviews.forEach { $0.removeFromSuperview() }
        setInitialDataDisplay()
    }

    func addSeparator(margin: CGPoint = CGPoint(x: 0.0, y: 0.0), height: CGFloat = 1.0) {
        let separator = UIView.init(frame: CGRect(x: margin.x, y: margin.y, width: self.view.frame.size.width - (margin.x * 2), height: height))
        separator.backgroundColor = Theme.shared.colors.cellSeparatorColor
        self.scroll.addSubview(separator)
    }

    func addTitleLabel(container:UIView, bigFontSize:Bool = true, under:CGRect? = nil, indent:CGFloat = 0, maxHeight:CGFloat? = 65) -> UILabel {
        let title = self.addLabelWithTag(tag: 101, font: Theme.shared.fonts.detailTitleFont, textColor: .black, container: container, multiline:true)
        if under != nil {
            let finalFrame = CGRect(origin: CGPoint(x: (under?.origin.x)!, y: (under?.origin.y)! + (under?.size.height)! + indent), size: CGSize(width:container.frame.size.width - ((under?.origin.x)! * 2), height: maxHeight! * BasicCellSize.sizeFactor))
            title.frame = finalFrame
            title.fitFontForSize(constrainedSize: title.frame.size, maxFontSize: 80.0, minFontSize: 14.0)
        }
        return title
    }

    func addGrayLabel(_ text: String) {

        let textLayer = UILabel(frame: CGRect(x: margin.x, y: nextY, width: self.view.frame.size.width - (margin.x * 2.5), height: 600))
        textLayer.lineBreakMode = .byWordWrapping
        textLayer.numberOfLines = 0
        textLayer.textAlignment = .justified
        textLayer.textColor = UIColor.gray
        let textContent = text
        let textString = NSMutableAttributedString(string: textContent, attributes: [
            NSAttributedStringKey.font: Theme.shared.fonts.regularFont.withSize(16.0)
            ])
        textLayer.attributedText = textString
        textLayer.sizeToFit()
        scroll.addSubview(textLayer)
        setNextY(view: textLayer)
    }

    func addLabel(_ text: String, deltaW: CGFloat! = 0) {
        let textLayer = UILabel(frame: CGRect(x: margin.x, y: nextY, width: self.view.frame.size.width - (margin.x * 2.5) - deltaW, height: 200))
        textLayer.lineBreakMode = .byWordWrapping
        textLayer.numberOfLines = 0
        textLayer.textAlignment = .justified
        textLayer.textColor = UIColor.black
        let textContent = text
        let textString = NSMutableAttributedString(string: textContent, attributes: [
            NSAttributedStringKey.font: Theme.shared.fonts.regularFont.withSize(16.0)
            ])
        textLayer.attributedText = textString
        textLayer.sizeToFit()
        scroll.addSubview(textLayer)
        setNextY(view: textLayer)
    }

    func setNextY(view: UIView) {
        nextY = view.frame.origin.y + view.frame.height + margin.y
    }

}
