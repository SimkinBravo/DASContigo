//
//  DASBasicTableViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 12/12/17.
//  Copyright © 2017 Last Code. All rights reserved.
//

import UIKit

class DASBasicTableViewController: BasicTableViewController {

    override func viewDidLoad() {
        activityIndicatorType = .ballClipRotate
        activityIndicatorColor = Theme.shared.colors.navigationBarColor3
        super.viewDidLoad()
        customNavigationBar()
    }

    func customNavigationBar() {
        var colors = [UIColor]()
        colors.append(Theme.shared.colors.navigationBarColor1)
        colors.append(Theme.shared.colors.navigationBarColor2)
        colors.append(Theme.shared.colors.navigationBarColor3)
        navigationController?.navigationBar.setGradientBackground(colors: colors)
        if Theme.shared.userLogged {
            let chatImg: UIImage = UIImage(named: "chat")!
            let chatButton = UIBarButtonItem(image: chatImg, style: .plain, target: self, action: #selector(openChat))
            self.navigationItem.rightBarButtonItem = chatButton
        }
    }

    @objc func openChat() {
        if Theme.shared.chatVc == nil {
            Theme.shared.chatVc = storyboard?.instantiateViewController(withIdentifier: "DASChatViewController") as! DASChatViewController
        }
        self.navigationController?.pushViewController(Theme.shared.chatVc, animated:true)
    }

    func setupUnloggedUserScreen(container: UIView) {
        let image = UIImage(named: "no-membership.png")
        let imageVw = UIImageView(image: image)
        imageVw.center = container.center
        imageVw.frame = CGRect(origin: CGPoint(x: imageVw.frame.origin.x, y: imageVw.frame.origin.y - 90), size: imageVw.frame.size)
        container.addSubview(imageVw)
        let indent:CGFloat = 30.0
        let text = UILabel(frame: CGRect(x: indent, y: imageVw.frame.origin.y + imageVw.frame.size.height + 10, width: container.frame.size.width - (indent * 2), height: 100))
        text.text = "Inicia sesión para acceder a esta sección"
        text.numberOfLines = 0
        text.lineBreakMode = .byWordWrapping
        text.textAlignment = .center
        text.textColor = Theme.shared.colors.blueButton
        container.addSubview(text)
        let loginButton = UIButton(type: .custom)
        loginButton.tintColor = .black
        loginButton.frame = CGRect(x: 15, y: text.frame.origin.y + text.frame.size.height + 10, width: self.view.frame.size.width - 30, height: 45)
        loginButton.setTitle("Iniciar sesión", for: .normal)
        loginButton.backgroundColor = Theme.shared.colors.blueButton
        loginButton.layer.cornerRadius = 8.0
        loginButton.clipsToBounds = true
        loginButton.addTarget(self, action: #selector(unloginPressed), for: .touchUpInside)
        container.addSubview(loginButton)

        let contactButton = UIButton(type: .custom)
        contactButton.tintColor = .black
        contactButton.frame = CGRect(x: 15, y: loginButton.frame.origin.y + loginButton.frame.size.height + 20, width: self.view.frame.size.width - 30, height: 45)
        contactButton.setTitle("Contáctanos", for: .normal)
        contactButton.backgroundColor = .white
        contactButton.setTitleColor(Theme.shared.colors.blueButton, for: .normal)
        contactButton.layer.cornerRadius = 8.0
        contactButton.layer.borderColor = Theme.shared.colors.blueButton.cgColor
        contactButton.layer.borderWidth = 1.0
        contactButton.clipsToBounds = true
        contactButton.addTarget(self, action: #selector(contactUsPressed), for: .touchUpInside)
        container.addSubview(contactButton)
    }

    @objc func contactUsPressed() {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        self.navigationController?.pushViewController(vc, animated:true)
    }

    @objc func unloginPressed() {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController")
        self.tabBarController?.navigationController?.setViewControllers([vc], animated: true)
    }

}
