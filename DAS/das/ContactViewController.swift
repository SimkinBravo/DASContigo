//
//  ContactViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 29/01/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit

class ContactViewController: DasBasicViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let label1 = addLabelWithTag(font: Theme.shared.fonts.boldFont.withSize(20.0), textColor: .black, container: self.view, multiline: true)
        label1.text = "Ponte en contacto con DAS"
        label1.textAlignment = .left
        label1.frame = CGRect(x: 20.0, y: (self.navigationController?.navigationBar.frame.size.height)! + 40.0, width: self.view.frame.size.width - 40.0, height: 60)

        let image = UIImage(named: "phone-icon")
        let imageVw = UIImageView(image: image)
        imageVw.frame = CGRect(x: (self.view.frame.width - (image?.size.width)!) * 0.5, y: label1.frame.origin.y + label1.frame.height + 30, width: (image?.size.width)!, height: (image?.size.height)!)
        self.view.addSubview(imageVw)

        let str = "Para mayor información comunícate a nuestro Call Center “DAS Contigo” las 24 horas al 018000222980."
        let attributedString = NSMutableAttributedString(string: str, attributes: [NSAttributedStringKey.font : Theme.shared.fonts.regularFont.withSize(14)])
        attributedString.addAttribute(NSAttributedStringKey.font, value: Theme.shared.fonts.italicFont.withSize(14), range: NSMakeRange(44, 11))

        let label3 = addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(16.0), textColor: .black, container: self.view, multiline: true)
        label3.attributedText = attributedString
//        label3.text = "Para mayor información comunícate a nuestro Call Center “DAS Contigo” las 24 horas al (0155) 91490727."
        label3.textAlignment = .center
        label3.frame = CGRect(x: 20.0, y: label1.frame.origin.y + 250.0, width: self.view.frame.size.width - 40.0, height: 80)
/*
        let label4 = addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(16.0), textColor: .black, container: self.view, multiline: true)
        label4.text = "Agregar o cambiar adicionales a tu membresía."
        label4.numberOfLines = 0
        label4.lineBreakMode = .byWordWrapping
        label4.textAlignment = .left
        label4.frame = CGRect(x: 30.0, y: label3.frame.origin.y + 50.0, width: self.view.frame.size.width - 40.0, height: 50)

        let label5 = addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(16.0), textColor: .black, container: self.view, multiline: true)
        label5.text = "Contratar coberturas adicionales."
        label5.textAlignment = .left
        label5.frame = CGRect(x: 30.0, y: label4.frame.origin.y + 50.0, width: self.view.frame.size.width - 40.0, height: 50)

        let label6 = addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(16.0), textColor: .black, container: self.view, multiline: true)
        label6.text = "Asistencia y orientación."
        label6.textAlignment = .left
        label6.frame = CGRect(x: 30.0, y: label5.frame.origin.y + 50.0, width: self.view.frame.size.width - 40.0, height: 50)
*/
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height * 0.6))
        button.addTarget(self, action: #selector(callEmergency), for: .touchUpInside)
        self.view.addSubview(button)

    }

    @objc func callEmergency() {
        guard let number = URL(string: "tel://018000222980") else { return }
        UIApplication.shared.open(number)
    }

}
