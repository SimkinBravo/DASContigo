//
//  CatalogTableViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 05/01/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

enum MedicServicesCatalogType {
    case states
    case region
    case medicServiceType
    case medicServiceSpeciality
}

class CatalogTableViewController: DASBasicTableViewController {

    var catalogType: MedicServicesCatalogType = .states

    override func viewDidLoad() {
        super.viewDidLoad()
        let backImg: UIImage = UIImage(named: "header-back-button")!
        let backButton = UIBarButtonItem(image: backImg, style: .plain, target: self, action: #selector(BasicDetailViewController.backAction))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.setBackgroundVerticalPositionAdjustment(-10.0, for: .default)
    }

    @objc func backAction() -> Void {
        switch catalogType {
            case .states:
            break
            default:            print("lalal")
        }
        dismiss(animated: true, completion: nil)
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
        }
    }

    func doubleDismiss() {
        if self.navigationController != nil {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        } else {
            let presentingViewController = self.presentingViewController
            dismiss(animated: false, completion: {
                presentingViewController!.dismiss(animated: true, completion: nil)
            })
        }
    }

    override func tableViewDidSelectData(data: BasicCellData) {
        super.tableViewDidSelectData(data: data)
        switch catalogType {
            case .states:
                if Theme.shared.app.stateId == nil || Theme.shared.app.stateId != data.slug {
                    Theme.shared.app.stateId = data.slug
                    Theme.shared.app.state = data.title
                }
                Theme.shared.app.specialtyId = ""
                Theme.shared.app.specialty = ""
                Theme.shared.app.typeId = ""
                Theme.shared.app.type = ""
                Theme.shared.app.region = ""
                Theme.shared.app.regionId = ""
                if data.slug == "0" {
                    Theme.shared.app.state = "Todos los estados"
                    backAction()
                } else {
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "CatalogTableViewController") as! CatalogTableViewController
                    vc.catalogType = .region
                    self.navigationController?.pushViewController(vc, animated:true)
                }
            case .region:
                Theme.shared.app.regionId = data.slug
                Theme.shared.app.region = data.title
                doubleDismiss()
            case .medicServiceType:
                Theme.shared.app.typeId = data.slug
                Theme.shared.app.type = data.title
                Theme.shared.app.specialtyId = ""
                Theme.shared.app.specialty = ""
                if data.slug == "0" {
                    Theme.shared.app.type = "Todos los tipos"
                    backAction()
                } else {
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "CatalogTableViewController") as! CatalogTableViewController
                    vc.catalogType = .medicServiceSpeciality
                    self.navigationController?.pushViewController(vc, animated:true)
                }
            case .medicServiceSpeciality:
                Theme.shared.app.specialtyId = data.slug
                Theme.shared.app.specialty = data.title
                doubleDismiss()
        }
    }

    override func loadData() {
        postCatalog()
//        self.elements.append(catalog)
//        self.onLoadDataResponse()
    }

    func postCatalog() {
        print("\(Urls.base)catalog", catalogType, getCatalogParameters())
        Alamofire.request("\(Urls.base)catalog", method: .post, parameters: getCatalogParameters(), encoding: JSONEncoding.default).responseJSON { response in
            if response.result.value as? [AnyObject] != nil {
                print("lalala")
            } else if response.result.value != nil {
                let rawResponse = response.result.value as! [String: AnyObject]
                var parsed = self.parseCatalogRawResponse(raw: rawResponse)
                parsed = parsed.sorted { $0.index < $1.index }
                self.elements.append(parsed)
                self.onLoadDataResponse()
            } else {
                let statusCode = response.response?.statusCode
                print("statusCode", statusCode)
                self.showError(error: "Error al conectar al servidor, revise su conexión a Internet")
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)
            }
        }
    }

    func showError(error: String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { action in
            self.backAction()
        })
        alert.addAction(action)
        self.present(alert, animated: true, completion:{})
    }

    func parseCatalogRawResponse(raw: [String: AnyObject])-> [BasicCellData] {
        var parsed = [BasicCellData]()
        switch catalogType {
            case .states:           parsed.append(BasicCellData(slug: "0", title: "Todos los estados", subtitle: "").configCatalogCell())
            case .region:           parsed.append(BasicCellData(slug: "0", title: "Todos los municipios", subtitle: "").configCatalogCell())
            case .medicServiceType: parsed.append(BasicCellData(slug: "0", title: "Todos los tipos", subtitle: "").configCatalogCell())
            case .medicServiceSpeciality:   parsed.append(BasicCellData(slug: "0", title: "Todas las especialidades", subtitle: "").configCatalogCell())
        }
        for key in raw.keys {
            parsed.append(BasicCellData(slug: key, title: raw[key] as! String, subtitle: "").configCatalogCell())
        }
        return parsed
    }

    func getCatalogParameters()-> [String:String] {
        switch catalogType {
            case .states:       return ["catalog": "states"]
            case .region:       return ["catalog": "region", "statesId": Theme.shared.app.stateId]
            case .medicServiceType:
                var param = [String:String]()
                param["statesId"] = Theme.shared.app.stateId
                param["catalog"] = "type"
                if Theme.shared.app.regionId != "0" && !Theme.shared.app.regionId.isEmpty {
                    param["regionId"] = Theme.shared.app.regionId
                }
                return param
            case .medicServiceSpeciality:
                var param = [String:String]()
                param["statesId"] = Theme.shared.app.stateId
                param["catalog"] = "speciatity"
                param["typeId"] = Theme.shared.app.typeId
                if Theme.shared.app.regionId != "0" && !Theme.shared.app.regionId.isEmpty  {
                    param["regionId"] = Theme.shared.app.regionId
                }
                return param
//            default:            return [:]
        }
    }

}
