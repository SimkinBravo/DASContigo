//
//  DirectoryViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 05/01/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import MapKit
import NVActivityIndicatorView

class DirectoryViewController: DasBasicViewController, UITextFieldDelegate, CLLocationManagerDelegate {

    let search = UITextField()
    let mapView = MKMapView()
    let panel = UIView()

    var locationA: UILabel!
    var dType: UILabel!
    var dTypeA: UILabel!
    var searchButton: UIActivityButton!
    var statesCatalog: [BasicCellData]!
    var userLocationState: String!
    var userLocationRegion: String!
    var isLoading: Bool = false
    var activityIndicator: NVActivityIndicatorView!
    var serviceTypesTemp = [String: [BasicCellData]]()
    var tempStateId: String!

    private var locationManager: CLLocationManager = CLLocationManager()
    private var currentLocation: CLLocation?

    override func viewDidLoad() {
        super.viewDidLoad()
        Theme.shared.app.stateId = "0"
        self.navigationItem.title = "Directorio"
        customNavigationBar()
        if !Theme.shared.userLogged {
            setupUnloggedUserScreen(container: self.view)
            return
        }

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)

        search.frame = CGRect(x: 15, y: 15, width: self.view.frame.size.width - 30, height: 45)
        search.placeholder = "Nombre de médico o servicio"
        search.font = UIFont.systemFont(ofSize: 15)
        search.borderStyle = UITextBorderStyle.roundedRect
        search.keyboardType = .alphabet
        search.clearButtonMode = .whileEditing
        search.contentVerticalAlignment = .center
        search.delegate = self
        search.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        panel.addSubview(search)
        addSeparator(margin: CGPoint(x: 0.0, y: 70), at: panel)

        let location = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: panel)
        location.frame = CGRect(x: 15.0, y: 70, width: view.frame.size.width - 30, height: 50)
        location.text = "Ubicación"
        locationA = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: Theme.shared.colors.textGray, container: panel)
        locationA.frame = CGRect(x: 85.0, y: 70, width: view.frame.size.width - 100, height: 50)
        locationA.textAlignment = .right
        locationA.text = "Buscando ubicación"

        let locationButton = UIButton(type: .custom)
        locationButton.frame = CGRect(x: 15, y: 70, width: self.view.frame.size.width - 30, height: 50)
        locationButton.clipsToBounds = true
        locationButton.addTarget(self, action: #selector(locationPressed), for: .touchUpInside)
        panel.addSubview(locationButton)

        addSeparator(margin: CGPoint(x: 0.0, y: 120), at: panel)

        dType = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: panel)
        dType.frame = CGRect(x: 15.0, y: 120, width: view.frame.size.width - 30, height: 50)
        dType.text = "Tipo"
        dTypeA = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: Theme.shared.colors.textGray, container: panel)
        dTypeA.frame = CGRect(x: 50.0, y: 120, width: view.frame.size.width - 65, height: 50)
        dTypeA.textAlignment = .right
        dTypeA.text = "Todos los tipos"

        let typeButton = UIButton(type: .custom)
        typeButton.frame = CGRect(x: 15, y: 120, width: self.view.frame.size.width - 30, height: 50)
        typeButton.clipsToBounds = true
        typeButton.addTarget(self, action: #selector(typePressed), for: .touchUpInside)
        panel.addSubview(typeButton)

        addSeparator(margin: CGPoint(x: 0.0, y: 170), at: panel)

        searchButton = UIActivityButton(type: .custom)
        searchButton.tintColor = .black
        searchButton.frame = CGRect(x: 15, y: 180, width: self.view.frame.size.width - 30, height: 45)
        searchButton.setTitle("Buscar", for: .normal)
        searchButton.backgroundColor = Theme.shared.colors.blueButton
        searchButton.layer.cornerRadius = 5.0
        searchButton.clipsToBounds = true
        searchButton.addTarget(self, action: #selector(searchPressed), for: .touchUpInside)
        searchButton.alpha = 0.3
        searchButton.isEnabled = false

        panel.addSubview(searchButton)

        let panelHeight = searchButton.frame.origin.y + searchButton.frame.size.height + 60
        panel.frame = CGRect(x: 0, y: self.view.frame.size.height - panelHeight, width: view.frame.size.width, height: panelHeight)
        self.view.addSubview(panel)

        updateMapFrame()
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.showsUserLocation = true
        view.addSubview(mapView)
        setupActivityIndicator()

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest

        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }

    func getAdressName(coords: CLLocation) {
        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
            if error != nil {
                print("Hay un error")
            } else {
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    let adressString : String = ""
                    if place.locality != nil {
                        self.userLocationState = place.locality?.folding(options: .diacriticInsensitive, locale: .current).lowercased()
                        self.tempStateId = adressString + place.locality!
                    }
                    if place.subAdministrativeArea != nil {
//                        self.userLocationRegion = place.subAdministrativeArea
//                        Theme.shared.app.region = place.subAdministrativeArea!
                    }
                    self.updateLocationTitle()
                    self.locationA.text = "Selecciona tu ubicación"
                    self.postCatalog()
                }
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        if currentLocation == nil {
            if let userLocation = locations.last {
                currentLocation = userLocation
                let viewRegion = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 2000, 2000)
                mapView.setRegion(viewRegion, animated: false)
//                print(userLocation.coordinate.latitude, userLocation.coordinate.longitude)
                self.postMapPoints()
                getAdressName(coords: userLocation)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if !Theme.shared.userLogged {
            return
        }
        updateLocationTitle()
    }

    func updateLocationTitle() {
        let stateIsEmpty = Theme.shared.app.state.isEmpty || Theme.shared.app.stateId == "0"
        if !stateIsEmpty {
            locationA.text = Theme.shared.app.state + (Theme.shared.app.region != "" ? ", " + Theme.shared.app.region : "")
            locationA.fitFontForSize(constrainedSize: locationA.frame.size, maxFontSize: 14.0, onlyWidth: true)
        } else {
            locationA.text = "Todos los estados"
        }
        if !Theme.shared.app.type.isEmpty {
            dTypeA.text = Theme.shared.app.type + (Theme.shared.app.specialty != "" ? ", " + Theme.shared.app.specialty : "")
            dTypeA.fitFontForSize(constrainedSize: dTypeA.frame.size, maxFontSize: 14.0, onlyWidth: true)
        } else {
            dTypeA.text = "Todos los tipos"
        }
        dType.alpha = stateIsEmpty ? 0.3 : 1.0
        dTypeA.alpha = stateIsEmpty ? 0.3 : 1.0
//        searchButton.alpha = searchEnabled ? 1.0 : 0.3
//        searchButton.isEnabled = searchEnabled
    }

    @objc func searchPressed() {
        if isLoading {
            return
        }
        dismissKeyboard()
        searchButton.isLoading(true)
        postSearch()
    }

    @objc func textFieldDidChange(textField: UITextField) {
        updateLocationTitle()
    }

    func postSearch() {
        var formParams:[String:String] = [:]
        if !(search.text?.isEmpty)! {
            formParams["keyword"] = search.text!
        }
        if  Theme.shared.app.stateId != "0" && !Theme.shared.app.stateId.isEmpty {
            formParams["stateId"] =  Theme.shared.app.stateId
        }
        if Theme.shared.app.regionId != "0" && !Theme.shared.app.regionId.isEmpty {
            formParams["regionId"] = Theme.shared.app.regionId
        }
        if Theme.shared.app.typeId != "0" && !Theme.shared.app.typeId.isEmpty {
            formParams["typeId"] = Theme.shared.app.typeId
        }
        if Theme.shared.app.specialtyId != "0" && !Theme.shared.app.specialtyId.isEmpty {
            formParams["specialtyId"] = Theme.shared.app.specialtyId
        }
        postSearch(formParams: formParams)
    }

    func postSearch(formParams: [String: String]) {
        if isLoading {
            return
        }
        self.isLoading = true
        print(formParams)
        Alamofire.request("\(Urls.base)search", method: .post, parameters: formParams, encoding: JSONEncoding.default).responseJSON { response in
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            self.isLoading = false
            self.searchButton.isLoading(false)
            print(response.result.value)
            if response.result.value != nil {
                if let rawResponse = response.result.value as? [String: AnyObject] {
                    let content = rawResponse["content"] as! [[String: AnyObject]]
                    var parsed = [BasicCellData]()
                    self.serviceTypesTemp = [String: [BasicCellData]]()
                    for rawItem in content {
                        let data = BasicCellData(index: parsed.count, searchRawItem: rawItem)
                        self.addServiceData(data: data)
                    }
                    let serviceTypes = self.serviceTypesTemp.sorted(by: { $0.key < $1.key })
                    for serviceType in serviceTypes {
                        var specialtiesTemp = [String: [BasicCellData]]()
                        for serviceData in serviceType.value {
                            if specialtiesTemp[serviceData.specialty] == nil {
                                specialtiesTemp[serviceData.specialty] = [BasicCellData]()
                            }
                            specialtiesTemp[serviceData.specialty]?.append(serviceData)
                        }
                        let specialties = specialtiesTemp.sorted(by: { $0.key < $1.key })
                        parsed.append(BasicCellData(title: serviceType.key).setupSectionSeparatorCell(blueBackground: true))
                        for specialty in specialties {
                            parsed.append(BasicCellData(title: specialty.key).setupSectionSeparatorCell())
                            parsed.append(contentsOf: specialty.value)
                        }
                    }
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "DirectorySearchResultTableViewController") as! DirectorySearchResultTableViewController
                    vc.items = parsed
                    self.navigationController?.pushViewController(vc, animated:true)
                } else {
                    self.showEmptyResult()
                }
            } else {
                let statusCode = response.response?.statusCode
                print("statusCode", statusCode)
                if statusCode == 200 {
                    self.showEmptyResult()
                } else {
                    self.showError(error: "Error al conectar al servidor, revise su conexión a Internet")
                }
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)
            }
        }
    }

    func showEmptyResult() {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "DirectorySearchResultTableViewController") as! DirectorySearchResultTableViewController
        vc.items = [BasicCellData]()
        self.navigationController?.pushViewController(vc, animated:true)
    }

    func showError(error: String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion:{})
    }

    func addServiceData(data: BasicCellData) {
        if serviceTypesTemp[data.serviceType] == nil {
            serviceTypesTemp[data.serviceType] = [BasicCellData]()
        }
        var isDataRegistered: Bool = false
        if let currentArray = serviceTypesTemp[data.serviceType] {
            for registerData in currentArray {
                if registerData.slug == data.slug {
                    if !isDataRegistered && !registerData.specialities.contains(data.specialty) {
                        registerData.specialities.append(data.specialty)
                    }
                    isDataRegistered = true
                }
            }
        }
        if !isDataRegistered {
            serviceTypesTemp[data.serviceType]?.append(data)
        }
    }

    func postCatalog() {
        if Theme.shared.app.stateId != nil || isLoading {
            return
        }
        print("\(Urls.base)catalog", getCatalogParameters())
        Alamofire.request("\(Urls.base)catalog", method: .post, parameters: getCatalogParameters(), encoding: JSONEncoding.default).responseJSON { response in
            self.isLoading = false
            if response.result.value != nil {
                let rawResponse = response.result.value as! [String: AnyObject]
                let parsed = self.parseCatalogRawResponse(raw: rawResponse)
                self.statesCatalog = parsed.sorted { $0.index < $1.index }
                for row in parsed {
                    if self.userLocationState == row.title.lowercased() {
                        Theme.shared.app.state = self.tempStateId
                        Theme.shared.app.stateId = row.slug
                        self.postSearch()
                        return
                    }
                }
            }
            self.locationA.text = "Selecciona tu úbicación"
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }
    }

    func postMapPoints() {
        if isLoading {
            return
        }
        let formParams: [String: AnyObject] = ["lat": currentLocation?.coordinate.latitude as AnyObject, "lng": currentLocation?.coordinate.longitude as AnyObject, "dKm": 3 as AnyObject]
        self.isLoading = true
        Alamofire.request("\(Urls.base)location", method: .post, parameters: formParams, encoding: JSONEncoding.default).responseJSON { response in
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            self.isLoading = false
            self.searchButton.isLoading(false)
            if let rawResponse = response.result.value as? [String: AnyObject] {
                let content = rawResponse["content"] as! [[String: AnyObject]]
                var parsed = [BasicCellData]()
                for rawItem in content {
                    let data = BasicCellData(index: parsed.count, searchRawItem: rawItem)
                    parsed.append(data)
                    if data.location != nil {
                        let annotation = MKPointAnnotation()
                        annotation.coordinate = data.location
                        annotation.title = data.title
                        self.mapView.addAnnotation(annotation)
                    }
                }
            } else {
                print(response.result.value ?? "asdf")
            }
        }
    }

    func parseCatalogRawResponse(raw: [String: AnyObject])-> [BasicCellData] {
        var parsed = [BasicCellData]()
        for key in raw.keys {
            parsed.append(BasicCellData(slug: key, title: raw[key] as! String, subtitle: "").configCatalogCell())
        }
        return parsed
    }

    func getCatalogParameters() -> [String:String] {
        return ["catalog": "states"]
    }

    @objc func locationPressed() {
        dismissKeyboard()
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "CatalogTableViewController") as! CatalogTableViewController
        self.navigationController?.pushViewController(vc, animated:true)
    }

    @objc func typePressed() {
        if Theme.shared.app.state.isEmpty || Theme.shared.app.stateId == "0" {
            return
        }
        dismissKeyboard()
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "CatalogTableViewController") as! CatalogTableViewController
        vc.catalogType = .medicServiceType
        self.navigationController?.pushViewController(vc, animated:true)
    }

    @objc func dismissKeyboard() {
        print("dismissKeyboard")
        self.view.endEditing(true)
    }

    func customNavigationBar() {
        var colors = [UIColor]()
        colors.append(UIColor(red: 67/255, green: 115/255, blue: 164/255, alpha: 1))
        colors.append(UIColor(red: 50/255, green: 82/255, blue: 129/255, alpha: 1))
        colors.append(UIColor(red: 40/255, green: 59/255, blue: 104/255, alpha: 1))
        navigationController?.navigationBar.setGradientBackground(colors: colors)

        if Theme.shared.userLogged {
            let chatImg: UIImage = UIImage(named: "chat")!
            let chatButton = UIBarButtonItem(image: chatImg, style: .plain, target: self, action: #selector(openChat))
            self.navigationItem.rightBarButtonItem = chatButton
        }

    }
    
    @objc func openChat() {
        if Theme.shared.chatVc == nil {
            Theme.shared.chatVc = storyboard?.instantiateViewController(withIdentifier: "DASChatViewController") as! DASChatViewController
        }
        self.navigationController?.pushViewController(Theme.shared.chatVc, animated:true)
    }

    func getCatalogDataWith(title: String, desc: String, big: Bool = false) -> BasicCellData {
        let data = BasicCellData(title: title, subtitle: desc)
        data.cellSize = big ? BasicCellSize.separatorS : BasicCellSize.separator
        data.titleHeightPercent = 1.0
        data.contentStyle = .vertical
        data.cellId = .basic
        data.isImageHidden = true
        data.textOffset = 10.0
        data.textMargin = CGPoint(x: 15.0, y: 5.0)
        data.titleFont = Theme.shared.fonts.regularFont.withSize(16.0)
        return data
    }

    var originalY: CGFloat = 0

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            originalY = panel.frame.origin.y
            print("keyboardWillShow", originalY, keyboardSize.height)
            panel.frame.origin.y -= (keyboardSize.height - 50)
            updateMapFrame()
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            print("keyboardWillHide", originalY, keyboardSize.height)
            if originalY != 0 {
                panel.frame.origin.y = originalY
                updateMapFrame()
            }
        }
    }

    

    func updateMapFrame() {
        mapView.frame = CGRect(x: 0, y: 60, width: view.frame.size.width, height: panel.frame.origin.y - 60)
    }

    func setupActivityIndicator() {
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 10, y: (self.navigationController?.navigationBar.frame.height)! + 30, width: 30, height: 30), type: NVActivityIndicatorType.ballClipRotate, color: Theme.shared.colors.navigationBarColor3, padding: 0)
        self.view.addSubview(activityIndicator)
        self.activityIndicator.startAnimating()
    }

}
