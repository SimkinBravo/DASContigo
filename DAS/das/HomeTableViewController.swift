//
//  HomeTableViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 04/01/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

class HomeTableViewController: DASBasicTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "DAS Contigo"
    }

    override func loadData() {
        if !Theme.shared.userLogged {
            setBenefitForUnloggedUser()
        }
        loadNews()
    }

    func loadNews() {
        print("\(Urls.base)feed")
        Alamofire.request("\(Urls.base)feed", method: .post, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.value as? [AnyObject] != nil {
                print("lalala")
            } else if response.result.value != nil {
                let rawResponse = response.result.value as! [String: AnyObject]
                let status = (rawResponse["status"] as! CFBoolean) == kCFBooleanTrue
                if status {
                    let content = rawResponse["data"] as! NSArray
                    var temp = [BasicCellData]()
                    for (index, element) in content.enumerated() {
                        if index < 5 {
                            temp.append(BasicCellData.init(index: index, newsDasApi: element as! [String : AnyObject]))
                        }
                    }
                    let data = BasicCellData.init(slug: "")
                    data.hCollection = temp
                    data.cellReuseID = BasicCellID.collection
                    data.cellSize = BasicCellSize.collection
                    let separator = BasicCellData.init(slug: "news", title: "Noticias", subtitle: "Ver todos").setupSeparatorTitleCell()
                    separator.isSeparatorHidden = true
                    if !Theme.shared.userLogged {
                        self.elements.append([self.getBlankRow(), separator, data])
                    } else {
                        self.elements.append([separator, data])
                    }
                } else {
                    print("error")
                }
                self.loadPromos()
            } else {
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)
            }
        }
    }

    func getBlankRow() -> BasicCellData {
        return BasicCellData(title: "").setupSectionSeparatorCell(grayBackground: false, isSeparatorHidden: true)
    }

    func loadPromos() {
        if !Theme.shared.userLogged {
            self.onLoadDataResponse()
            return
        }
        print("\(Urls.base)promociones")
        let formParams:[String:String] = ["id": Theme.shared.user.affiliateNumber]
        Alamofire.request("\(Urls.base)promociones", method: .post, parameters: formParams, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.value != nil {
                let rawResponse = response.result.value as! [[String: AnyObject]]
                var temp = [BasicCellData]()
                temp.append(self.getBlankRow())
                temp.append(BasicCellData.init(slug: "promo", title: "Promociones", subtitle: "Ver todas").setupSeparatorTitleCell(isSeparatorHidden: false))
                for row in rawResponse {
                    if temp.count < 5 {
                        temp.append(BasicCellData.init(index: temp.count, promoDasApi: row))
                    }
                }
                self.elements.append(temp)
            } else {
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)
            }
            self.onLoadDataResponse()
        }
    }

    override func onLoadDataResponse() {
        if Theme.shared.userLogged {
            setBenefitForLoggedUser()
        }
        self.elements.append([BasicCellData(title: "").setupSectionSeparatorCell(grayBackground: false),
                              BasicCellData(slug: "pp", title: "Políticas de privacidad").setupLinkedCell()])
        super.onLoadDataResponse()
    }

    override func tableViewDidSelectData(data: BasicCellData) {
        super.tableViewDidSelectData(data: data)
        if data.contentStyle != nil && data.contentStyle == .sectionSeparator {
            if data.slug == "news" {
                let navigationC = self.storyboard!.instantiateViewController(withIdentifier: "NewsTableNavigationController") as! UINavigationController
                let vc = navigationC.viewControllers.first! as! NewsTableViewController
                self.navigationController?.pushViewController(vc, animated:true)
            } else {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "PromosTableViewController") as! PromosTableViewController
                self.navigationController?.pushViewController(vc, animated:true)
            }
        }
        if data.sectionSlug == "news" {
            let navigationC = self.storyboard!.instantiateViewController(withIdentifier: "NewsDetailNavigationController") as! UINavigationController
            let vc = navigationC.viewControllers.first! as! NewsDetailViewController
            vc.ownerData = self.data
            vc.item = data
            self.navigationController?.pushViewController(vc, animated:true)
        }
        if data.sectionSlug == "promo" {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "PromoDetailViewController") as! PromoDetailViewController
            vc.ownerData = self.data
            vc.item = data
            self.navigationController?.pushViewController(vc, animated:true)
        }
        if data.contentStyle == .custom {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "MembershipTableViewController") as! MembershipTableViewController
            self.navigationController?.pushViewController(vc, animated:true)
        }
        if data.slug == "pp" {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "PrivacyViewController") as! BasicWebViewController
            vc.url = "https://dascontigo.com.mx/politicas-de-privacidad.pdf"
            vc.webviewScalesToFit = true
            self.navigationController?.pushViewController(vc, animated:true)
        }
        if data.slug == "funeral" {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "FuneralServicesViewController")
            self.navigationController?.pushViewController(vc, animated:true)
        }
        if data.slug == "insurance" {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "AccidentInsuranceViewController")
            self.navigationController?.pushViewController(vc, animated:true)
        }

    }

    func setBenefitForUnloggedUser() {
        var data = [BasicCellData]()
        data.append(BasicCellData(slug: "benefits", title: "Nuestras Membresías", subtitle: "").setupSeparatorTitleCell())
        data.append(BasicCellData(title: "").setupDoubleColumnCell(text1: "Master\n(individual)", text2: "Premium\n(familiar)"))
        data.append(BasicCellData(title: "Precio").setupDoubleColumnCell(text1: "$500", text2: "$1,900"))
        data.append(BasicCellData(title: "Membresía para un Titular").setupDoubleIconBenefitCell())
        data.append(BasicCellData(title: "Cobertura para adicionales").setupDoubleIconBenefitCell(haveCross: true))
        data.append(BasicCellData(title: "Centro de Atención Médica Telefónica “DAS Contigo” las 24 horas, los 365 días del año").setupDoubleIconBenefitCell())
        data.append(BasicCellData(title: "Centro de Atención Nutricional “DAS Contigo”").setupDoubleIconBenefitCell())
        data.append(BasicCellData(title: "Consultas médicas generales y especialistas a precios estandarizados ($300 y $400)").setupDoubleIconBenefitCell())
        data.append(BasicCellData(title: "Descuentos en medicamentos").setupDoubleIconBenefitCell())
        data.append(BasicCellData(title: "Descuentos en laboratorios médicos y otros servicios de hasta 50%").setupDoubleIconBenefitCell())
        data.append(BasicCellData(title: "Ambulancia SIN COSTO en urgencias médicas, sin límite de eventos").setupDoubleIconBenefitCell())
        let check = BasicCellData(title: "Check Up general a precio preferencial").setupDoubleIconBenefitCell()
        let str = "Check Up general a precio preferencial"
        let attributedString = NSMutableAttributedString(string: str, attributes: [NSAttributedStringKey.font : Theme.shared.fonts.regularFont])
        attributedString.addAttribute(NSAttributedStringKey.font, value: Theme.shared.fonts.italicFont, range: NSMakeRange(0, 8))
        check.attachmentString = attributedString as NSAttributedString
        data.append(check)
        data.append(BasicCellData(title: "Consulta de medicina general a domicilio a precio preferencial (consultar disponibilidad)").setupDoubleIconBenefitCell())
        data.append(BasicCellData(title: "SERVICIOS ADICIONALES").setupSectionSeparatorCell())
        let cell = BasicCellData(slug: "insurance", title: "Seguro de Accidentes Personales para el Titular").setupLinkedCell()
        cell.titleFont = Theme.shared.fonts.regularFont.withSize(UIDevice.current.screenType == .iPhones_5_5s_5c_SE ? 12 : 14)
        data.append(cell)
        data.append(BasicCellData(slug: "funeral", title: "Seguro de Asistencia Funeraria").setupLinkedCell())
        self.elements.append(data)
        super.onLoadDataResponse()
    }

    func setBenefitForLoggedUser() {
        var data = [BasicCellData]()
        data.append(self.getBlankRow())
        let title = Theme.shared.user.isAditional ? "Membresía Adicional" : (Theme.shared.user.membershipType == .master ? "Membresía Master (Individual)" : "Membresía Premium (Familiar)")
        data.append(BasicCellData(title: title).setupSeparatorTitleCell(isSeparatorHidden: false))
        data.append(BasicCellData(title: "Membresía para un Titular").setupIconBenefitCell())
        if Theme.shared.user.membershipType == .premium {
            data.append(BasicCellData(title: "Cobertura para adicionales").setupIconBenefitCell(haveCross: Theme.shared.user.isAditional))
        }
        data.append(BasicCellData(title: "Centro de Atención Médica Telefónica “DAS Contigo” las 24 horas, los 365 días del año").setupIconBenefitCell())
        data.append(BasicCellData(title: "Centro de Atención Nutricional “DAS Contigo”").setupIconBenefitCell())
        data.append(BasicCellData(title: "Consultas médicas generales y especialistas a precios estandarizados ($300 y $400)").setupIconBenefitCell())
        data.append(BasicCellData(title: "Descuentos en medicamentos").setupIconBenefitCell())
        data.append(BasicCellData(title: "Descuentos en laboratorios médicos y otros servicios de hasta 50%").setupIconBenefitCell())
        data.append(BasicCellData(title: "Ambulancia SIN COSTO en urgencias médicas, sin límite de eventos").setupIconBenefitCell())
        let check = BasicCellData(title: "Check Up general a precio preferencial").setupIconBenefitCell()
        let str = "Check Up general a precio preferencial"
        let attributedString = NSMutableAttributedString(string: str, attributes: [NSAttributedStringKey.font : Theme.shared.fonts.regularFont])
        attributedString.addAttribute(NSAttributedStringKey.font, value: Theme.shared.fonts.italicFont, range: NSMakeRange(0, 8))
        check.attachmentString = attributedString as NSAttributedString
        data.append(check)
        data.append(BasicCellData(title: "Consulta de medicina general a domicilio a precio preferencial (consultar disponibilidad)").setupIconBenefitCell())
        if !Theme.shared.user.isAditional {
            if Theme.shared.user.haveAccidentInsurance || Theme.shared.user.haveFuneralService {
                data.append(BasicCellData(title: "SERVICIOS ADICIONALES:").setupSectionSeparatorCell())
            }
            if Theme.shared.user.haveAccidentInsurance {
                let cell = BasicCellData(slug: "insurance", title: "Seguro de Accidentes Personales para el Titular").setupLinkedCell()
                cell.titleFont = Theme.shared.fonts.regularFont.withSize(UIDevice.current.screenType == .iPhones_5_5s_5c_SE ? 11 : 13)
                data.append(cell)
            }
            if Theme.shared.user.haveFuneralService {
                data.append(BasicCellData(slug: "funeral", title: "Seguro de Asistencia Funeraria").setupLinkedCell())
            }
        }
        self.elements.append(data)
        super.onLoadDataResponse()
    }

}
