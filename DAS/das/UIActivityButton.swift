//
//  UIActivityButton.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 13/02/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class UIActivityButton: UIButton {

    var activityIndicator: NVActivityIndicatorView!

    override func draw(_ rect: CGRect) {
        setupActivity()
    }

    func setupActivity() {
        if activityIndicator != nil {
            return
        }
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: frame.width - frame.height, y: 5, width: frame.height - 10, height: frame.height - 10), type: NVActivityIndicatorType.ballClipRotate, color: Theme.shared.colors.navigationBarColor3, padding: 0)
        addSubview(activityIndicator)
        activityIndicator.isHidden = true
    }

    func isLoading(_ isLoading: Bool) {
        setupActivity()
        if isLoading {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
        activityIndicator.isHidden = !isLoading
        isEnabled = !isLoading
        alpha = isLoading ? 0.8 : 1
    }

}
