//
//  PromosTableViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 04/01/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

class PromosTableViewController: DASBasicTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Promociones"
        let backImg: UIImage = UIImage(named: "header-back-button")!
        let backButton = UIBarButtonItem(image: backImg, style: .plain, target: self, action: #selector(BasicDetailViewController.backAction))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.setBackgroundVerticalPositionAdjustment(-10.0, for: .default)
    }
    
    @objc func backAction() -> Void {
        dismiss(animated: true, completion: nil)
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
        }
    }

    override func loadData() {
        loadPromos()
    }

    func loadPromos() {
        print("\(Urls.base)promociones")
        let formParams:[String:String] = ["id": Theme.shared.user.affiliateNumber]
        Alamofire.request("\(Urls.base)promociones", method: .post, parameters: formParams, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.value != nil {
                let rawResponse = response.result.value as! [[String: AnyObject]]
                var temp = [BasicCellData]()
                for row in rawResponse {
                    temp.append(BasicCellData.init(index: temp.count, promoDasApi: row))
                }
                self.elements.append(temp)
                self.onLoadDataResponse()
            } else {
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)
            }
        }
    }

    override func tableViewDidSelectData(data: BasicCellData) {
        super.tableViewDidSelectData(data: data)
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "PromoDetailViewController") as! PromoDetailViewController
        vc.ownerData = self.data
        vc.item = data
        self.navigationController?.pushViewController(vc, animated:true)
    }

}
