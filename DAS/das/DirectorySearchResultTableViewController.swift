//
//  DirectorySearchResultTableViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 06/02/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit

class DirectorySearchResultTableViewController: DASBasicTableViewController {

    var items:[BasicCellData]!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Resultados"
    }

    override func loadData() {
        self.elements = []
        if items.count == 0 {
            let back = UIView(frame: self.tableView.frame)
            self.tableView.backgroundView = back
            setupEmptyAgenda(container: back)
        }
        self.elements.append(items)
        self.onLoadDataResponse()
    }

    func setupEmptyAgenda(container: UIView) {
        let image = UIImage(named: "no-membership.png")
        let imageVw = UIImageView(image: image)
        imageVw.center = container.center
        imageVw.frame = CGRect(origin: CGPoint(x: imageVw.frame.origin.x, y: imageVw.frame.origin.y - 50), size: imageVw.frame.size)
        container.addSubview(imageVw)
        let indent:CGFloat = 40.0
        let text = UILabel(frame: CGRect(x: indent, y: imageVw.frame.origin.y + imageVw.frame.size.height + 10, width: container.frame.size.width - (indent * 2), height: 100))
        text.text = "No hay resultados"
        text.numberOfLines = 0
        text.lineBreakMode = .byWordWrapping
        text.textAlignment = .center
        text.textColor = Theme.shared.colors.blueButton
        container.addSubview(text)
    }

    override func tableViewDidSelectData(data: BasicCellData) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "MedicalServiceDetailViewController") as! MedicalServiceDetailViewController
        vc.item = data
        self.navigationController?.pushViewController(vc, animated:true)
    }

}
