//
//  EmergencyViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 10/01/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import MapKit

class EmergencyViewController: DasBasicViewController {

    private var locationManager: CLLocationManager = CLLocationManager()
    private var currentLocation: CLLocation?
    var adressLabel: UILabel!
    var coordinateLabel: UILabel!

    override func viewDidLoad() {
        self.navigationItem.title = "Emergencia"
        super.viewDidLoad()
        customNavigationBar()
        if !Theme.shared.userLogged {
            setupUnloggedUserScreen(container: self.view)
            return
        }

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }

        var colors = [UIColor]()
        colors.append(UIColor(red: 50/255, green: 82/255, blue: 129/255, alpha: 1))
        colors.append(UIColor(red: 67/255, green: 115/255, blue: 164/255, alpha: 1))
        let back = RadialGradientView(frame: self.view.frame)
        back.colors = colors
        self.view.addSubview(back)

        let logoImage = UIImage(named:"logo-white")
        let logo = UIImageView(image: logoImage)
        logo.frame = CGRect(x: (self.view.frame.size.width - ((logoImage?.size.width)! * 0.5)) * 0.5, y: 75, width: (logoImage?.size.width)! * 0.5, height: (logoImage?.size.height)! * 0.5)
        self.view.addSubview(logo)
        let label1 = addLabelWithTag(font: Theme.shared.fonts.boldFont.withSize(26), textColor: Theme.shared.colors.textBlueGray, container: back, multiline: true)
        let str = "Enlace directo con nuestro Call Center Médico"
        let attributedString = NSMutableAttributedString(string: str, attributes: [NSAttributedStringKey.font : Theme.shared.fonts.boldFont.withSize(26)])
        attributedString.addAttribute(NSAttributedStringKey.font, value: Theme.shared.fonts.italicFont.withSize(28), range: NSMakeRange(27, 11))
        label1.attributedText = attributedString

        label1.textAlignment = .center
        label1.frame = CGRect(x: 20.0, y: logo.frame.origin.y + logo.frame.size.height - 5, width: self.view.frame.size.width - 40.0, height: 80)

        let circleSize = self.view.frame.size.height * 0.35
        let cView1 = UIView(frame: CGRect(x: (self.view.frame.size.width - circleSize) * 0.5, y: label1.frame.origin.y + label1.frame.size.height + 30, width: circleSize, height: circleSize))
        cView1.layer.cornerRadius = circleSize * 0.5
        cView1.clipsToBounds = true
        cView1.backgroundColor = .white
        self.view.addSubview(cView1)

        let cView2 = UIView(frame: CGRect(x: cView1.frame.origin.x + 10, y: cView1.frame.origin.y + 10, width: cView1.frame.size.width - 20.0, height: cView1.frame.size.width - 20.0))
        cView2.layer.cornerRadius = (cView1.frame.size.width - 20.0) * 0.5
        cView2.clipsToBounds = true
        cView2.backgroundColor = Theme.shared.colors.redEmergency
        self.view.addSubview(cView2)

        let button = UIButton(frame: cView1.frame)
        button.setTitle("Ayuda", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(callEmergency), for: .touchUpInside)
        button.titleLabel?.font = Theme.shared.fonts.boldFont.withSize(30.0)
        self.view.addSubview(button)

        let label2 = addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: Theme.shared.colors.textYellow, container: back)
        label2.text = "Ubicación actual"
        label2.textAlignment = .center
        
        adressLabel = addLabelWithTag(font: Theme.shared.fonts.boldFont.withSize(14.0), textColor: .white, container: back, multiline: true)
        adressLabel.text = "Buscando dirección..."
        adressLabel.textAlignment = .center
        
        coordinateLabel = addLabelWithTag(font: Theme.shared.fonts.boldFont.withSize(14.0), textColor: Theme.shared.colors.textBlueGray, container: back)
        coordinateLabel.text = " - "
        coordinateLabel.textAlignment = .center
        coordinateLabel.frame = CGRect(x: 20.0, y: self.view.frame.size.height - coordinateLabel.frame.size.height - (tabBarController?.tabBar.frame.size.height)! - 40, width: self.view.frame.size.width - 40.0, height: 40)
        adressLabel.frame = CGRect(x: 20.0, y: coordinateLabel.frame.origin.y - adressLabel.frame.size.height - 40, width: self.view.frame.size.width - 40.0, height: 40)
        label2.frame = CGRect(x: 20.0, y: adressLabel.frame.origin.y - label2.frame.size.height - 20, width: self.view.frame.size.width - 40.0, height: 20)

    }

    func customNavigationBar() {
        var colors = [UIColor]()
        colors.append(UIColor(red: 67/255, green: 115/255, blue: 164/255, alpha: 1))
        colors.append(UIColor(red: 50/255, green: 82/255, blue: 129/255, alpha: 1))
        colors.append(UIColor(red: 40/255, green: 59/255, blue: 104/255, alpha: 1))
        navigationController?.navigationBar.setGradientBackground(colors: colors)
        if Theme.shared.userLogged {
            let chatImg: UIImage = UIImage(named: "chat")!
            let chatButton = UIBarButtonItem(image: chatImg, style: .plain, target: self, action: #selector(openChat))
            self.navigationItem.rightBarButtonItem = chatButton
        }
    }

    @objc func callEmergency() {
        guard let number = URL(string: "tel://018000222980") else { return }
        UIApplication.shared.open(number)
    }

    @objc func openChat() {
        if Theme.shared.chatVc == nil {
            Theme.shared.chatVc = storyboard?.instantiateViewController(withIdentifier: "DASChatViewController") as! DASChatViewController
        }
        self.navigationController?.pushViewController(Theme.shared.chatVc, animated:true)
    }

}

extension EmergencyViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        getAdressName(coords: locations.last!)
    }

    func getAdressName(coords: CLLocation) {
        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
            if error != nil {
                print("Hay un error")
            } else {
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    var adressString : String = ""
                    if place.thoroughfare != nil {
                        adressString = adressString + place.thoroughfare! + ", "
                    }
                    if place.subThoroughfare != nil {
                        adressString = adressString + place.subThoroughfare! + "\n"
                    }
                    if place.locality != nil {
                        adressString = adressString + place.locality! + " - "
                    }
                    if place.postalCode != nil {
                        adressString = adressString + place.postalCode! + "\n"
                    }
                    if place.subAdministrativeArea != nil {
                        adressString = adressString + place.subAdministrativeArea! + " - "
                    }
                    if place.country != nil {
                        adressString = adressString + place.country!
                    }
                    self.coordinateLabel.text = "\(coords.coordinate.longitude) - \(coords.coordinate.latitude)"
                    self.adressLabel.text = adressString
                }
            }
        }
    }
}
