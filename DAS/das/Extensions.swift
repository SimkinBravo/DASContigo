//
//  Extensions.swift
//  GenericNews
//
//  Created by Patricio Bravo Cisneros on 22/11/17.
//  Copyright © 2017 OpenMultimedia. All rights reserved.
//

import UIKit
import MapKit

extension UITableView {

    func contentHeight() -> CGFloat {
        var height = CGFloat(0)
        for sectionIndex in 0..<numberOfSections {
            height = height + rect(forSection: sectionIndex).size.height
        }
        return height
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

extension CAGradientLayer {
    
    convenience init(frame: CGRect, colors: [UIColor]) {
        self.init()
        self.frame = frame
        self.colors = []
        for color in colors {
            self.colors?.append(color.cgColor)
        }
        startPoint = CGPoint(x: 0, y: 0)
        endPoint = CGPoint(x: 0, y: 1)
    }
    
    func creatGradientImage() -> UIImage? {
        
        var image: UIImage? = nil
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return image
    }
    
}

extension UINavigationBar {
    
    func setGradientBackground(colors: [UIColor]) {
        
        var updatedFrame = bounds
        updatedFrame.size.height += 20
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: colors)
        
        setBackgroundImage(gradientLayer.creatGradientImage(), for: UIBarMetrics.default)
    }
}

struct Urls {
    static var base:String = "http://198.251.72.220/api/"
    static var socketBase: String = "http://198.251.72.220:8080"
}

extension String {

    func resultsLimitedTo(_ limit: Int) -> String {
        return addParameter(name: "limit", value: String(limit))
    }

    func beginningAt(_ start: Int) -> String {
        return addParameter(name: "offset", value: String(start))
    }

    func addParameter(name: String, value: String) -> String {
        return self + (self.range(of:"?") != nil ? "&" : "?") + name + "=" + value
    }

}

class Appointment {
    var id: String!
    var providerId: String!
    var providerName: String!
    var providerAdress: String!
    var startTime: String!
    var endTime: String!
    var startD: Date!
    var endD: Date!
    var starred: NSNumber = 0
    var comments: String!
    var recommend: NSNumber = 1
    var useItAgain: NSNumber = 1
    var whereuFindOut: NSNumber = 0
    var older: Bool = false
    var isRated: Bool = false
    var tempDate: String!

    init(rawData:[String: AnyObject]) {
        self.id = (rawData["id"] as! NSNumber).stringValue
        self.providerId = (rawData["id_proveedor"] as! NSNumber).stringValue
        let provider = rawData["proveedor"] as! [String: AnyObject]
        self.providerName = provider["nombre"] as! String
        self.providerAdress = provider["direccion"] as! String
        let inFormatter = DateFormatter()
        inFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let outFormatter = DateFormatter()
        outFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        startD = inFormatter.date(from: rawData["hora_inicio"] as! String) ?? Date()
        endD = inFormatter.date(from: rawData["hora_final"] as! String) ?? Date()
        self.startTime = outFormatter.string(from: startD)
        self.endTime = outFormatter.string(from: endD)
        self.older = startD < Date()
        self.recommend = rawData["recomendarias"] as! NSNumber
        self.useItAgain = rawData["autoevaluacion"] as! NSNumber
        self.starred = rawData["calificacion"] as! NSNumber
        if let whereuFind = rawData["comoSeEntero"] as? [AnyObject], whereuFind.count != 0 {
            let first = whereuFind.first as! [String: AnyObject]
            self.whereuFindOut = first["id_cita_catalogo"] as! NSNumber
        }
        if recommend != 0 || useItAgain != 0 || starred != 0 || whereuFindOut != 0 {
            isRated = true
        }
        let index = self.startTime.index(self.startTime.startIndex, offsetBy: 8)
        self.tempDate = self.startTime.substring(to: index)
        print("....", self.startTime, startD, Date(), self.older)
    }
}

extension BasicCellData {

    convenience init (index: Int, newsDasApi: [String: AnyObject]) {
        self.init(slug: "")
//        print(newsDasApi)
        self.id = newsDasApi["id"] as! String
        self.title = newsDasApi["title"] as! String
        self.desc = newsDasApi["author"] as! String
//        self.sectionTitle = newsDasApi["category"] as! String
        self.sectionSlug = self.sectionTitle.lowercased().trimmingCharacters(in: .whitespaces)
        self.detailHtml = newsDasApi["content"] as! String
        self.cellSize = BasicCellSize.collectionFullImage
        self.sectionSlug = "news"
        let imageTStr = newsDasApi["image"] as! String
//        let componentsT = imageTStr.components(separatedBy: ".")
//        let thumbUrlT = componentsT[0] + "." + componentsT[1].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let imageData = BasicImageData(title: "", desc: "", thumbURL: imageTStr)
        let imageStr = newsDasApi["image"] as! String
        let components = imageStr.components(separatedBy: ".")
        imageData.url = components[0] + "." + components[1].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        self.imageURL = imageData.url
        imageData.imageTag = self.imagesTag
        imageData.roundedCourners = true
        self.images = [imageData]
        self.placeholderBackgroundColor = Theme.shared.colors.textBlue
        self.imageMargin = CGPoint(x: 10.0, y: 10.0)
        self.textMargin = CGPoint(x: 10.0, y: 5.0)
        self.contentStyle = .vertical
        self.cellReuseID = BasicCellID.basicImage
        self.isSeparatorHidden = true
        self.titleHeightPercent = 1.0
        self.isDescriptionHidden = true
        self.titleFont = Theme.shared.fonts.regularFont
/*
        self.descFont = Theme.shared.fonts.regularFont
        self.descTextColor = Theme.shared.colors.textGray
*/
    }

    convenience init (index: Int, promoDasApi: [String: AnyObject]) {
        self.init(slug: "")
        self.id = (promoDasApi["id"] as! NSNumber).stringValue
        self.title = promoDasApi["title"] as! String
        self.title = self.title.replacingOccurrences(of: "[i]", with: "")
        self.title = self.title.replacingOccurrences(of: "[/i]", with: "")
        self.detailHtml = promoDasApi["introtext"] as! String
        self.desc = self.detailHtml.html2String
//        self.sectionSlug = promoDasApi["name"] as! String
//        self.sectionTitle = newsDasApi["category"] as! String
//        self.sectionSlug = self.sectionTitle.lowercased().trimmingCharacters(in: .whitespaces)
//        self.author = promoDasApi["address"] as! String
//        self.extra = promoDasApi["extras"] as! String
//        self.detail = (promoDasApi["price"] as! NSNumber).stringValue
//        self.detailHtml = (promoDasApi["final_price"] as! NSNumber).stringValue
//        self.expiresAt = promoDasApi["expires_at"] as! String
//        self.phone = promoDasApi["phone"] as! String
        self.cellSize = BasicCellSize.separatorS
        self.sectionSlug = "promo"

        self.contentStyle = .vertical
        self.cellId = .custom5
        self.isImageHidden = true
        self.imagePercent = 0.0
        self.isSeparatorHidden = false
        self.adjustFontSize = false
        self.titleHeightPercent = 0.4
        self.textOffset = 10.0
        self.textMargin = CGPoint(x: 15.0, y: 5.0)
        self.titleFont = Theme.shared.fonts.regularFont.withSize(16.0)
        self.descFont = Theme.shared.fonts.regularFont.withSize(14.0)
        self.descTextColor = Theme.shared.colors.textGray

    }

    convenience init (index: Int, message: ChatMessage) {
        self.init(slug: "")
        let isClient = message.sender == .client
        self.title = message.text
        let text = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width * 0.65, height: 1000))
        text.numberOfLines = 0
        text.lineBreakMode = .byWordWrapping
        text.font = Theme.shared.fonts.regularFont.withSize(Theme.shared.fonts.chatFontSize)
        text.text = message.text
        text.sizeToFit()
        self.desc = message.dateStr
        self.descTextColor = isClient ? Theme.shared.colors.graySeparator : Theme.shared.colors.textBlueGray
//        print(index, text.frame.size)
        self.cellSize = CGSize(width: UIScreen.main.bounds.width, height: text.frame.height + 20)
        self.contentStyle = .chat
        self.cellId = .basic
        self.userInteractionEnabled = false
    }

    convenience init (index: Int, newsDasApi2: [String: AnyObject]) {
        self.init(slug: "")
//        print(newsDasApi2)
        self.id = newsDasApi2["id"] as! String
        self.title = newsDasApi2["title"] as! String
        self.desc = newsDasApi2["author"] as! String
        //        self.sectionTitle = newsDasApi["category"] as! String
        self.sectionSlug = self.sectionTitle.lowercased().trimmingCharacters(in: .whitespaces)
        self.detailHtml = newsDasApi2["content"] as! String
        self.cellSize = BasicCellSize.double
        self.sectionSlug = "collectionnews"
        let imageTStr = newsDasApi2["image"] as! String
//        let componentsT = imageTStr.components(separatedBy: ".")
//        let thumbUrlT = componentsT[0] + "." + componentsT[1].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let imageData = BasicImageData(title: "", desc: "", thumbURL: imageTStr)
        let imageStr = newsDasApi2["image"] as! String
        let components = imageStr.components(separatedBy: ".")
        imageData.url = components[0] + "." + components[1].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        self.imageURL = imageData.url
        imageData.imageTag = self.imagesTag
        imageData.roundedCourners = true
        self.images = [imageData]
        self.placeholderBackgroundColor = Theme.shared.colors.textBlue
        self.imageMargin = CGPoint(x: 10.0, y: 10.0)
        self.imagePercent = 0.75
        self.contentStyle = .vertical
        self.textPercent = 0.25
        self.textMargin = CGPoint(x: 10.0, y: 0)
        self.titleHeightPercent = 1.0
        self.isSeparatorHidden = true
        self.titleFont = Theme.shared.fonts.regularFont
        self.isDescriptionHidden = true
    }
/*
 "state": CIUDAD DE MEXICO, "regionId": 1, "locationId": 2, "longitud": -99.1842, "neighborhoodId": 7798, "type": MEDICOS, "postalCode": 01020, "zone": SUR, "id": 1212, "region": ALVARO OBREGON, "latitud": 19.3572, "distance": 1.805904721406535, "zoneId": 1, "numExt": EXT.1799, "telephoneA": 56627098, "name": ESCALANTE MONDRAGON MARGARITA, "numInt": INT.101, "neighborhood": GUADALUPE INN, "typeId": 1, "specialtyId": 91, "stateId": 9, "location": CIUDAD DE MEXICO Y AREA METROPOLITANA, "specialty": PEDIATRIA, "telephoneB": 56618404, "promotion": Costo de la consulta $400.00, "providerId": 657
 */
    convenience init (index: Int, searchRawItem: [String: AnyObject]) {
        self.init(slug: "")
//        print(searchRawItem)
        self.id = (searchRawItem["id"] as! NSNumber).stringValue
        self.title = searchRawItem["name"] as! String
        var fullAddress = ""
        if let street = searchRawItem["street"] as? String {
            fullAddress = street
        }
        if let numExt = searchRawItem["numExt"] as? String {
            fullAddress.append(" " + numExt)
        }
        if let numInt = searchRawItem["numInt"] as? String, !numInt.isEmpty {
            fullAddress.append(" " + numInt)
        }
        if let adress = searchRawItem["neighborhood"] as? String {
            fullAddress.append(", " + adress)
        }
        if let region = searchRawItem["region"] as? String {
            fullAddress.append(", " + region)
        }
        if let location = searchRawItem["location"] as? String {
            fullAddress.append(", " + location)
        }
        self.author = fullAddress
        self.slug = (searchRawItem["providerId"] as! NSNumber).stringValue

        self.detail = searchRawItem["promotion"] as! String
//        let state = searchRawItem["location"] as! String
//        self.author = "\(self.author ?? ""), \(self.desc), \(state)"
        self.phone = searchRawItem["telephoneA"] as! String
        if let phoneB = searchRawItem["telephoneB"] as? String {
            self.phone = "\(self.phone!)\n\(phoneB)"
        }

//        let specialtyId = (searchRawItem["specialtyId"] as! NSNumber).stringValue
//        let specialty = searchRawItem["specialty"] as! [String: AnyObject]
//        let typeId = (searchRawItem["typeId"] as! NSNumber).stringValue
//        let serviceType = searchRawItem["type"] as! [String: AnyObject]
        self.specialty = searchRawItem["specialty"] as! String
        self.specialities.append(self.specialty)
        self.serviceType = searchRawItem["type"] as! String
        if let long = searchRawItem["longitud"] as? NSNumber {
            let longitude = long
            let latitude = searchRawItem["latitud"] as! NSNumber
            self.location = CLLocationCoordinate2DMake(CLLocationDegrees(truncating: latitude), CLLocationDegrees(truncating: longitude))
        }
        self.cellSize = BasicCellSize.separatorS
        self.contentStyle = .vertical
        self.cellId = .basic
        self.isImageHidden = true
        self.imagePercent = 0.0
        self.isSeparatorHidden = false
//        self.adjustFontSize = false
        self.titleHeightPercent = 0.4
        self.textOffset = 10.0
        self.textMargin = CGPoint(x: 15.0, y: 5.0)
        self.titleFont = Theme.shared.fonts.regularFont.withSize(16.0)
        self.descFont = Theme.shared.fonts.regularFont.withSize(14.0)
        self.descTextColor = Theme.shared.colors.textGray
    }

    convenience init (index: Int, calendarRawItem: [String: AnyObject]) {
        self.init(slug: "")
        self.appointment = Appointment(rawData: calendarRawItem)
        self.title = appointment.providerName
        self.desc = appointment.providerAdress

        self.cellSize = BasicCellSize.separatorS
        self.contentStyle = .custom1
        self.cellId = .basic
        self.isImageHidden = true
        self.imagePercent = 0.0
        self.isSeparatorHidden = false
        //        self.adjustFontSize = false
        self.titleHeightPercent = 0.4
        self.textOffset = 10.0
        self.textMargin = CGPoint(x: 15.0, y: 5.0)
        self.titleFont = Theme.shared.fonts.regularFont.withSize(16.0)
        self.descFont = Theme.shared.fonts.regularFont.withSize(14.0)
        self.descTextColor = Theme.shared.colors.textGray
    }

    public func configCatalogCell() -> BasicCellData {
        self.index = Int(self.slug)
        self.cellSize = BasicCellSize.separator
        self.titleHeightPercent = 1.0
        self.contentStyle = .vertical
        self.cellId = .basic
        self.isImageHidden = true
        self.textOffset = 10.0
        self.textMargin = CGPoint(x: 15.0, y: 5.0)
        self.titleFont = Theme.shared.fonts.regularFont.withSize(16.0)
        return self
    }

    func setupSeparatorTitleCell(isSeparatorHidden: Bool! = true) -> BasicCellData {
        self.cellId = .separator
        self.contentStyle = .sectionSeparator
        self.cellSize = BasicCellSize.separatorS
        self.titleFont = UIDevice.current.screenType == .iPhones_5_5s_5c_SE ? Theme.shared.fonts.boldFont.withSize(19) : Theme.shared.fonts.boldFont.withSize(23.0)
        self.titleTextColor = .black
        self.textMargin = CGPoint(x: 15, y: 3)
        self.isSeparatorHidden = isSeparatorHidden
        self.descFont = Theme.shared.fonts.regularFont.withSize(17.0)
        self.descTextColor = Theme.shared.colors.textYellow
        return self
    }

    func setupCustomIconsCell(withIcons haveIcons: Bool! = true, rightOffset: CGFloat! = 0) -> BasicCellData {
        self.cellId = .separator
        self.contentStyle = .sectionSeparator
        self.cellSize = BasicCellSize.separatorS
        if haveIcons {
            self.icons.append(IconData(index: 0, imageName: "account.png", positionPercent: CGPoint(x: 0.73, y: 0.0)))
            self.icons.append(IconData(index: 1, imageName: "premium.png", positionPercent: CGPoint(x: 0.93, y: 0.0)))
        }
        self.textOffset = 20.0
        self.textMargin = CGPoint(x: 15.0, y: 10.0)
        self.titleFont = Theme.shared.fonts.regularFont
        self.descFont = Theme.shared.fonts.regularFont
        self.descTextColor = .black
        self.userInteractionEnabled = false
        self.textROffset = rightOffset
        self.adjustFontSize = false
        self.isSeparatorHidden = false
        return self
    }

    func setupDoubleColumnCell(text1: String, text2: String) -> BasicCellData {
        self.cellId = .custom4
        self.contentStyle = .custom2
        self.cellSize = BasicCellSize.separatorM
        self.titleFont = Theme.shared.fonts.regularFont
        self.desc = text1
        self.extra = text2
        self.textMargin = CGPoint(x: 15.0, y: 10.0)
        self.descFont = Theme.shared.fonts.regularFont
        self.descTextColor = .black
        self.userInteractionEnabled = false
        self.isSeparatorHidden = false
        return self
    }

    func setupDoubleIconBenefitCell(haveCross: Bool = false) -> BasicCellData {
        self.cellSize = BasicCellSize.separatorS
        self.cellId = .basic
        self.contentStyle = .horizontal
        self.adjustFontSize = false
        self.icons.append(IconData(index: 0, imageName: haveCross ? "minus.png" : "ok.png", positionPercent: CGPoint(x: 0.7, y: 0.5)))
        self.icons.append(IconData(index: 0, imageName: "ok.png", positionPercent: CGPoint(x: 0.9, y: 0.5)))
        self.imagePercent = 0.45
        self.textMargin = CGPoint(x: 15.0, y: 2)
        self.isImageHidden = true
        self.titleFont = Theme.shared.fonts.regularFont
        self.descFont = Theme.shared.fonts.boldFont
        self.userInteractionEnabled = false
        self.isSeparatorHidden = false
        return self
    }

    func setupIconBenefitCell(haveCross: Bool = false) -> BasicCellData {
        self.cellSize = BasicCellSize.separatorS
        self.cellId = .custom2
        self.contentStyle = .horizontal
        self.adjustFontSize = false
        self.icons.append(IconData(index: 0, imageName: haveCross ? "minus.png" : "ok.png", positionPercent: CGPoint(x: 0.9, y: 0.5)))
        self.imagePercent = 0.4
        self.textMargin = CGPoint(x: 15.0, y: 10.0)
        self.isImageHidden = true
        self.titleFont = Theme.shared.fonts.regularFont
        self.descFont = Theme.shared.fonts.boldFont
        self.userInteractionEnabled = false
        self.isSeparatorHidden = false
        return self
    }

    func setupLinkedCell(haveCross: Bool = false) -> BasicCellData {
        self.cellSize = BasicCellSize.separator
        self.cellId = .custom
        self.contentStyle = .horizontal
        self.adjustFontSize = false
        self.imagePercent = 0
        self.icons.append(IconData(index: 0, imageName: "rarrow-icon.png", positionPercent: CGPoint(x: 0.95, y: 0.5), scale: 0.65))
        self.textMargin = CGPoint(x: 15.0, y: 10.0)
        self.isImageHidden = true
        self.titleFont = Theme.shared.fonts.regularFont.withSize(17.0)
        self.titleTextColor = .black
        self.isSeparatorHidden = false
        return self
    }

    func setupSectionSeparatorCell(grayBackground: Bool! = true, isSeparatorHidden: Bool! = false, blueBackground: Bool! = false) -> BasicCellData {
        self.cellSize = BasicCellSize.separator
        self.cellId = .separator
        self.contentStyle = .horizontal
        self.adjustFontSize = false
        self.imagePercent = 0
        self.textMargin = CGPoint(x: 15.0, y: 10.0)
        self.isImageHidden = true
        self.backgroundColor = blueBackground ? Theme.shared.colors.textBlueGray : (grayBackground ? Theme.shared.colors.graySeparator : .white)
        self.titleFont = Theme.shared.fonts.regularFont
        self.titleTextColor = blueBackground ? .black : Theme.shared.colors.textGray
        self.userInteractionEnabled = false
        self.isSeparatorHidden = isSeparatorHidden
        return self
    }

    func setupRowButtonCell() -> BasicCellData {
        self.cellSize = BasicCellSize.separator
        self.contentStyle = .horizontal
        self.cellId = .custom5
        self.slug = "add"
        self.adjustFontSize = false
        self.textMargin = CGPoint(x: 20.0, y: 10.0)
        self.isImageHidden = true
        self.imagePercent = 0.05
        self.titleFont = Theme.shared.fonts.regularFont.withSize(16.0)
        self.titleTextColor = Theme.shared.colors.textBlue
        self.textAlignment = .center
        self.isSeparatorHidden = false
        return self
    }

}

extension Array {

    func randomItem() -> Element? {
        if isEmpty { return nil }
        let index = Int(arc4random_uniform(UInt32(self.count)))
        return self[index]
    }
}

class RadialGradientLayer: CALayer {
    
    var center: CGPoint {
        return CGPoint(x: bounds.width/2, y: bounds.height/2)
    }
    
    var radius: CGFloat {
        return (bounds.width + bounds.height)/2
    }
    
    var colors: [UIColor] = [UIColor.black, UIColor.lightGray] {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var cgColors: [CGColor] {
        return colors.map({ (color) -> CGColor in
            return color.cgColor
        })
    }
    
    override init() {
        super.init()
        needsDisplayOnBoundsChange = true
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
    }
    
    override func draw(in ctx: CGContext) {
        ctx.saveGState()
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let locations: [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: colorSpace, colors: cgColors as CFArray, locations: locations) else {
            return
        }
        ctx.drawRadialGradient(gradient, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: radius, options: CGGradientDrawingOptions(rawValue: 0))
    }
    
}



class RadialGradientView: UIView {
    
    private let gradientLayer = RadialGradientLayer()
    
    var colors: [UIColor] {
        get {
            return gradientLayer.colors
        }
        set {
            gradientLayer.colors = newValue
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if gradientLayer.superlayer == nil {
            layer.insertSublayer(gradientLayer, at: 0)
        }
        gradientLayer.frame = bounds
    }
    
}

