//
//  LoginViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 04/01/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import NVActivityIndicatorView

class LoginViewController: UIViewController, UITextFieldDelegate {

//    var accessoryDoneButton: UIBarButtonItem!
//    let accessoryToolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
    //Could also be an IBOutlet, I just happened to have it like this
//    var memberNumber = UITextField()

    let background = UIView()
    let memberNumber = UITextField()
    let loginButton = UIActivityButton(type: .custom)
    var isLoading = false

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
/*
        self.accessoryDoneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(loginPressed))
        self.accessoryDoneButton.title = "
        self.accessoryToolBar.items = [self.accessoryDoneButton]
*/
        background.frame = CGRect(x: 0, y: self.view.frame.size.height - 220, width: self.view.frame.size.width, height: 220)
        background.backgroundColor = .white
        let imageBackground = UIImageView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - background.frame.size.height))
        imageBackground.contentMode = .scaleAspectFill
        imageBackground.image = UIImage(named: "splash-screen")
        self.view.addSubview(imageBackground)
        self.view.addSubview(background)

        let subtitle = self.addLabelWithTag(font: Theme.shared.fonts.boldFont.withSize(24.0), textColor: .black, container: background)
        subtitle.frame = CGRect(x: 15.0, y: 0, width: background.frame.size.width - 50, height: 60)
        subtitle.text = "Bienvenido"
        addSeparator(margin: CGPoint(x: 0.0, y: 60), at: background)

        memberNumber.frame = CGRect(x: 15, y: 60, width: self.view.frame.size.width - 15, height: 60)
        memberNumber.placeholder = "Escribe tu número de afiliado"
        memberNumber.font = UIFont.systemFont(ofSize: 15)
        memberNumber.borderStyle = .none
        memberNumber.keyboardType = .numberPad
        memberNumber.clearButtonMode = .whileEditing
        memberNumber.contentVerticalAlignment = .center
        memberNumber.delegate = self
        background.addSubview(memberNumber)
        memberNumber.text = ""
        memberNumber.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)

        addSeparator(margin: CGPoint(x: 0.0, y: 120), at: background)

        loginButton.tintColor = .black
        loginButton.frame = CGRect(x: 15, y: 130, width: self.view.frame.size.width - 30, height: 45)
        loginButton.setTitle("Iniciar sesión", for: .normal)
        loginButton.backgroundColor = Theme.shared.colors.blueButton
        loginButton.layer.cornerRadius = 8.0
        loginButton.isEnabled = false
        loginButton.alpha = 0.6
        loginButton.clipsToBounds = true
        loginButton.addTarget(self, action: #selector(loginPressed), for: .touchUpInside)
        background.addSubview(loginButton)

        let button2 = UIButton(type: .custom)
        button2.frame = CGRect(x: 15, y: 180, width: self.view.frame.size.width - 15, height: 35)
        button2.setTitle("Aún no tengo membresía", for: .normal)
        button2.setTitleColor(Theme.shared.colors.textBrightBlue, for: .normal)
        button2.addTarget(self, action: #selector(notMemberPressed), for: .touchUpInside)
        background.addSubview(button2)

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        if let userId = UserDefaults.standard.object(forKey: "member") as? String {
            postLogin(userId: userId)
        }
    }

    @objc func textFieldDidChange(textField: UITextField) {
        let textIsEmpty = textField.text?.isEmpty
        loginButton.alpha = textIsEmpty! ? 0.6 : 1.0
        loginButton.isEnabled = !textIsEmpty!
    }

    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }

    @objc func loginPressed() {
        if isLoading {
            return
        }
        dismissKeyboard()
        postLogin(userId: memberNumber.text!)
    }

    @objc func notMemberPressed() {
        if isLoading {
            return
        }
        Theme.shared.userLogged = false
        dismissKeyboard()
        pushHomeVc()
    }

    func postLogin(userId: String) {
        print("\(Urls.base)login", userId)
        loginButton.isLoading(true)
        isLoading = true
        let formParams:[String:String] = ["id": userId]
        Alamofire.request("\(Urls.base)login", method: .post, parameters: formParams, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.value as? [AnyObject] != nil {
                print("lalala")
            } else if response.result.value != nil {
                let rawResponse = response.result.value as! [String: AnyObject]
                let status = (rawResponse["status"] as! CFBoolean) == kCFBooleanTrue
                if status, let data = rawResponse["data"] as? [String: AnyObject] {
                    let isValid = data["activo"] as! NSNumber == 1
                    Theme.shared.userLogged = isValid
                    Theme.shared.user.affiliateNumber = (data["idAfiliacion"] as! NSNumber).stringValue
                    if !isValid {
                        UserDefaults.standard.set(nil, forKey: "member")
                        HUD.flash(.labeledError(title: "Error", subtitle: "Tu membresía no está vigente"), delay: 2.0)
                    } else {
                        UserDefaults.standard.set(Theme.shared.user.affiliateNumber, forKey: "member")
                    }
                    Theme.shared.user.id = (data["idAfiliado"] as! NSNumber).stringValue
                    Theme.shared.user.name = data["nombre"] as! String
                    let date = data["fecha_termino"] as! String
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd/MM/yyyy"
                    let datTemp = formatter.date(from: date)
                    let printF = DateFormatter()
                    printF.locale = Locale(identifier: "es-MX")
                    printF.dateFormat = "MMMM yyyy"
                    Theme.shared.user.expiresAt = printF.string(from: datTemp!).capitalized
//                    print(data["seg_funerarios"] as! String)
                    Theme.shared.user.haveFuneralService = (data["seg_funerarios"] as! String) == "1"
                    Theme.shared.user.haveAccidentInsurance = (data["seg_accidentes"] as! String) == "1"

                    self.postGetUserInfo(userId: Theme.shared.user.affiliateNumber)
//                    self.pushHomeVc()
                } else {
                    HUD.flash(.labeledError(title: "Error", subtitle: "No se ha encontrado la membresía ingresada"), delay: 2.0)
                    self.loginButton.isLoading(false)
                    self.isLoading = false
                }
            } else {
                HUD.flash(.labeledError(title: "Error", subtitle: "Error en la conexión"), delay: 2.0)
                self.loginButton.isLoading(false)
                self.isLoading = false
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)
            }
        }
    }

    func postGetUserInfo(userId: String) {
        print("\(Urls.base)afiliados", userId)
        let formParams:[String:String] = ["idAfiliacion": userId]
        print(formParams)
        Alamofire.request("\(Urls.base)afiliados", method: .post, parameters: formParams, encoding: JSONEncoding.default).responseJSON { response in
            self.loginButton.isLoading(false)
            self.isLoading = false
            if response.result.value as? [AnyObject] != nil {
                print("lalala")
            } else if let rawResponse = response.result.value as? [String: AnyObject] {
                print(rawResponse)
                if let aditionals = rawResponse["adicionales"] as? [[String: AnyObject]] {
                    Theme.shared.user.aditionals = aditionals
                }
                let typeM = rawResponse["numeroDeAfiliados"] as! NSNumber
                Theme.shared.user.maxAdditionals = typeM
                let membershipType = rawResponse["tipoDeAfiliacion"] as! String
                Theme.shared.user.membershipType = typeM != 0 ? .premium : .master
                Theme.shared.user.isAditional = membershipType == "adicional"
                self.pushHomeVc()
            } else {
                HUD.flash(.labeledError(title: "Error", subtitle: "No se ha encontrado la infomación de la membresía ingresada"), delay: 2.0)
                self.loginButton.isLoading(false)
                self.isLoading = false
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)
            }
        }
    }

    func pushHomeVc() {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "Main")
        self.navigationController?.pushViewController(vc, animated: true)
    }


/*
    // Getting
    let defaults = UserDefaults.standard
    if let stringOne = defaults.string(forKey: defaultsKeys.keyOne) {
        print(stringOne) // Some String Value
    }
    if let stringTwo = defaults.string(forKey: defaultsKeys.keyTwo) {
        print(stringTwo) // Another String Value
    }
*/
    func addLabelWithTag(tag:Int! = 0, font:UIFont, textColor:UIColor, container:UIView, multiline:Bool = false) -> UILabel {
        let label = UILabel.init()
        if tag != 0 {
            container.viewWithTag(tag)?.removeFromSuperview()
            label.tag = tag
        }
        label.font = font
        label.textColor = textColor
        if multiline {
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping
        }
        container.addSubview(label)
        return label
    }

    func addSeparator(margin: CGPoint = CGPoint(x: 0.0, y: 0.0), at: UIView! = nil,  height: CGFloat = 1.0) {
        let separator = UIView.init(frame: CGRect(x: margin.x, y: margin.y, width: self.view.frame.size.width - (margin.x * 2), height: height))
        separator.backgroundColor = Theme.shared.colors.cellSeparatorColor
        if at != nil {
            at.addSubview(separator)
        } else {
            self.view.addSubview(separator)
        }
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            background.frame.origin.y -= keyboardSize.height
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            background.frame.origin.y += keyboardSize.height
        }
    }

}
