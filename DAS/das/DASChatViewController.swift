//
//  DASChatViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 05/01/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import SocketIO
import PKHUD
import NVActivityIndicatorView
import AVFoundation

enum Sender {

    case client
    case server

}

class ChatMessage: NSObject {

    var senderId: String!
    var sender: Sender!
    var text: String!
    var date: Date!
    var dateStr: String!

    convenience init(response: [String: AnyObject]) {
        self.init()
        if let msg = response["mensaje"] as? String {
            self.text = msg
        } else if let msg = response["msg"] as? String {
            self.text = msg
        }
        self.sender = response["send"] as! String == "affiliate" ? .client : .server
        let rawDate = response["date"] as! String
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        self.date = formatter.date(from: rawDate)
        formatter.dateFormat = "HH:mm"
        self.dateStr = formatter.string(from: self.date)
        print(self.text, self.dateStr)
    }

    convenience init(text: String, sender: Sender! = nil) {
        self.init()
        self.text = text
        self.sender = sender
    }

}

class DASChatViewController: DASBasicTViewController, UITextFieldDelegate {

    lazy var chatInput: UITextField = {
        let textfield = UITextField()
        textfield.delegate = self
        textfield.frame = CGRect(x: 15, y: 5, width: self.view.frame.width - 90, height: 42)
        textfield.font = Theme.shared.fonts.regularFont.withSize(Theme.shared.fonts.chatFontSize)
        textfield.backgroundColor = .white
        textfield.autocorrectionType = .no
        textfield.layer.cornerRadius = 5
        textfield.layer.masksToBounds = true
        textfield.textColor = .black
        textfield.placeholder = "Escribe tu mensaje"
        return textfield
    }()

    lazy var sendButton: UIActivityButton = {
        let button = UIActivityButton(type: .custom)
        button.frame = CGRect(x: self.view.frame.width - 70, y: 5, width: 60, height: 42)
        button.setTitle("Enviar", for: .normal)
        button.layer.cornerRadius = 5
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(sendText), for: .touchUpInside)
        button.backgroundColor = Theme.shared.colors.blueButton
        return button
    }()

//    let footerView = UIView()
    let systemSoundID: SystemSoundID = 1016

    var timer: Timer!
    var scrollTimer: Timer!
    var chatOpen: Bool = true
    var socket: SocketIOClient!
    var attentionId: String = ""
    var connected: Bool = false
    var staticView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Chat"

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = true
        self.tableView.addGestureRecognizer(tapGesture)
        let manager = SocketClientManager()
        socket = SocketIOClient(socketURL: URL(string: "\(Urls.socketBase)")!, config: [SocketIOClientOption.log(false)])
        manager.addSocket(socket, labeledAs: "main")
        print(Theme.shared.user.affiliateNumber)
        showLoader(isHidden: false, text: "Cargando")
        enableButton(false)
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected", data, ack)
            self.connected = true
            self.socket.emit("searchAttention", ["type": "affiliate", "id": Theme.shared.user.affiliateNumber, "did": UIDevice.current.identifierForVendor!.uuidString])
        }
        socket.on(clientEvent: .disconnect) {data, ack in
            print("socket disconnect", data, ack)
            self.connected = false
            self.enableButton(false)
            self.delayConnect()
        }
        socket.on(clientEvent: .error) {data, ack in
            print("socket error", data, ack)
            print("")
            self.activityLabel.text = "Error en la conexión"
            self.delayConnect()
        }
        socket.on(clientEvent: .statusChange) { data, ack in
            print("socket status change", data, ack)
            print("-------------------")
        }
/*
        socket.on("closeChat") { data, ack in
            self.layouts = []
            self.collectionView?.reloadData()
            self.delayConnect()
        }
*/
        socket.on("message") { data, ack in
            print("message", data, ack)
            print("-------------------")
            let raw = data[0] as! String
            self.activityLabel.text = raw
        }
        socket.on("openNewChat") {data, ack in
            print("openNewChat")
            self.connected = true
            self.enableButton()
            self.deleteAllMessages()
            for rawData in data {
                print("\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
                let pData = rawData as! [String: AnyObject]
                if let messages = pData["fromMsg"] as? [[String: AnyObject]] {
                    var temp = [ChatMessage]()
                    for message in messages {
                        temp.append(ChatMessage(response: message))
                    }
                    self.addMessages(temp, scrollToBottom: false, animated: true)
                }
                self.attentionId = String(describing: pData["toIds"] as! CFNumber)
            }
            self.showLoader(isHidden: true)
        }
        socket.on("sendMsg") {data, ack in
            print("sendMsg", data, ack)
            var temp = [ChatMessage]()
            for rawData in data {
                temp.append(ChatMessage(response: rawData as! [String :AnyObject]))
            }
            self.addMessages(temp, scrollToBottom: false, animated: true)
        }
        staticView.backgroundColor = Theme.shared.colors.cellSeparatorColor
        staticView.addSubview(chatInput)
        staticView.addSubview(sendButton)
        setupInitialFrames()
        view.addSubview(staticView)
        self.doConnect()
    }

    func customNavigationBar() {
        var colors = [UIColor]()
        colors.append(Theme.shared.colors.navigationBarColor1)
        colors.append(Theme.shared.colors.navigationBarColor2)
        colors.append(Theme.shared.colors.navigationBarColor3)
        navigationController?.navigationBar.setGradientBackground(colors: colors)
    }

    func deleteAllMessages() {
        print("deleteAllMessages")
        self.elements = []
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        chatOpen = true
        if !connected {
            delayConnect()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear", connected, timer)
        chatOpen = true
        dismissKeyboard()
        addNotifications()
        if !connected {
            delayConnect()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        dismissKeyboard()
        removeNotifications()
        setupInitialFrames()
        chatOpen = false
        timer?.invalidate()
        timer = nil
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dismissKeyboard()
        if self.navigationController != nil {
            let controllersCount: Int = (self.navigationController?.viewControllers.count)!
            if controllersCount >= 2 {
                chatOpen = false
                timer?.invalidate()
                timer = nil
                self.navigationController?.popViewController(animated: false)
            }
        }
    }

    func delayConnect(delay: TimeInterval! = 5) {
        if !connected && timer == nil {
            timer = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(doConnect), userInfo: nil, repeats: false)
        }
    }

    @objc func doConnect() {
        timer = nil
        print("doConnect", self.connected, self.chatOpen)
        if !self.connected && self.chatOpen {
            showLoader(isHidden: false, text: "Estableciendo conexión")
            DispatchQueue.main.async {
                self.socket.connect()
            }
        }
    }

    private func sendMessage(_ message: ChatMessage) {
        print("sendMessage", "newMsg", ["name": "affiliate.\(Theme.shared.user.affiliateNumber!)", "to": self.attentionId, "from": Theme.shared.user.affiliateNumber, "msg": message.text])
        //name: "affiliate.20792", to: "55", from: "20792", msg: "asdf"
        socket.emit("newMsg", ["name": "affiliate.\(Theme.shared.user.affiliateNumber!)", "to": self.attentionId, "from": Theme.shared.user.affiliateNumber, "msg": message.text])
        AudioServicesPlaySystemSound (systemSoundID)
    }

    private func addMessages(_ messages: [ChatMessage], scrollToBottom: Bool, animated: Bool) {
        print("addMessages")
        var temp = [BasicCellData]()
        for message in messages {
            let cell = BasicCellData(index: temp.count, message: message)
            cell.isClient = message.sender == .client
            temp.append(cell)
        }
        if self.elements.count == 0 {
            self.elements.append(temp)
        } else {
            self.elements[0].append(contentsOf: temp)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.scrollToBottom()
        }
    }

    func scrollToBottom(_ animated: Bool! = true) {
        let section = self.tableView.numberOfSections - 1
        let row = self.tableView.numberOfRows(inSection: section) - 1
        let indexPath = IndexPath(row: row, section: section)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: animated)
    }

    func didReceiveMessages(messages: [ChatMessage], chatId: String) {
        if isViewLoaded == false { return }
        addMessages(messages, scrollToBottom: true, animated: true)
        AudioServicesPlaySystemSound (systemSoundID)
    }

    func disconnect() {
        self.connected = false
        enableButton(false)
        socket.disconnect()
    }

    @objc func sendText() {
        if !(chatInput.text?.isEmpty)! {
            sendMessage(ChatMessage(text: chatInput.text!, sender: .client))
            chatInput.text = ""
        }
    }

    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }

    func enableButton(_ isEnabled: Bool = true) {
        sendButton.backgroundColor = isEnabled ? Theme.shared.colors.blueButton : Theme.shared.colors.redEmergency
        sendButton.isEnabled = isEnabled
        chatInput.isEnabled = isEnabled
        chatInput.alpha = isEnabled ? 1 : 0.3
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            staticView.frame = CGRect(x: 0, y: view.frame.height - 55 - keyboardSize.height, width: view.frame.width, height: 55)
            tableView.frame = CGRect(x: 0, y: tableView.frame.origin.y, width: view.frame.width, height: staticView.frame.origin.y)
            scrollToBottom()
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            setupInitialFrames()
        }
    }

    func setupInitialFrames() {
        let deltaH = (self.tabBarController?.tabBar.frame.height)! + 55
        staticView.frame = CGRect(x: 0, y: view.frame.height - deltaH, width: view.frame.width, height: 55)
        tableView.frame = CGRect(x: 0, y: tableView.frame.origin.y, width: view.frame.width, height: staticView.frame.origin.y)
    }

    func addNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func removeNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}
