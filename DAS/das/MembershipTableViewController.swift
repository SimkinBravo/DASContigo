//
//  MembershipTableViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 11/01/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit

class MembershipTableViewController: BasicTableViewController {

    var memberType: Int = 1

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func loadData() {
        var data = [BasicCellData]()
        data.append(getTitleDataWith(title: memberType == 0 ? "Nuestras membresías" : (memberType == 1 ? "Membresía Master" : "Membresía Premium")))
        data.append(getElementDataWith(title: "Precio", desc: memberType == 1 ? "$450" : "$1,700"))
        data.append(getElementDataWith(title: "Membresía para un titular.", icon: 1))
        data.append(getElementDataWith(title: "Cobertura para titular + 4 Personas.", icon: memberType == 1 ? 2 : 1))
        data.append(getElementDataWith(title: "Centro de Atención Médica Telefónica “DAS Contigo” las 24 horas, los 365 días del año.", icon: 1))
        data.append(getElementDataWith(title: "Centro de Atención Nutricional “DAS Contigo”.", icon: 1))
        data.append(getElementDataWith(title: "Consultas médicas generales y especialistas a precios estandarizados ($250 y $350).", icon: 1))
        data.append(getElementDataWith(title: "Descuentos en medicamentos en la principal cadena de autoservicios de México (5% y 7%).", icon: 1))
        data.append(getElementDataWith(title: "Descuentos en laboratorios médicos y otros servicios hasta un 50%.", icon: 1))
        data.append(getElementDataWith(title: "Ambulancia de Urgencia SIN COSTO en emergencias médicas.", icon: 1))
        data.append(getElementDataWith(title: "Check Up general a precio preferencial.", icon: 1))
        data.append(getElementDataWith(title: "Consulta de medicina general a domicilio a precio preferencial (consultar disponibilidad).", icon: 1))
        if memberType != 0 {
            data.append(getSectionDataWith(title: "TAMBIÉN PUEDES AGREGAR A TU MEMBRESÍA:"))
            data.append(getElementDataWith(title: "Seguro de accidentes personales."))
            data.append(getElementDataWith(title: "Servicios funerarios."))
            data.append(getTitleDataWith(title: "¡Contacta a tu asesor!"))
        }
        self.elements.append(data)
        super.onLoadDataResponse()
    }

    func getTitleDataWith(title: String) -> BasicCellData {
        let data = BasicCellData(title: title)
        data.cellSize = BasicCellSize.basic
        data.cellId = .highlighted
        data.adjustFontSize = false
        data.contentStyle = .horizontal
        data.imagePercent = 0
        data.textMargin = CGPoint(x: 20.0, y: 10.0)
        data.isImageHidden = true
        data.titleFont = Theme.shared.fonts.boldFont.withSize(24.0)
        data.userInteractionEnabled = false
        data.isSeparatorHidden = false
        return data
    }

    func getSectionDataWith(title: String) -> BasicCellData {
        let data = BasicCellData(title: title)
        data.cellSize = BasicCellSize.separator
        data.cellId = .highlighted
        data.adjustFontSize = false
        data.contentStyle = .horizontal
        data.imagePercent = 0
        data.textMargin = CGPoint(x: 20.0, y: 10.0)
        data.isImageHidden = true
        data.backgroundColor = Theme.shared.colors.graySeparator
        data.titleTextColor = Theme.shared.colors.textGray
        data.titleFont = Theme.shared.fonts.regularFont
        data.userInteractionEnabled = false
        data.isSeparatorHidden = false
        return data
    }

    func getElementDataWith(title: String, desc: String = "", icon: Int = 0) -> BasicCellData {
        let data = BasicCellData(title: title, subtitle: desc)
        data.cellSize = BasicCellSize.separatorS
        data.contentStyle = .horizontal
        data.adjustFontSize = false
        if icon != 0 {
            data.showIcon = true
            data.iconImagename = icon == 1 ? "ok" : "cross"
            data.iconPositionPercent = CGPoint(x: 0.9, y: 0.5)
            data.iconSizePercent = 1.0
            data.imagePercent = 0.3
            data.cellId = .basic
        } else {
            data.imagePercent = 0.015
            data.cellId = .custom
        }
        data.textMargin = CGPoint(x: 20.0, y: 10.0)
        data.isImageHidden = true
        data.titleFont = Theme.shared.fonts.regularFont
        data.descFont = Theme.shared.fonts.boldFont
        data.userInteractionEnabled = false
        data.isSeparatorHidden = false
        return data
    }

}
