//
//  MedicalServiceDetailViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 12/02/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import MapKit

class MedicalServiceDetailViewController: BasicDetailViewController {

    let mapView = MKMapView()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setInitialDataDisplay() {
        let offsetY: CGFloat = 10.0
        if self.item != nil {
            self.navigationItem.title = self.item.sectionTitle
        }
        let scrollY:CGFloat = 0.0
        self.scroll.frame = CGRect(x: 0.0, y: scrollY, width: self.view.frame.size.width, height: self.view.frame.size.height - scrollY)
        self.view.addSubview(self.scroll)
        let betweenMargin: CGFloat = 15.0
        nextY = 0
        let labelsW = self.view.frame.width - (margin.x * 2)
        titleLabel = self.addTitleLabel(container: self.scroll, under: CGRect(x: margin.x, y: nextY, width: labelsW, height: 1), indent: betweenMargin, maxHeight: 30.0)
        nextY = titleLabel.frame.origin.y + titleLabel.frame.size.height + betweenMargin
        
        let subtitle = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textGray, container: self.scroll, multiline: true)
        subtitle.frame = CGRect(x: margin.x, y: nextY, width: labelsW, height: 250)
        subtitle.text = self.item.detail
        subtitle.sizeToFit()
        nextY = nextY + subtitle.frame.height + betweenMargin + 5

        mapView.frame = CGRect(x: 0, y: nextY, width: view.frame.size.width, height: 200)
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.showsUserLocation = true

        let annotation = MKPointAnnotation()
        annotation.coordinate = self.item.location
        annotation.title = self.item.title
        mapView.addAnnotation(annotation)

        let viewRegion = MKCoordinateRegionMakeWithDistance(annotation.coordinate, 2000, 2000)
        mapView.setRegion(viewRegion, animated: false)

        self.scroll.addSubview(mapView)
        nextY = nextY + mapView.frame.height + betweenMargin

        let address = self.addLabelWithTag(font: UIFont.detailDesc, textColor: .black, container: self.scroll)
        address.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        address.text = "Dirección"
        address.sizeToFit()
        nextY = nextY + address.frame.height + betweenMargin - offsetY
        
        let address2 = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textBrightBlue, container: self.scroll, multiline: true)
        address2.frame = CGRect(x: margin.x, y: nextY, width: labelsW, height: 250)
        address2.text = self.item.author
        address2.sizeToFit()
        nextY = nextY + address2.frame.height + betweenMargin

        addSeparator(margin: CGPoint(x: 10.0, y: nextY))
        nextY = nextY + betweenMargin
/*
        let detail = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textGray, container: self.scroll, multiline: true)
        detail.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        detail.text = self.item.expiresAt
        detail.sizeToFit()
        nextY = nextY + detail.frame.height + betweenMargin

        addSeparator(margin: CGPoint(x: 10.0, y: nextY))
        nextY = nextY + betweenMargin
*/
        let phone = self.addLabelWithTag(font: UIFont.detailDesc, textColor: .black, container: self.scroll)
        phone.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        phone.text = "Teléfonos"
        phone.sizeToFit()
        nextY = nextY + phone.frame.height + betweenMargin - offsetY
        
        let phone2 = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textBrightBlue, container: self.scroll)
        phone2.frame = CGRect(x: margin.x, y: nextY, width: labelsW, height: 250)
        phone2.numberOfLines = 0
        phone2.text = self.item.phone!
        phone2.sizeToFit()
        nextY = nextY + phone2.frame.height + betweenMargin

        addSeparator(margin: CGPoint(x: 10.0, y: nextY))
        nextY = nextY + betweenMargin
        
        let til = self.addLabelWithTag(font: UIFont.detailDesc, textColor: .black, container: self.scroll)
        til.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        til.text = "Especialidad"
        til.sizeToFit()
        nextY = nextY + til.frame.height + betweenMargin - offsetY
        
        let til2 = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textBrightBlue, container: self.scroll)
        til2.frame = CGRect(x: margin.x, y: nextY, width: labelsW, height: 250)
        til2.numberOfLines = 0
        til2.lineBreakMode = .byWordWrapping
        til2.text = self.item.specialities.joined(separator: "\n")
        til2.sizeToFit()
        nextY = nextY + til2.frame.height + betweenMargin
        
        addSeparator(margin: CGPoint(x: 10.0, y: nextY))
        nextY = nextY + betweenMargin
        
        self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: nextY)
    }
    
    func addSeparator(margin: CGPoint = CGPoint(x: 0.0, y: 0.0), height: CGFloat = 1.0) {
        let separator = UIView.init(frame: CGRect(x: margin.x, y: margin.y, width: self.view.frame.size.width - (margin.x * 2), height: height))
        separator.backgroundColor = Theme.shared.colors.cellSeparatorColor
        self.scroll.addSubview(separator)
    }

}
