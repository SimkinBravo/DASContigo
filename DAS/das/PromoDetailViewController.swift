//
//  PromoDetailViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 04/01/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit

class PromoDetailViewController: BasicDetailViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func setInitialDataDisplay() {
        if self.item != nil {
            self.navigationItem.title = self.item.sectionTitle
        }
        let scrollY:CGFloat = 0.0
        self.scroll.frame = CGRect(x: 0.0, y: scrollY, width: self.view.frame.size.width, height: self.view.frame.size.height - scrollY)
        self.view.addSubview(self.scroll)
        let betweenMargin: CGFloat = 15.0
        nextY = 0

        let logoImage = UIImage(named:"logo-white")
        let logo = UIImageView(image: logoImage)
        logo.frame = CGRect(x: (self.view.frame.size.width - ((logoImage?.size.width)! * 0.5)) * 0.5, y: 10, width: (logoImage?.size.width)! * 0.5, height: (logoImage?.size.height)! * 0.5)
        self.scroll.addSubview(logo)
        nextY = logo.frame.origin.y + logo.frame.size.height + betweenMargin

        titleLabel = self.addTitleLabel(container: self.scroll, under: CGRect(x: margin.x, y: nextY, width: self.view.frame.width - (margin.x * 2), height: 1), indent: betweenMargin, maxHeight: 30.0)
        nextY = titleLabel.frame.origin.y + titleLabel.frame.size.height + betweenMargin

        let subtitle = self.addLabelWithTag(font: UIFont.detailDesc, textColor: .black, container: self.scroll, multiline: true)
        subtitle.frame = CGRect(x: margin.x, y: nextY, width: self.view.frame.width - (margin.x * 2), height: 1000)

        let finalHtml = self.item.detailHtml.replacingOccurrences(of: "este cup&oacute;n impreso</p>", with: "este cup&oacute;n</p>").replacingOccurrences(of: ">Imprimir Promoci&oacute;n</button>", with: "></button>")
        guard let data = finalHtml.data(using: String.Encoding.unicode) else { return }
        try? subtitle.attributedText = NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        subtitle.sizeToFit()
        nextY = nextY + subtitle.frame.height + betweenMargin + 10
/*
        let detail = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textGray, container: self.scroll, multiline: true)
        detail.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        detail.text = self.item.expiresAt
        detail.sizeToFit()
        nextY = nextY + detail.frame.height + betweenMargin

        addSeparator(margin: CGPoint(x: 10.0, y: nextY))
        nextY = nextY + betweenMargin

        let price = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textGray, container: self.scroll)
        price.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        price.text = "Precio regular        Precio DAS"
        price.sizeToFit()
        nextY = nextY + price.frame.height + betweenMargin

        let price2 = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textGray, container: self.scroll)
        price2.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
//        price2.text = "$" + self.item.detail + "                $" + self.item.detailHtml
        price2.sizeToFit()
        nextY = nextY + price2.frame.height + betweenMargin

        addSeparator(margin: CGPoint(x: 10.0, y: nextY))
        nextY = nextY + betweenMargin

        let til = self.addLabelWithTag(font: UIFont.detailDesc, textColor: .black, container: self.scroll)
        til.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        til.text = "Vigencia hasta"
        til.sizeToFit()
        nextY = nextY + til.frame.height + betweenMargin - offsetY

        let til2 = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textBrightBlue, container: self.scroll)
        til2.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
//        til2.text = self.item.expiresAt
        til2.sizeToFit()
        nextY = nextY + til2.frame.height + betweenMargin

        addSeparator(margin: CGPoint(x: 10.0, y: nextY))
        nextY = nextY + betweenMargin

        let address = self.addLabelWithTag(font: UIFont.detailDesc, textColor: .black, container: self.scroll)
        address.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        address.text = "Dirección"
        address.sizeToFit()
        nextY = nextY + address.frame.height + betweenMargin - offsetY

        let address2 = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textBrightBlue, container: self.scroll, multiline: true)
        address2.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        address2.text = self.item.author
        address2.sizeToFit()
        nextY = nextY + address2.frame.height + betweenMargin

        addSeparator(margin: CGPoint(x: 10.0, y: nextY))
        nextY = nextY + betweenMargin

        let phone = self.addLabelWithTag(font: UIFont.detailDesc, textColor: .black, container: self.scroll)
        phone.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        phone.text = "Teléfonos"
        phone.sizeToFit()
        nextY = nextY + phone.frame.height + betweenMargin - offsetY
        
        let phone2 = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textBrightBlue, container: self.scroll)
        phone2.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        phone2.text = self.item.phone
        phone2.sizeToFit()
        nextY = nextY + phone2.frame.height + betweenMargin
        
        addSeparator(margin: CGPoint(x: 10.0, y: nextY))
        nextY = nextY + betweenMargin

        let conditions = self.addLabelWithTag(font: UIFont.detailDesc, textColor: .black, container: self.scroll)
        conditions.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        conditions.text = "Condiciones"
        conditions.sizeToFit()
        nextY = nextY + conditions.frame.height + betweenMargin - offsetY
        
        let conditions2 = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textBrightBlue, container: self.scroll)
        conditions2.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        conditions2.text = self.item.extra
        conditions2.sizeToFit()
        nextY = nextY + conditions2.frame.height + betweenMargin
        
        addSeparator(margin: CGPoint(x: 10.0, y: nextY))
        nextY = nextY + betweenMargin

        let promotion = self.addLabelWithTag(font: UIFont.detailDesc, textColor: .black, container: self.scroll)
        promotion.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        promotion.text = "Número de promoción"
        promotion.sizeToFit()
        nextY = nextY + promotion.frame.height + betweenMargin - offsetY
        
        let promotion2 = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textBrightBlue, container: self.scroll)
        promotion2.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        promotion2.text = self.item.id
        promotion2.sizeToFit()
        nextY = nextY + promotion2.frame.height + betweenMargin
        
        addSeparator(margin: CGPoint(x: 10.0, y: nextY))
        nextY = nextY + betweenMargin

        let valid = self.addLabelWithTag(font: UIFont.detailDesc, textColor: Theme.shared.colors.textGray, container: self.scroll, multiline: true)
        valid.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        valid.text = "Promoción para Afiliados DAS, válido al presentar esta pantalla"
        valid.textAlignment = .center
        valid.sizeToFit()
        valid.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: valid.frame.size.height)
        nextY = nextY + valid.frame.height + betweenMargin
*/
        self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: nextY)
    }

    func addSeparator(margin: CGPoint = CGPoint(x: 0.0, y: 0.0), height: CGFloat = 1.0) {
        let separator = UIView.init(frame: CGRect(x: margin.x, y: margin.y, width: self.view.frame.size.width - (margin.x * 2), height: height))
        separator.backgroundColor = Theme.shared.colors.cellSeparatorColor
        self.scroll.addSubview(separator)
    }

}
