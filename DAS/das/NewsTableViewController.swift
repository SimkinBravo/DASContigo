//
//  NewsTableViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 26/12/17.
//  Copyright © 2017 Last Code. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

class NewsTableViewController: DASBasicTableViewController {

    override func viewDidLoad() {
        placeholderName = ""
        activityIndicatorColor = .blue
        super.viewDidLoad()
        self.navigationItem.title = "Noticias"
        let backImg: UIImage = UIImage(named: "header-back-button")!
        let backButton = UIBarButtonItem(image: backImg, style: .plain, target: self, action: #selector(BasicDetailViewController.backAction))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.setBackgroundVerticalPositionAdjustment(-10.0, for: .default)
    }

    @objc func backAction() -> Void {
        dismiss(animated: true, completion: nil)
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
        }
    }

    override func loadData() {
        loadNews()
/*
        var fsection = [BasicCellData]()
        fsection.append(setupDoubleCell(slug: "news", maintitle: "", secondaryTitle: "", a: getCollectionDataWith(title: "Ostopedía y osteporosis", desc: "Enfermedades"), b: getCollectionDataWith(title: "Vacuna Influenza", desc: "Mitos y realidades")))
        fsection.append(setupDoubleCell(slug: "news", maintitle: "", secondaryTitle: "", a: getCollectionDataWith(title: "Vacuna Influenza", desc: "Mitos y realidades"), b: getCollectionDataWith(title: "Ostopedía y osteporosis", desc: "Enfermedades")))
        fsection.append(setupDoubleCell(slug: "news", maintitle: "", secondaryTitle: "", a: getCollectionDataWith(title: "Ostopedía y osteporosis", desc: "Enfermedades"), b: getCollectionDataWith(title: "Vacuna Influenza", desc: "Mitos y realidades")))
        fsection.append(setupDoubleCell(slug: "news", maintitle: "", secondaryTitle: "", a: getCollectionDataWith(title: "Vacuna Influenza", desc: "Mitos y realidades"), b: getCollectionDataWith(title: "Ostopedía y osteporosis", desc: "Enfermedades")))
        fsection.append(setupDoubleCell(slug: "news", maintitle: "", secondaryTitle: "", a: getCollectionDataWith(title: "Ostopedía y osteporosis", desc: "Enfermedades"), b: getCollectionDataWith(title: "Vacuna Influenza", desc: "Mitos y realidades")))
        fsection.append(setupDoubleCell(slug: "news", maintitle: "", secondaryTitle: "", a: getCollectionDataWith(title: "Vacuna Influenza", desc: "Mitos y realidades"), b: getCollectionDataWith(title: "Ostopedía y osteporosis", desc: "Enfermedades")))
        self.elements.append(fsection)
        self.onLoadDataResponse()
*/
    }

    func loadNews() {
        print("\(Urls.base)feed")
        Alamofire.request("\(Urls.base)feed", method: .post, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.value as? [AnyObject] != nil {
                print("lalala")
            } else if response.result.value != nil {
                let rawResponse = response.result.value as! [String: AnyObject]
                let status = (rawResponse["status"] as! CFBoolean) == kCFBooleanTrue
                if status {
                    let content = rawResponse["data"] as! NSArray
                    var temp = [BasicCellData]()
                    var tempElement: BasicCellData!
                    for (index, element) in content.enumerated() {
                        if tempElement == nil {
                            tempElement = BasicCellData.init(index: index, newsDasApi2: element as! [String : AnyObject])
                        } else {
                            temp.append(self.setupDoubleCell(slug: "", maintitle: "", secondaryTitle: "", a: tempElement, b: BasicCellData.init(index: index, newsDasApi2: element as! [String : AnyObject])))
                            tempElement = nil
                        }
                    }
                    if tempElement != nil {
                        temp.append(self.setupDoubleCell(slug: "", maintitle: "", secondaryTitle: "", a: tempElement, b: nil))
                    }
                    self.elements.append(temp)
                } else {
                    print("error")
                }
                self.onLoadDataResponse()
            } else {
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)
            }
        }
    }

    override func setupDoubleCell(slug: String, maintitle: String, secondaryTitle: String, a: BasicCellData, b: BasicCellData!, haveSectionHeader: Bool = false) -> BasicDoubleCellData {
        let data = super.setupDoubleCell(slug: slug, maintitle: maintitle, secondaryTitle: secondaryTitle, a: a, b: b, haveSectionHeader: haveSectionHeader)
        data.cellSize = BasicCellSize.double
        return data
    }

    func getCollectionDataWith(title: String, desc: String) -> BasicCellData {
        let data = BasicCellData(title: title, subtitle: desc)
        data.cellSize = BasicCellSize.double
        data.slug = "collectionnews"
        let imageData = BasicImageData(title: "", desc: "", thumbURL: "")
        imageData.roundedCourners = true
        data.images = [imageData]
        data.placeholderBackgroundColor = Theme.shared.colors.textBlue
        data.imageMargin = CGPoint(x: 10.0, y: 10.0)
        data.imagePercent = 0.75
        data.contentStyle = .vertical
        data.textPercent = 0.25
        data.textMargin = CGPoint(x: 10.0, y: 10.0)
        data.titleHeightPercent = 1.0
        data.imageURL = ""
        data.isSeparatorHidden = true
        data.titleFont = Theme.shared.fonts.regularFont
        data.isDescriptionHidden = true
        return data
    }

    override func tableViewDidSelectData(data: BasicCellData) {
        super.tableViewDidSelectData(data: data)
        let navigationC = self.storyboard!.instantiateViewController(withIdentifier: "NewsDetailNavigationController") as! UINavigationController
        let vc = navigationC.viewControllers.first! as! NewsDetailViewController
        vc.ownerData = self.data
        vc.item = data
        self.navigationController?.pushViewController(vc, animated:true)
    }

}
