//
//  BasicDetailViewController.swift
//  lajornada
//
//  Created by Simkin Bravo on 08/12/16.
//  Copyright © 2016 La Jornada. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import AVFoundation
import AVKit

class BasicDetailViewController: UIViewController, UIWebViewDelegate {

    var item:BasicCellData!
    var ownerData: BasicPageData!
    let margin:CGPoint = CGPoint(x: 17.0, y: 25.0)
    let initialPoint:CGPoint = CGPoint(x: 20.0, y: 100.0)
    var nextY:CGFloat = 0.0
    let scroll:UIScrollView = UIScrollView()
    var horizontalViews:[UIView] = []
    var imageViews:[UIImageView] = []
    var hImageScroll:UIScrollView!
    var maxheightHViews:CGFloat = 0.0
    var imageFirst:Bool = true
//    var JOData:JOBasicCellData!
    var first:Bool = true
    var requestId:String = ""
    var titleLabel: UILabel!
    var imageContainer: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        if self.ownerData != nil {
            self.navigationItem.title = self.ownerData.title
            self.navigationController?.title = self.ownerData.title
        }
        self.setInitialDataDisplay()
        self.customNavigationBar()
        if self.item.detailUrl != nil {
            loadDataFrom(url: self.item.detailUrl)
        }
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(backSwiped(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
    }

    @objc func backSwiped(gesture: UISwipeGestureRecognizer) {
        switch gesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                backAction()
                self.presentingViewController?.viewWillAppear(false)
            default: break
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func customNavigationBar() {
/*
        let backImg: UIImage = UIImage(named: "header-back-button")!
        let backButton = UIBarButtonItem(image: backImg, style: .plain, target: self, action: #selector(BasicDetailViewController.backAction))
        let shareImg: UIImage = UIImage(named: "share-icon")!
        let shareButton = UIBarButtonItem(image: shareImg, style: .plain, target: self, action: #selector(BasicDetailViewController.shareAction))
        self.navigationItem.rightBarButtonItem = shareButton
        self.navigationItem.rightBarButtonItem?.setBackgroundVerticalPositionAdjustment(-10.0, for: .default)
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.setBackgroundVerticalPositionAdjustment(-10.0, for: .default)
*/
        if self.ownerData != nil {
            self.navigationController?.navigationBar.tintColor = self.ownerData.color
        }
        self.navigationItem.title = item.sectionTitle
    }

    @objc func backAction() -> Void {
        dismiss(animated: true, completion: nil)
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
        }
    }

    @objc func shareAction() -> Void {
        let url = self.item.apiURL != nil ? NSURL(string:self.item.apiURL) : nil
        let textToShare = self.item.title
        var shareItems:[Any] = []
        if self.imageViews.count != 0 {
            shareItems = [textToShare ?? "", self.imageViews[0].image ?? "", url ?? ""]
        } else {
            shareItems = [textToShare ?? "", url ?? ""]
        }
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
        self.present(activityViewController, animated: true, completion: nil)
    }

    func addImage(image:BasicImageData) {
        let imageView = UIImageView(frame:CGRect(x: 0, y: 0, width: hImageScroll.frame.size.width, height: 250))
        let hView = UIView(frame: CGRect(x: self.view.frame.size.width * CGFloat(self.imageViews.count), y: 0, width: self.view.frame.size.width * CGFloat(self.imageViews.count + 1), height: self.scroll.frame.size.height))
        hView.addSubview(imageView)
        if image.thumbURL != nil && !image.thumbURL.isEmpty {
            let  filter = AspectScaledToFillSizeFilter (size: imageView.frame.size)
            imageView.af_setImage(withURL: URL(string: image.thumbURL)!, filter: filter)
        } else {
            imageView.backgroundColor = Theme.shared.colors.textBlue
        }
        let imageAlt = self.addLabelWithTag(tag: 110 + self.imageViews.count, font: UIFont.detailDesc, textColor: UIColor.cellTitleA, container: self.view, multiline:true)
        imageAlt.textAlignment = .justified
        imageAlt.frame = CGRect(x: margin.x, y: 250 + margin.y, width: imageView.frame.size.width - (margin.x * 2) - 5, height: 200)
/*
        var rangeStr = image.desc.range(of: "Foto ")!
        if rangeStr == nil {
            rangeStr = image.desc.range(of: "Fotos ")!
        }
        if rangeStr != nil {
            let attributedString = NSMutableAttributedString(string:image.desc)
            let ranges = image.desc.ranges(of: "Foto ")
//            let n1 = NSRange(rangeStr, in: image.desc)
//            let NSRangeStr = (image.desc as NSString).substring(with: n1)
//            NSRange(
//            let newRange = NSRange(location:ranges, length:image.desc.characters.count - ranges)
            attributedString.addAttribute(NSFontAttributeName, value: JOFonts.displayRegularMedium!, range: ranges)
            attributedString.addAttribute(NSForegroundColorAttributeName, value:UIColor.black, range: newRange)
            imageAlt.attributedText = attributedString
        } else {
*/
            imageAlt.text = image.desc
//        }
        imageAlt.sizeToFit()
        hView.addSubview(imageAlt)
        imageViews.append(imageView)
        horizontalViews.append(hView)
        hImageScroll.addSubview(hView)
        maxheightHViews = max(maxheightHViews, imageAlt.frame.origin.y + imageAlt.frame.size.height)
        hImageScroll.contentSize = CGSize(width: CGFloat(imageViews.count) * hImageScroll.frame.size.width, height: maxheightHViews)
        if image.showIcon {
            let iconImg = UIImage(named: item.iconImagename)
            let icon = UIImageView(image: iconImg)
            icon.clipsToBounds = false
            icon.layer.shadowColor = UIColor.black.cgColor
            icon.layer.shadowOpacity = 1
            icon.layer.shadowOffset = CGSize.zero
            let iconSizePercent:CGFloat = 1.5
            let iconPositionPercent:CGPoint = CGPoint(x: 0.05, y: 0.95)
            let iconSize = CGSize(width: (iconImg?.size.width)! * iconSizePercent, height: (iconImg?.size.height)! * iconSizePercent)
            icon.frame = CGRect(origin: CGPoint(x:(hImageScroll.frame.size.width - iconSize.width) * iconPositionPercent.x, y:(hImageScroll.frame.size.height - iconSize.height)  * iconPositionPercent.y), size: iconSize)
            icon.tag = 123456
            imageView.addSubview(icon)
            let button = UIButton(frame: imageView.frame)
            hView.addSubview(button)
            button.addTarget(self, action: #selector(playButtonAction), for: UIControlEvents.touchUpInside)
        }
    }

    @objc func playButtonAction() {
        UIApplication.shared.open(URL(string: self.item.video.url)!, options: [:], completionHandler: nil)
//        playVideo(url: URL(string: JOData.video.hlsUrl)!, data:JOData)
    }

    func playVideo(url: URL, data:BasicCellData) {
        let player = AVPlayer(url: url)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true, completion: nil)
    }

    private func getSinceFormattedString(from date:Date) -> String {
        if Bundle.main.infoDictionary?["TargetName"] as! String == "teleSUR" {
            let currentDate = Date()
            let daysBefore = currentDate.days(from: date)
            if daysBefore == 0 {
                let hoursBefore = currentDate.hours(from: date)
                if hoursBefore == 0 {
                    return "Hace unos minutos"
                }
                return "Hace " + String(hoursBefore) + " h"
            } else if daysBefore > 30 {
                let monthsTo = floor(Double(daysBefore / 30))
                return "Hace " + String(monthsTo) + " mes" + (monthsTo != 1 ? "es" : "")
            }
            return "Hace " + String(daysBefore) + " día" + (daysBefore != 1 ? "s" : "")
        } else {
            let formatter = DateFormatter()
            formatter.dateStyle = DateFormatter.Style.long
            formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
            return "Published " + formatter.string(from: date)
        }
    }

    func setInitialDataDisplay() {
        if self.item != nil {
            self.title = self.item.sectionTitle
        }
        let scrollY:CGFloat = 0.0
        self.scroll.frame = CGRect(x: 0.0, y: scrollY, width: self.view.frame.size.width, height: self.view.frame.size.height - scrollY)
        self.view.addSubview(self.scroll)
        let betweenMargin = margin.y
        nextY = 0
/*
        let section = self.addSectionLabel(origin: CGPoint(x: margin.x, y: nextY), container: self.scroll)
*/
        titleLabel = self.addTitleLabel(container: self.scroll, under: CGRect(x: margin.x, y: nextY, width: self.view.frame.width - (margin.x * 2), height: 1), indent: betweenMargin, maxHeight: 120.0)
        nextY = titleLabel.frame.origin.y + titleLabel.frame.size.height + betweenMargin

        if item.imageURL != nil {
            for image in self.item.images {
                if hImageScroll == nil {
                    hImageScroll = UIScrollView(frame: CGRect(x: 0, y: nextY, width: self.scroll.frame.size.width, height: 250))
                    hImageScroll.isPagingEnabled = true
                    self.scroll.addSubview(hImageScroll)
                }
                hImageScroll.isScrollEnabled = false
                self.addImage(image: image)
            }
            nextY = nextY + hImageScroll.frame.height + betweenMargin
        }

        if item.publishedAt != nil {
            let since = self.addLabelWithTag(tag: 54312, font: UIFont.cellSince, textColor: UIColor.cellSince, container: self.scroll)
            since.text = self.getSinceFormattedString(from: item.publishedAt)
            since.frame = CGRect(x: margin.x, y: titleLabel.frame.origin.y + titleLabel.frame.size.height, width: titleLabel.frame.size.width, height: 20 * BasicCellData.sizeFactor)
            nextY = since.frame.origin.y + since.frame.size.height + betweenMargin
        }

        let desc = self.addLabelWithTag(tag: 10004, font: UIFont.detailDesc, textColor: .black, container: self.scroll, multiline: true)
        desc.frame = CGRect(x: margin.x, y: nextY, width: titleLabel.frame.width, height: 250)
        do {
            desc.text = try self.item.desc.convertHtmlSymbols()
        } catch {}
        desc.sizeToFit()
        nextY = nextY + desc.frame.height + betweenMargin
        setDataContent()
        self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: nextY)
    }

    func updateHScrollSize() {
        if hImageScroll != nil {
            hImageScroll.frame = CGRect(origin: hImageScroll.frame.origin, size: CGSize(width: hImageScroll.frame.size.width, height: maxheightHViews + margin.y))
            nextY = nextY + (margin.y * 2)
        }
    }

    func setDataContent() {
        let webView = UIWebView(frame: CGRect(x: 0, y: nextY, width: self.view.frame.size.width - (margin.x * 2), height: 400))
        webView.tag = 200
        if item.detailHtml != nil {
            let htmlBody = item.detailHtml!
            let styledHtmlBody = NSString(format:"<html><head><style></style></head><body><span style=\"font-family: \(UIFont.detailHTML.fontName); font-size: \(UIFont.detailHTML.pointSize);text-align: left; width:303px; overflow:hidden; \">%@</span></body></html>" as NSString, htmlBody) as String
            webView.loadHTMLString(htmlBody, baseURL: nil)
        }
        webView.delegate = self
        webView.isHidden = true
        self.scroll.addSubview(webView)
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(.zero)

        webView.scrollView.isScrollEnabled = false
        webView.isHidden = false
        self.scroll.contentSize = CGSize(width: webView.frame.size.width, height: webView.frame.origin.y + webView.frame.size.height + margin.y)
    }

/*
 [parameters addObject: [ NSString stringWithFormat:@"id=%@", currentRequest.requestedDetailID ] ];
 [parameters addObject:@"detail=full"];
 [parameters addObject:@"richness=html"];
*/

    func addSectionLabel(origin:CGPoint, container:UIView) -> UILabel {
        let section = self.addLabelWithTag(tag: 100, font: UIFont.cellSection, textColor: UIColor.separatorFont, container: container)
        section.frame = CGRect(origin: origin, size: CGSize(width:300, height:100))
        section.text = self.item.sectionTitle.uppercased()
        if self.item.specialTextColor != nil {
            section.textColor = self.item.specialTextColor
        }
        section.sizeToFit()
        return section
    }

    func addTitleLabel(container:UIView, bigFontSize:Bool = true, under:CGRect? = nil, indent:CGFloat = 0, maxHeight:CGFloat? = 65) -> UILabel {
        let title = self.addLabelWithTag(tag: 101, font: Theme.shared.fonts.detailTitleFont, textColor: .black, container: container, multiline:true)
        if under != nil {
            title.text = self.item.title
            let finalFrame = CGRect(origin: CGPoint(x: (under?.origin.x)!, y: (under?.origin.y)! + (under?.size.height)! + indent), size: CGSize(width:container.frame.size.width - ((under?.origin.x)! * 2), height: maxHeight! * BasicCellSize.sizeFactor))
            title.frame = finalFrame
            title.fitFontForSize(constrainedSize: title.frame.size, maxFontSize: 80.0, minFontSize: 14.0)
        }
        return title
    }

    func addLabelWithTag(tag:Int! = 0, font:UIFont, textColor:UIColor, container:UIView, multiline:Bool = false) -> UILabel {
        let label = UILabel.init()
        if tag != 0 {
            container.viewWithTag(tag)?.removeFromSuperview()
            label.tag = tag
        }
        label.font = font
        label.textColor = textColor
        if multiline {
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping
        }
        container.addSubview(label)
        return label
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == .linkClicked {
            print(request, request.url ?? "")
            return false
        }
        return true
    }

    func loadDataFrom(url:String) {
        print("->", url)
        Alamofire.request(url).responseJSON { response in
            if response.result.value as? [AnyObject] != nil {
                self.setDataResponse(responseData: response.result.value as! [AnyObject])
            } else if response.result.value as AnyObject! != nil {
//                self.setDataResponse(responseData: [response.result.value as AnyObject])
            } else {
                print(response)
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)
            }
        }
    }

    func setDataResponse(responseData:[AnyObject]) {
        _ = self.getParsedElements(array: responseData as! [[String : AnyObject]])
    }

    func getParsedElements(array:[[String:AnyObject]]) -> [BasicCellData] {
        for element in array {
            let temp = self.getParsedElement(index: 0, element: element)
            if temp.slug == item.slug {
                item.detailHtml = temp.detailHtml
                setDataContent()
                return []
            }
        }
        return []
    }

    func getParsedElement(index:Int, element:[String:AnyObject]) -> BasicCellData {
        return BasicCellData.init(index: index, rawJSON: element)
    }

}
