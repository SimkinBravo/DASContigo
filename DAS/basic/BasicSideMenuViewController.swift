//
//  BasicViewController.swift
//  lajornada
//
//  Created by Simkin Bravo on 10/11/16.
//  Copyright © 2016 La Jornada. All rights reserved.
//

import UIKit
import SideMenu

class BasicSideMenuViewController: UIViewController {

    var leftSideMenuButtonName: String! = "left-menu-button"
    var leftSideMenuControllerName: String = "LeftSideMenuNavigationController"
    var sideMenuBackgroundColor: UIColor = UIColor.headerBackground

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupBasicSideMenuViewController()
    }

    func setupBasicSideMenuViewController() {
        self.view.backgroundColor = UIColor.white
        if leftSideMenuButtonName != nil {
            let leftBtn = UIButton(type: .custom)
            let leftImg = UIImage(named: leftSideMenuButtonName)
            leftBtn.setImage(leftImg, for: .normal)
            leftBtn.frame = CGRect(origin: CGPoint(), size: (leftImg?.size)!)
            leftBtn.addTarget(self, action: #selector(leftMenuButtonTouchedDown(_:)), for: .touchDown)
            self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: leftBtn), animated: true)
        }
        if ( SideMenuManager.default.menuLeftNavigationController == nil ) {
            let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: leftSideMenuControllerName) as? UISideMenuNavigationController
            menuLeftNavigationController?.leftSide = true
            SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
            SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
            SideMenuManager.default.menuAnimationBackgroundColor = sideMenuBackgroundColor
        }
    }

    @objc func leftMenuButtonTouchedDown(_ button: UIButton) {
        self.navigationController?.present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }

}
