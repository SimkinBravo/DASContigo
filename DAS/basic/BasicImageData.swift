//
//  BasicImageData.swift
//  lajornada
//
//  Created by Simkin Bravo on 14/11/16.
//  Copyright © 2016 La Jornada. All rights reserved.
//

import UIKit

class BasicImageData: NSObject {

    var slug:String!
    var title:String!
    var desc:String!
    var alt:String!
    var thumbURL:String!
    var url:String!
    var sectionSlug:String!
    var showIcon:Bool = false
    var roundedCourners: Bool = false

    var cellSize:CGSize!

    var index:Int!
    var imageTag:Int!
    var imageName: String!

    var adjustImageViewSize:Bool = false

    init (rawJSON: Dictionary<String, Any>) {
        super.init()
        self.setRawJSON(rawJSON: rawJSON)
    }

    init (index: Int, rawJSON: Dictionary<String, Any>) {
        super.init()
        self.index = index
        self.cellSize = index == 0 ? CGSize(width: UIScreen.main.bounds.width, height: 400 * BasicCellSize.sizeFactor) :
            CGSize(width: UIScreen.main.bounds.width, height: 120 * BasicCellSize.sizeFactor)
        self.setRawJSON(rawJSON: rawJSON)
    }
/*
 "fotografia_principal_mediana_url": "",
 //	"description": "Atenas.  La noche del 6",
 //	"fotografia_principal_descripcion": "Atenas.  La noche del 6 de diciembre de 2008",
	"fotografia_principal_thumbnail_url": "",
 //	"titulo": "Manifestantes y polic\u00edas se enfrentan en Atenas ",
	"fecha": "2016-12-06 14:21:57",
	"site_url": "http://www.jornada.unam.mx/ultimas/2016/12/06/manifestantes-y-policias-se-enfrentan-en-atenas",
	"fotografia_principal_id": "564044cbb4cf4e848cdb2601726b4c8c",
 //	"id": "manifestantes-y-policias-se-enfrentan-en-atenas",
	"fotografia_principal_url": ""
 */
    init (galleryRawJSON: Dictionary<String, Any>) {
        self.title = galleryRawJSON["titulo"] as! String!
        self.desc = galleryRawJSON["titulo"] as! String!
        self.thumbURL = galleryRawJSON["fotografia_principal_thumbnail_url"] as! String!
        if self.thumbURL == nil {
            self.thumbURL = galleryRawJSON["fotografia_small_url"] as! String!
            self.desc = galleryRawJSON["description"] as! String!
        }
        self.url = galleryRawJSON["fotografia_mediana_url"] as! String!
    }

    init (title: String, desc: String, thumbURL: String) {
        self.title = title
        self.desc = desc
        self.thumbURL = thumbURL
    }

    func setRawJSON(rawJSON: Dictionary<String, Any>) {
        self.slug = rawJSON["id"] as! String!
        self.sectionSlug = rawJSON["section"] as! String!
        self.title = rawJSON["alt"] as! String!
        self.desc = rawJSON["caption"] as! String
        self.thumbURL = rawJSON["snap"] as! String!
        self.url = rawJSON["url"] as! String!
        self.alt = rawJSON["alt"] as! String!
    }

}
/*
 "images": <__NSArrayI 0x170038cc0>(
 {
 alt = "Teapa tabasco.jpg";
 author = "";
 caption = "Elementos de la Secretar\U00eda de la Defensa Nacional est\U00e1n evacuando a los habitantes para llevarlos a los refugios improvisados por el ayuntamiento de la localidad. Foto Ren\U00e9 Alberto L\U00f3pez";
 header = "";
 id = "http://www.jornada.unam.mx/ultimas/2016/11/13/tabasco-mas-de-mil-afectados-por-lluvias-entra-en-vigor-plan-dn-iii/teapa-tabasco.jpg";
 kind = content;
 snap = "http://www.jornada.unam.mx/ultimas/2016/11/13/tabasco-mas-de-mil-afectados-por-lluvias-entra-en-vigor-plan-dn-iii/teapa-tabasco.jpg/image_medium";
 url = "http://www.jornada.unam.mx/ultimas/2016/11/13/tabasco-mas-de-mil-afectados-por-lluvias-entra-en-vigor-plan-dn-iii/teapa-tabasco.jpg";
 },
 {
 alt = "13Teapa tabasco.jpg";
 author = "";
 caption = "M\U00e1s de mil afectados en al menos nueve comunidades inundadas del municipio serrano de Teapa, son al momento los resultados a causa del desbordamiento de los r\U00edos Teapa y Pichucalco. Foto Ren\U00e9 Alberto L\U00f3pez";
 header = "";
 id = "http://www.jornada.unam.mx/ultimas/2016/11/13/tabasco-mas-de-mil-afectados-por-lluvias-entra-en-vigor-plan-dn-iii/13teapa-tabasco.jpg";
 kind = content;
 snap = "http://www.jornada.unam.mx/ultimas/2016/11/13/tabasco-mas-de-mil-afectados-por-lluvias-entra-en-vigor-plan-dn-iii/13teapa-tabasco.jpg/image_medium";
 url = "http://www.jornada.unam.mx/ultimas/2016/11/13/tabasco-mas-de-mil-afectados-por-lluvias-entra-en-vigor-plan-dn-iii/13teapa-tabasco.jpg";
 }
*/
