//
//  BasicTableViewController.swift
//  lajornada
//
//  Created by Simkin Bravo on 15/11/16.
//  Copyright © 2016 La Jornada. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import NVActivityIndicatorView

class HCollectionData: NSObject {

    var collection:UICollectionView!
    var data:BasicCellData!

    init(collection:UICollectionView, data:BasicCellData) {
        self.collection = collection
        self.data = data
    }

}

class BasicTableViewController: UITableViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    var elements:[[BasicCellData]] = []
    var loadErrors:[[String:AnyObject]] = []
    var data:BasicPageData!

    var doubleACellTag:Int = 120000
    var doubleBCellTag:Int = 130000
    var lastSelectedDouble:BasicDoubleCellData!

    var loadIndicator:Int = 0
    var hCollections:[HCollectionData] = []
    var activityIndicator:NVActivityIndicatorView!
    var activityIndicatorType:NVActivityIndicatorType = .orbit
    var activityIndicatorColor:UIColor = UIColor.tabBarTint
    var activityLabel = UILabel()

    var placeholderName:String = "placeholder"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = .none
        self.setActivityIndicator()
        self.loadData()
        self.refreshControl?.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.tableView.indexPathForSelectedRow != nil {
            self.tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
        }
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.elements.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.elements[section].count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let element:BasicCellData = self.elements[indexPath.section][indexPath.row]
        return element.cellSize == nil ? 150 : element.cellSize.height
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = elements[indexPath.section][indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: getCellReuseId(data), for: indexPath)
        cell.backgroundColor = self.tableView.backgroundColor
        cell.isUserInteractionEnabled = data.userInteractionEnabled
        data.indexPath = indexPath
        if isCellACollection(data) {
            setupHCollectionCell(cell: cell, data: data)
        }
        return data.configCell(cell: cell)
    }

    func getCellReuseId(_ data: BasicCellData) -> String {
        if data.cellId != nil {
            return data.cellId.rawValue
        }
        return data.cellReuseID
    }

    func isCellACollection(_ data: BasicCellData) -> Bool {
        if data.cellReuseID != nil {
            return data.cellReuseID == BasicCellID.collection
        }
        return false
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let data:BasicCellData = elements[indexPath.section][indexPath.row]
        if data.images.count != 0 && !data.isImageHidden {
            for image in data.images {
                if image.imageTag != nil {
                    let imageContainer = cell.contentView.viewWithTag(image.imageTag) as! UIImageView
                    if imageContainer != nil && !image.thumbURL.isEmpty {
                        let url:URL = URL(string: image.thumbURL)!
                        self.loadImage(imageView: imageContainer, url:url, data:image, cellData: data)
                    }
                }
            }
        }
        if data.hCollection != nil {
            data.hCollectionView = cell.viewWithTag(7000)!.viewWithTag(10) as! UICollectionView
            data.hCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .left, animated: false)
            data.hCollectionView.reloadData()
        }
    }

    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section >= elements.count || indexPath.row >= elements[indexPath.section].count {
            return
        }
        let data:BasicCellData = elements[indexPath.section][indexPath.row]
        if data.hCollection != nil && data.hCollectionView != nil {
            data.hCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .left, animated: false)
            data.hCollectionView.delegate = nil
            data.hCollectionView.dataSource = nil
            data.hCollectionView = nil
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableViewDidSelectData(data: elements[indexPath.section][indexPath.row])
    }

    func tableViewDidSelectData(data:BasicCellData) {
        deselectLastDoubleSelected()
        print("Select: ", data.indexPath!)
    }

    func deselectLastDoubleSelected() {
        if lastSelectedDouble != nil {
            lastSelectedDouble!.deselect()
            lastSelectedDouble = nil
        }
    }


    // MARK: - Custom funcs
    @objc func refresh(sender:AnyObject) {
        self.tableView.alpha = 0.5
        _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.loadData), userInfo: nil, repeats: false)
    }

    @objc func loadData() {
        showLoader()
    }

    func setActivityIndicator() {
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80), type: activityIndicatorType, color: activityIndicatorColor, padding: 0)
        activityIndicator.center = self.view.center
        activityIndicator.frame = CGRect(origin: CGPoint(x: activityIndicator.frame.origin.x, y: activityIndicator.frame.origin.y - (self.view.frame.height * 0.2)), size: activityIndicator.frame.size)
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        activityLabel.textAlignment = .center
        activityLabel.textColor = activityIndicatorColor
        activityLabel.frame = CGRect(x: 50, y: activityIndicator.frame.origin.y + 100, width: self.view.frame.width - 100, height: 50)
        self.view.addSubview(activityLabel)
    }

    func onLoadImageResponse(imageVw: UIImageView, image: UIImage, imageData: BasicImageData, cellData: BasicCellData) {
        imageVw.backgroundColor = .clear
    }

    //let bannerView = DFPBannerView(adSize: kGADAdSizeSmartBannerPortrait)

    func loadImage(imageView:UIImageView, url:URL, data:BasicImageData, cellData: BasicCellData) {
        let currentPlaceholder: String = data.imageName == nil || data.imageName.isEmpty ? placeholderName : data.imageName
        let placeholderImage = UIImage(named: currentPlaceholder)
        let currentContentMode = imageView.contentMode
        imageView.contentMode = .scaleAspectFit
        if !data.adjustImageViewSize {
                imageView.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: data.roundedCourners ? AspectScaledToFillSizeWithRoundedCornersFilter (size: imageView.frame.size, radius:6.0) : AspectScaledToFillSizeFilter (size: imageView.frame.size), imageTransition: .crossDissolve(0.3), completion: { response in
                imageView.contentMode = currentContentMode
                if response.result.error != nil {
                    print(response.result.error ?? "Sin mensaje de error")
                } else {
                    self.onLoadImageResponse(imageVw: imageView, image: response.result.value!, imageData: data, cellData: cellData)
                }
            })
        } else {
            let filter = RoundedCornersFilter (radius: 4.0)
            imageView.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.3),  completion: { response in
                self.onLoadImageResponse(imageVw: imageView, image: response.result.value!, imageData: data, cellData: cellData)
            })
        }
    }

    func loadDataFrom(url:String) {
        print("->", url)
        Alamofire.request(url).responseJSON { response in
            if response.result.value as? [AnyObject] != nil {
                self.setDataResponse(responseData: response.result.value as! [AnyObject])
            } else if response.result.value as AnyObject! != nil {
                self.setDataResponse(responseData: [response.result.value as AnyObject])
            } else {
                print(response)
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)
                self.loadErrors.append(["url": response.request as AnyObject, "error": "No pudimos conectar con el servidor" as AnyObject, "resultCode":response.result as AnyObject])
            }
            self.onLoadDataResponse()
        }
    }

    func loadXMLFrom(url: String) {
        print("->", url)
        Alamofire.request(url).response { response in
            if let data = response.data {
                self.onLoadXMLResponse(response: data)
            }
        }
    }

    func showLoader(isHidden:Bool = false, text: String! = "") {
        if !isHidden {
            activityIndicator.startAnimating()
            activityLabel.text = text
        } else {
            activityIndicator.stopAnimating()
        }
        activityIndicator.isHidden = isHidden
        activityLabel.isHidden = isHidden
    }

    func setDataResponse(responseData:[AnyObject]) {
        self.elements.append(self.getParsedElements(array: responseData as! [[String : AnyObject]]))
    }

    func onLoadDataResponse() {
        self.showLoader(isHidden: true)
        self.refreshControl?.endRefreshing()
        self.tableView.alpha = 1.0
        self.tableView.reloadData()
    }

    func onLoadXMLResponse(response: Data) {
        self.showLoader(isHidden: true)
    }

    func getParsedElements(array:[[String:AnyObject]]) -> [BasicCellData] {
        var tempElements:[BasicCellData] = []
        for element in array {
            tempElements.append(self.getParsedElement(index: tempElements.count, element: element))
        }
        return tempElements
    }

    func getParsedElement(index:Int, element:[String:AnyObject]) -> BasicCellData {
        return BasicCellData.init(index: index, rawJSON: element)
    }

    func setupDoubleCell(slug: String, maintitle: String, secondaryTitle: String, a: BasicCellData, b: BasicCellData!, haveSectionHeader: Bool = false) -> BasicDoubleCellData {
        if haveSectionHeader {
            a.sectionTitle = ""
            if b != nil {
                b.sectionTitle = ""
            }
        }
        let doubleData = BasicDoubleCellData(slug: slug, maintitle: maintitle, secondaryTitle: secondaryTitle, a: a, b: b)
        doubleData.aButton.addTarget(self, action: #selector(doubleButtonCellAction), for: .touchUpInside)
        doubleData.bButton.addTarget(self, action: #selector(doubleButtonCellAction), for: .touchUpInside)
        return doubleData
    }

    @objc func doubleButtonCellAction(sender: BasicDataButton!) {
        self.tableView.selectRow(at: sender.data.indexPath, animated: false, scrollPosition: .none)
        self.tableViewDidSelectData(data: sender.data)
        lastSelectedDouble = sender.parentData
    }

    func setupHCollectionCell(cell: UITableViewCell, data: BasicCellData) {
        let collectionView:UICollectionView = cell.viewWithTag(7000)!.viewWithTag(10) as! UICollectionView
        data.hCollectionView = collectionView
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
    }

    func getCollectionData(_ collection: UICollectionView) -> BasicCellData! {
        for sections in self.elements {
            for element in sections {
                if element.cellReuseID != nil && element.cellReuseID == BasicCellID.collection {
                    if element.hCollectionView != nil, element.hCollectionView == collection {
                        return element
                    }
                }
            }
        }
        return nil
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let data = getCollectionData(collectionView)
        return data != nil ? data!.hCollection.count : 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellData = getCollectionData(collectionView)!
        let data = cellData.hCollection[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: data.cellReuseID, for: indexPath)
        cell.frame = CGRect(origin: cell.frame.origin, size: data.cellSize)
        cell.backgroundColor = collectionView.backgroundColor
        cell.isUserInteractionEnabled = data.userInteractionEnabled
        data.indexPath = indexPath
        return data.configCollectionCell(cell: cell)
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cellData = getCollectionData(collectionView)!
        let data = cellData.hCollection[indexPath.row]
        if data.images.count != 0 && !data.isImageHidden {
            for image in data.images {
                if image.imageTag != nil {
                    let imageContainer = cell.contentView.viewWithTag(image.imageTag)
                    if imageContainer != nil && image.thumbURL != nil {
                        let url:URL = URL(string: image.thumbURL)!
                        self.loadImage(imageView: imageContainer as! UIImageView, url:url, data:image, cellData: data)
                    }
                }
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellData = getCollectionData(collectionView)!
        self.tableViewDidSelectData(data: cellData.hCollection[indexPath.row])
    }

    override var shouldAutorotate: Bool {
        return false
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }

}
