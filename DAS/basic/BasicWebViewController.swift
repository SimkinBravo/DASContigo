//
//  BasicWebViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 02/02/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BasicWebViewController: UIViewController {

    var url: String!
    var activityIndicator: NVActivityIndicatorView!
    var webviewScalesToFit: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        let webview = UIWebView(frame: self.view.frame)
        webview.delegate = self
        self.view.addSubview(webview)
        setupActivityIndicator()
        webview.loadRequest(URLRequest(url: URL(string: url)!))
        webview.scalesPageToFit = webviewScalesToFit
    }

    func setupActivityIndicator() {
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80), type: NVActivityIndicatorType.ballClipRotate, color: Theme.shared.colors.navigationBarColor3, padding: 0)
        activityIndicator.center = self.view.center
        activityIndicator.frame = CGRect(origin: CGPoint(x: activityIndicator.frame.origin.x, y: activityIndicator.frame.origin.y - (self.view.frame.height * 0.2)), size: activityIndicator.frame.size)
        self.view.addSubview(activityIndicator)
        activityIndicator.isHidden = true
    }

}

extension BasicWebViewController: UIWebViewDelegate {

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == .linkClicked {
            return false
        }
        return true
    }

    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
    }

}
