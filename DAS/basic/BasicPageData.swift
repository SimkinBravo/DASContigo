//
//  BasicPageData.swift
//  lajornada
//
//  Created by Simkin Bravo on 13/11/16.
//  Copyright © 2016 La Jornada. All rights reserved.
//

import UIKit

class BasicPageData: NSObject {

    var id:Int!
    var slug:String!
    var title:String!
    var controllerID:String!
    var url:String!
    var apiUrl: String!
    var printEdition:Bool = false
    var rawData:[String:AnyObject]!
    var pageParentVC:BasicPageMenuViewController!
    var sendId:String!
    var color:UIColor!
    var sectionColors: [String: UIColor]!
    var icon:String!

    var videosStr:[String]!

    init(slug: String, title: String) {
        self.slug = slug
        self.title = title
    }

    convenience init (slug: String, title: String, controllerID: String, printEdition: Bool = false) {
        self.init(slug: slug, title: title)
        self.controllerID = controllerID
        self.printEdition = printEdition
    }

    convenience init (slug: String, title: String, controllerID: String, color: UIColor) {
        self.init(slug: slug, title: title, controllerID: controllerID)
        self.color = color
    }

    convenience init (slug: String, title: String, controllerID: String, color: UIColor, icon: String) {
        self.init(slug: slug, title: title, controllerID: controllerID, color: color)
        self.icon = icon
    }

    init (rawData: [String:AnyObject]) {
        self.url = rawData["url"] as! String!
        self.title = rawData["nombre"] as! String!
        self.slug = rawData["slug"] as! String!
        self.videosStr = rawData["videos"] as! [String]!
        self.controllerID = "BasicVideoTableViewController"
        self.rawData = rawData
    }

    init (rawBlogData: [String:AnyObject]) {
//        self.url = rawBlogData["url"] as! String!
//        print(rawBlogData)
        self.title = rawBlogData["titulo_blog"] as! String!
        if !(rawBlogData["search_id_blog"] is NSNull) {
            self.slug = rawBlogData["search_id_blog"] as! String!
        }
        self.controllerID = "SingleBlogTableViewController"
        self.rawData = rawBlogData
    }

}
