//
//  JOVideoData.swift
//  lajornada
//
//  Created by Simkin Bravo on 07/12/16.
//  Copyright © 2016 La Jornada. All rights reserved.
//

import UIKit

class BasicVideoData: NSObject {

    var id:NSNumber!
    var slug:String!
    var title:String!
    var desc:String!
    var thumbURL:String!
    var duration:String!
    var url:String!
    var fileUrl:String!
    var hlsUrl:String!

    var seconds:Int!

    var videoSize:CGSize!

    init (index: Int) {
        
    }

    init (index: Int, rawJSON: [String: AnyObject]) {
//        print(rawJSON)
        self.id = rawJSON["id"] as! NSNumber!
        self.slug = rawJSON["slug"] as! String!
        self.title = rawJSON["titulo"] as! String!
        self.desc = rawJSON["descripcion"] as! String!
        self.duration = rawJSON["duracion"] as! String!
//        self.seconds = rawJSON["segundos"] as! Int!
        self.videoSize = CGSize(width: rawJSON["width"] as! Int!, height: rawJSON["height"] as! Int!)
        self.url = rawJSON["api_url"] as! String!
        self.fileUrl = rawJSON["archivo_url"] as! String!
        if let hls = rawJSON["hls_url"] as? String {
            self.hlsUrl = hls
        } else {
            self.hlsUrl = self.fileUrl
        }
        /*
        if let streaming = rawJSON["streaming"] as? [String: AnyObject] {
            if let url = streaming["rtsp_url"] as? String {
                self.hlsUrl = url
            }
        }
        */
        self.thumbURL = rawJSON["thumbnail_mediano"] as! String!
    }

}
