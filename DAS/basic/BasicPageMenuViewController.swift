//
//  BasicPageMenuViewController.swift
//  lajornada
//
//  Created by Simkin Bravo on 10/11/16.
//  Copyright © 2016 Open Multimedia. All rights reserved.
//

import UIKit
import PageMenu

class BasicPageMenuViewController: UIViewController, CAPSPageMenuDelegate {

    var pageMenu: CAPSPageMenu?
    var pageMenuHeight: CGFloat = 80.0
    var pageMenuMargin: CGFloat = 30.0
    var pagedControllers: [BasicPageData]!
    var data:BasicPageData!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.setupBasicPageViewController()
    }

    internal func setupBasicPageViewController() {
        if pagedControllers == nil {
            setBasicPageMenuDefaultData()
        }
        var controllerArray:[UIViewController] = []
        for data:BasicPageData in pagedControllers {
            data.pageParentVC = self
            let controller = data.controllerID == nil || data.controllerID.isEmpty ? UIViewController() : storyboard?.instantiateViewController(withIdentifier: data.controllerID)
            if ( controller as? BasicTableViewController != nil ) {
                (controller as! BasicTableViewController).data = data
            }
            controller?.title = data.title
            controllerArray.append(controller!)
//            self.childViewControllers.append(controller)
//            self.navigationController?.viewControllers.append(controller!)
        }
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: getPageViewControllerFrame(), pageMenuOptions: getPageMenuParameters())
        pageMenu?.delegate = self;
        self.view.addSubview(pageMenu!.view)
    }

    func getPageViewControllerFrame() -> CGRect {
        return CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height:self.view.frame.height)
    }

    func getPageMenuParameters() -> [CAPSPageMenuOption] {
        return [
                .scrollMenuBackgroundColor(Theme.shared.colors.pageControllerBackgroundColor),
                .viewBackgroundColor(Theme.shared.colors.viewBackgroundColor),
                .selectionIndicatorColor(UIColor.clear),
                .bottomMenuHairlineColor(Theme.shared.colors.cellSeparatorColor),
                .menuItemWidthBasedOnTitleTextWidth(true),
                .menuItemFont(Theme.shared.fonts.pageControllerSelectedItemFont),
                .menuHeight(pageMenuHeight),
                .menuItemUnselectedFont(Theme.shared.fonts.pageControllerUnselectedItemFont),
                .selectedMenuItemLabelColor(UIColor.pageMenuSelectedItem),
                .unselectedMenuItemLabelColor(UIColor.pageMenuItem),
                .menuMargin(pageMenuMargin),
                .centerMenuItems(true)
        ]
    }

    func setBasicPageMenuDefaultData() {
        pagedControllers = []
    }

    public func goToPage(slug:String) {
        var index:Int = -1
        var i = 0
        for data:BasicPageData in pagedControllers {
            if data.slug == slug {
                index = i
            }
            i = i + 1
        }
        if index >= 0 {
            pageMenu?.moveToPage(index)
        }
    }

}
