//
//  BasicCellData.swift
//  lajornada
//
//  Created by Simkin Bravo on 15/11/16.
//  Copyright © 2016 La Jornada. All rights reserved.
//

import UIKit
import MapKit

enum CellContentStyle {
    case vertical
    case horizontal
    case collection
    case fullImage
    case separator
    case sectionSeparator
    case custom
    case custom1
    case custom2
    case chat
    case customExtended
}

enum CellId: String {
    typealias RawValue = String

    case basic = "BasicCell"
    case highlighted = "BasicHighlightedCell"
    case separator = "SeparatorCell"
    case custom = "CustomCell"
    case custom2 = "CustomCell2"
    case custom3 = "CustomCell3"
    case custom4 = "CustomCell4"
    case custom5 = "CustomCell5"
}

struct BasicCellID {
    static var principalImage:String = "PrincipalImage"
    static var textHighlighted:String = "TextHighlightedCell"
    static var tripleImage:String = "TripleImageCell"
    static var doubleImage:String = "DoubleImageCell"
    static var highlighted:String = "BasicHighlightedCell"
    static var double:String = "DoubleCell"
    static var basic:String = "BasicCell"
    static var basicImage:String = "BasicImageCell"
    static var basicExtra:String = "BasicExtraCell"
    static var separator:String = "SeparatorCell"
    static var separatorS:String = "SeparatorSCell"
    static var color:String = "ColorCell"
    static var basicColumn:String = "BasicColumn"
    static var fullImage:String = "FullImageCell"
    static var fullImageWSection:String = "FullImageWithSectionCell"
    static var authorContent:String = "AuthorContentCell"
    static var blogArticle:String = "BlogArticleCell"
    static var blogHeader:String = "BlogHeaderCell"
    static var collection:String = "CollectionCell"
    static var empty:String = "Empty"
}

struct IconData {

    var index: Int = 0
    var imageName: String!
    var positionPercent: CGPoint = CGPoint(x: 0.5, y: 0.5)
    var scale: CGFloat = 1.0
    var isDropShadowEnabled: Bool = false

    init (index: Int, imageName: String, positionPercent: CGPoint = CGPoint(x: 0.5, y: 0.5), scale: CGFloat = 1.0, isDropShadowEnabled: Bool = false) {
        self.index = index
        self.imageName = imageName
        self.positionPercent = positionPercent
        self.scale = scale
        self.isDropShadowEnabled = isDropShadowEnabled
    }

}

struct BasicCellSize {
    static var sizeFactor = UIScreen.main.bounds.width / 320.0
    static var principalImage:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 300 * sizeFactor)
    static var textHighlighted:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 200 * sizeFactor)
    static var tripleImage:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 385 * sizeFactor)
    static var doubleImage:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 280 * sizeFactor)
    static var highlighted:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 200 * sizeFactor)
    static var double:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 180 * sizeFactor)
    static var doubleS:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 255 * sizeFactor)
    static var basic:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 120 * sizeFactor)
    static var basicImage:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 150 * sizeFactor)
    static var basicExtra:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 130 * sizeFactor)
    static var separator:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 40 * sizeFactor)
    static var separatorM:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 60 * sizeFactor)
    static var separatorS:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 80 * sizeFactor)
    static var color:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 150 * sizeFactor)
    static var collection:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 180 * sizeFactor)
    static var collectionFullImage:CGSize = CGSize(width: 152 * sizeFactor, height: 185 * sizeFactor)
    static var blogFullImage:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 110 * sizeFactor)
    static var cartoonFullImage:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 210 * sizeFactor)
    static var hFullImage:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 260 * sizeFactor)
    static var authorContent:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 320 * sizeFactor)
    static var blogArticle:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 150 * sizeFactor)
    static var blogHeader:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 100 * sizeFactor)
    static var ad:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 270)
    static var lines1:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 40 * sizeFactor)
    static var lines2:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 60 * sizeFactor)
    static var lines3:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 80 * sizeFactor)
    static var lines4:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 100 * sizeFactor)
    static var lines5:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 120 * sizeFactor)
}

class BasicCellData: NSObject {

    static var sizeFactor = UIScreen.main.bounds.width / 320.0
    public let separatorTag = 123123

    public var appointment: Appointment!
    public var id:String!
    public var slug:String = ""
    public var title:String!
    public var apiURL:String!
    public var siteURL:String!
    public var desc:String!
    public var imageURL:String!
    public var cellReuseID:String!
    public var sectionSlug:String!
    public var sectionTitle:String = ""
    public var type:String!
    public var url:String!
    public var imagename:String!
    public var detailUrl:String!
    public var publishedAt:Date!
    public var userInteractionEnabled:Bool = true
    public var cellAutoResize:Bool = false
    public var specialTextColor:UIColor!
    public var detail: String!
    public var extra: String!
    public var serviceType: String!
    public var specialities: [String] = []
    public var specialty: String!
    public var location: CLLocationCoordinate2D!
    public var detailHtml: String!
    public var adId: String!
    public var author: String!
    public var phone: String!
    public var contentStyle: CellContentStyle!
    public var cellId: CellId!
    public var expiresAt: String!
    public var icons: [IconData] = [IconData]()

    public var video:BasicVideoData!
    public var showIcon:Bool = false
    public var cellSize:CGSize!
    public var adjustFontSize:Bool = true

    public var hCollection:[BasicCellData]!
    public var hCollectionView:UICollectionView!

    public var index:Int!

    public var imageMargin:CGPoint = CGPoint()
    public var imagePercent:CGFloat = 0.6
    public var imageRounded: Bool = false
    public var iconSizePercent:CGFloat = 1.5
    public var iconPositionPercent:CGPoint = CGPoint(x: 0.5, y: 0.5)
    public var iconImagename:String = "cell-video-icon"
    public var iconDropShadow: Bool = false
    public var imageFirst:Bool = true
    public var textMargin:CGPoint = CGPoint(x: 8, y: 3)
    public var separatorMargin:CGPoint = CGPoint(x: 0, y: 0)
    public var textOffset: CGFloat = 0.0
    public var textROffset: CGFloat = 0.0
    public var textPercent:CGFloat = 1.0
    public var titleHeightPercent:CGFloat = 0.4
    public var titleTextColor: UIColor = Theme.shared.colors.cellTitleColor
    public var titleFont = Theme.shared.fonts.cellTitleFont
    public var descTextColor:UIColor = .black
    public var textDropShadow:Bool = false
    public var placeholderBackgroundColor:UIColor = .lightGray
    public var isDescriptionHidden: Bool = false
    public var isSeparatorHidden: Bool = true
    public var isImageHidden: Bool = false
    public var backgroundColor: UIColor!
    public var descFont = UIFont.defaultRegular.withSize(10.0)
    public var isClient: Bool = true
    public var attachmentString: NSAttributedString!

    var labelsTag:Int = 10000
    public var imagesTag:Int = 9000
    var initialIconTag:Int = 8000
    var viewsTag:Int = 7000
    let marginA:CGPoint = CGPoint(x: 16, y: 8)
    let marginB:CGPoint = CGPoint(x: 20, y: 25)
    let marginC:CGPoint = CGPoint(x: 10, y: 20)
    let marginD:CGPoint = CGPoint(x: 10, y: 10)
    let marginE:CGPoint = CGPoint(x: 15, y: 15)

    public var textAlignment: NSTextAlignment = .left

    public var images:[BasicImageData] = []
    var currentImage:Int = 0

    public var indexPath:IndexPath!

    init (slug: String, title: String = "", subtitle: String = "", imageURL: String = "") {
        self.slug = slug
        self.title = title
        self.desc = subtitle
        if !imageURL.isEmpty {
            self.imageURL = imageURL
        }
    }

    init (title: String, subtitle: String = "") {
        self.title = title
        self.desc = subtitle
    }

    init (index: Int, rawJSON: Dictionary<String, AnyObject>) {
        super.init()
        self.setRawJSON(index: index, rawJSON: rawJSON)
    }

    init (index: Int, rawVideo: [String: AnyObject]) {
        super.init()
        self.setVideosRawJSON(index: index, rawJSON: rawVideo)
    }

    init (rawJSON: Dictionary<String, AnyObject>) {
        super.init()
        self.setRawJSON(rawJSON: rawJSON)
    }

    internal func setRawJSON(index: Int, rawJSON: [String:AnyObject]) {
        self.index = index
        self.cellReuseID = index == 0 ? BasicCellID.highlighted : BasicCellID.basic
        self.cellSize = index == 0 ? BasicCellSize.highlighted : BasicCellSize.basic
        self.cellAutoResize = index == 0
        self.setRawJSON(rawJSON: rawJSON)
    }

    func setRawJSON(rawJSON: Dictionary<String, AnyObject>) {
        print(rawJSON)
    }

    func setVideosRawJSON(index: Int, rawJSON:[String: AnyObject], setImage:Bool = true) {
        self.index = index
        self.video = BasicVideoData.init(index: 0, rawJSON: rawJSON)
        self.title = video.title
        self.desc = video.desc
        if let aUrl = rawJSON["api_url"] as? String {
            self.apiURL = aUrl
        }
        self.publishedAt = RFC912DateFormatter().date(from: rawJSON["fecha"] as! String)
        if setImage == true {
            self.images = [BasicImageData.init(title: "", desc: "", thumbURL: video.thumbURL)]
            self.images.first?.imageTag = self.imagesTag
        }
        if let categoryObj = rawJSON["categoria"] as? [String:AnyObject] {
            self.sectionSlug = categoryObj["slug"] as! String
            self.sectionTitle = categoryObj["nombre"] as! String
        }
        self.cellReuseID = BasicCellID.fullImage
        self.cellSize = BasicCellSize.collectionFullImage
    }

    func configCell(cell:UITableViewCell) -> UITableViewCell {
        let container:UIView = cell.contentView.viewWithTag(self.viewsTag)!
        if cellSize != nil {
            cell.frame = CGRect(origin: cell.frame.origin, size: cellSize)
        }
        container.frame = CGRect(origin:CGPoint(), size:cell.frame.size)
        container.backgroundColor = cell.backgroundColor
        configViewCell(container: cell.contentView.viewWithTag(self.viewsTag)!)
        return cell
    }

    func configCollectionCell(cell:UICollectionViewCell) -> UICollectionViewCell {
        var container:UIView! = cell.contentView.viewWithTag(self.viewsTag)
        if container == nil {
            container = UIView()
            container.tag = self.viewsTag
            cell.contentView.addSubview(container)
        }
        if cellSize != nil {
            cell.frame = CGRect(origin: cell.frame.origin, size: cellSize)
        }
        container.frame = CGRect(origin:CGPoint(), size:cell.frame.size)
        container.backgroundColor = cell.backgroundColor
        configViewCell(container: cell.contentView.viewWithTag(self.viewsTag)!)
        return cell
    }

    func configColumnCell(container:UIView, atIndex:Int) {
        if !self.images.isEmpty && !isImageHidden {
            let imageHeight = (container.frame.size.height * self.imagePercent) - (self.imageMargin.y * 2)
            let imageFrame = CGRect(x: atIndex ==  0 ? self.imageMargin.x : self.imageMargin.x * 0.5, y: self.imageMargin.y, width: container.frame.size.width - (self.imageMargin.x * 1.5), height: imageHeight)
            _ = self.addImageViewWith(frame: imageFrame, tag: self.images[0].imageTag, container: container)
        } else {
            
        }
        let section:UILabel = self.addSectionLabel(origin: CGPoint(x: textMargin.x, y: (container.frame.size.height * (1 - textPercent)) + textMargin.y + textOffset), container: container)
        _ = self.addTitleLabel(container: container, under: section.frame, indent: textMargin.y)
        
/*
        let desc = self.addLabelWithTag(tag: labelsTag + 2, font: UIFont.defaultRegular.withSize(12.0), textColor: textColor, container: container, multiline: true)
        desc.text = self.desc
        let descY = title.frame.origin.y + title.frame.size.height + textMargin.y
        desc.frame = CGRect(x: textMargin.x, y: descY, width: title.frame.size.width, height: container.frame.height - descY)
        desc.sizeToFit()
*/
    }

    func configViewCell(container:UIView) {
        if contentStyle != nil {
            switch contentStyle {
                case .vertical:         self.setupVerticalStyle(container: container)
                case .horizontal:       self.setupHorizontalStyle(container: container)
                case .fullImage:        self.setupFullImageStyle(container: container)
                case .separator:        self.setupSeparatorStyle(container: container)
                case .sectionSeparator: self.setupSeparatorSectionCell(container: container)
                case .collection:       self.setupCollectionCell(container: container)
                case .custom:           self.setupCustomStyle(container: container)
                case .customExtended:   self.setupAccountCustomStyle(container: container)
                case .custom1:          self.setupAppointmentCustomStyle(container: container)
                case .custom2:          self.setupDoubleColumnCell(container: container)
                case .chat:             self.setupChatCustomStyle(container: container)
                case .none:             break
                case .some(_):          print("configViewCell:  some")
            }
        } else if cellReuseID != nil {
            if cellReuseID == BasicCellID.basic {
                self.setupCell(container: container)
            } else if cellReuseID == BasicCellID.basicExtra {
                self.setupBasicExtraCell(container: container)
            } else if cellReuseID == BasicCellID.highlighted {
                self.setupHighlightedCell(container: container)
            } else if cellReuseID == BasicCellID.separator {
                self.setupSeparatorCell(container: container)
            } else if cellReuseID == BasicCellID.separatorS {
                self.setupSeparatorSectionCell(container: container)
            } else if cellReuseID == BasicCellID.fullImage {
                self.setupFullImageCell(container: container)
            } else if cellReuseID == BasicCellID.collection {
                self.setupCollectionCell(container: container)
            }
        }
    }

    func setupVerticalStyle(container:UIView) {
        container.frame = CGRect(origin: container.frame.origin, size: self.cellSize)
        var titleRect = CGPoint(x: textMargin.x, y: (container.frame.size.height * (1 - textPercent)) + textMargin.y + textOffset)
        var imageView:UIImageView!
        if self.images.count != 0 {
            imageView = self.addImageView(container: container, under:CGRect(), heightPercent: imagePercent)
            titleRect = CGPoint(x: textMargin.x, y: imageView.frame.origin.y + imageView.frame.size.height)
        }
        setupIcons(container: container)
        let section:UILabel = self.addSectionLabel(origin: titleRect, container: container)
        let title = self.addTitleLabel(container: container, under: section.frame, indent: textMargin.y, center:false)
        if !isDescriptionHidden {
            let desc = self.addLabelWithTag(tag: labelsTag + 2, font: descFont, textColor: descTextColor, container: container, multiline: true)
            desc.text = self.desc
            let descY = title.frame.origin.y + title.frame.size.height + textMargin.y
            desc.frame = CGRect(x: textMargin.x, y: descY, width: container.frame.size.width - (textMargin.x * 2), height: container.frame.height - (descY + textMargin.y))
//        desc.sizeToFit()
        }
        self.addSeparator(container: container)
    }

    func setupIcons(container: UIView) {
        removeAllIcons(container: container)
        for icon in icons {
            setupIcon(container: container, data: icon)
        }
    }

    func removeAllIcons(container: UIView, startAt index: Int = 0) {
        if let icon = container.viewWithTag(initialIconTag + index) {
            icon.removeFromSuperview()
            removeAllIcons(container: container, startAt: index + 1)
        }
    }

    func setupIcon(container: UIView, data: IconData) {
        let iconImg = UIImage(named: data.imageName)
        let icon = UIImageView(image: iconImg)
        icon.clipsToBounds = false
        if data.isDropShadowEnabled {
            icon.layer.shadowColor = UIColor.black.cgColor
            icon.layer.shadowOpacity = 1
            icon.layer.shadowOffset = CGSize.zero
        }
        let iconSize = CGSize(width: (iconImg?.size.width)! * data.scale, height: (iconImg?.size.height)! * data.scale)
        icon.frame = CGRect(origin: CGPoint(x:(container.frame.size.width - iconSize.width) * data.positionPercent.x, y:(container.frame.size.height - iconSize.height)  * data.positionPercent.y), size: iconSize)
        icon.tag = initialIconTag + data.index
        container.addSubview(icon)
    }

    func setupHorizontalStyle(container:UIView) {
        container.frame = CGRect(origin: container.frame.origin, size: self.cellSize)
        if self.backgroundColor != nil {
            container.backgroundColor = self.backgroundColor
        }
        if self.images.count != 0 && !isImageHidden {
            _ = self.addImageViewD(container: container, widthPercent: imagePercent)
        }
        setupIcons(container: container)
        let section:UILabel = self.addSectionLabel(origin: textMargin, container: container)
        let title = self.addTitleLabel(container: container, under: section.frame, indent: textMargin.y)
        if self.attachmentString != nil {
            title.attributedText = self.attachmentString
        }
        if !isDescriptionHidden && !self.desc.isEmpty {
            let desc = self.addLabelWithTag(tag: labelsTag + 2, font: descFont, textColor: descTextColor, container: container)
            desc.text = self.desc
            desc.frame = title.frame
            desc.textAlignment = .right
        } else if let desc = container.viewWithTag(labelsTag + 2) as? UILabel {
            desc.text = ""
        }
        self.addSeparator(container: container)
    }

    func setupSeparatorStyle(container:UIView) {
        container.frame = CGRect(origin: container.frame.origin, size: self.cellSize)
        let back = UIView(frame: CGRect(x: imageMargin.x, y: imageMargin.y, width: container.frame.width - (imageMargin.x * 2), height: container.frame.height - (imageMargin.y * 2)))
        back.backgroundColor = self.placeholderBackgroundColor
        container.addSubview(back)
        let left = self.addLabelWithTag(tag: labelsTag, font: UIFont.defaultBold, textColor: self.titleTextColor, container: container)
        left.frame = CGRect(origin: textMargin, size: CGSize(width: getTextContainerWidth(container: container), height: container.frame.size.height - (textMargin.y * 2)))
        left.text = self.title
        let right = self.addLabelWithTag(tag: labelsTag + 1, font: UIFont.defaultBold.withSize(10.0), textColor: self.descTextColor, container: container)
        right.frame = CGRect(origin: textMargin, size: CGSize(width: getTextContainerWidth(container: container), height: container.frame.size.height - (textMargin.y * 2)))
        right.text = self.desc
        right.textAlignment = .right
    }

    func setupFullImageStyle(container:UIView) {
        container.frame = CGRect(origin: container.frame.origin, size: self.cellSize)
        if self.backgroundColor != nil {
            container.backgroundColor = self.backgroundColor
        }
        var imageView:UIImageView!
        if self.images.count != 0 && !isImageHidden {
            imageView = self.addImageViewD(container: container)
        } else {
            if let imageView = container.viewWithTag(imagesTag) as? UIImageView {
                imageView.image = nil
            }
        }
        setupIcons(container: container)
        let section:UILabel = self.addSectionLabel(origin: CGPoint(x: textMargin.x, y: (container.frame.size.height * (1 - textPercent)) + textMargin.y + textOffset), container: container)
        let title = self.addTitleLabel(container: container, under: section.frame, indent: textMargin.y)
        if !isDescriptionHidden {
            let desc = self.addLabelWithTag(tag: labelsTag + 2, font: UIFont.defaultRegular, textColor: descTextColor, container: container, multiline: true)
            desc.text = self.desc
            let descY = title.frame.origin.y + title.frame.size.height + textMargin.y
            desc.frame = CGRect(x: textMargin.x, y: descY, width: title.frame.size.width, height: container.frame.height - descY)
            desc.sizeToFit()
        }
        self.addSeparator(container: container)
    }

    func setupCell(container:UIView) {
        let section:UILabel = self.addSectionLabel(origin: marginA, container: container)
        _ = self.addTitleLabel(container: container, under: section.frame, indent: 3 * BasicCellData.sizeFactor)
        self.addSeparator(container: container)
    }

    func setupCustomStyle(container:UIView) {
        container.frame = CGRect(origin: container.frame.origin, size: self.cellSize)
        if self.backgroundColor != nil {
            container.backgroundColor = self.backgroundColor
        }
        let imageView:UIImageView = self.addImageViewD(container: container)
        imageView.image = UIImage(named: "banner-beneficios.jpg")
        imageView.layer.cornerRadius = 5.0
        imageView.clipsToBounds = true

        let titleA = self.addLabelWithTag(tag: labelsTag, font: titleFont, textColor: .white, container: container)
        titleA.text = "Conoce tus"
        titleA.frame = CGRect(origin: CGPoint(x: 25, y: 25), size: CGSize(width: 100, height: 20))
        titleA.fitFontForSize(constrainedSize: titleA.frame.size, maxFontSize: 40.0, minFontSize: 12.0)

        let titleB = self.addLabelWithTag(tag: labelsTag + 1, font: Theme.shared.fonts.cellTitleWithBackground, textColor: .white, container: container)
        titleB.text = "Beneficios"
        titleB.frame = CGRect(origin: CGPoint(x: 25, y: 50), size: CGSize(width: 200, height: 45))
        titleB.fitFontForSize(constrainedSize: titleB.frame.size, maxFontSize: 40.0, minFontSize: 12.0)

        let titleC = self.addLabelWithTag(tag: labelsTag + 2, font: Theme.shared.fonts.regularFont, textColor: .white, container: container, multiline: true)
        titleC.text = "Todos los beneficios de tu membresía explicados aquí"
        titleC.frame = CGRect(origin: CGPoint(x: 25, y: container.frame.height - 50), size: CGSize(width: 190, height: 28))
        titleC.fitFontForSize(constrainedSize: titleC.frame.size, maxFontSize: 40.0, minFontSize: 8.0)

        let titleD = self.addLabelWithTag(tag: labelsTag + 3, font: Theme.shared.fonts.boldFont, textColor: Theme.shared.colors.textBlue, container: container)
        titleD.text = "SABER MÁS"
        titleD.layer.backgroundColor = UIColor.white.cgColor
        titleD.layer.cornerRadius = 15.0
        titleD.textAlignment = .center
        titleD.frame = CGRect(origin: CGPoint(x: container.frame.size.width - 120, y: container.frame.height - 50), size: CGSize(width: 100, height: 30))

        self.addSeparator(container: container)
    }

    func setupAccountCustomStyle(container:UIView) {
        container.frame = CGRect(origin: container.frame.origin, size: self.cellSize)
        if self.backgroundColor != nil {
            container.backgroundColor = self.backgroundColor
        }
        let imageView:UIImageView = self.addImageViewD(container: container)
        imageView.image = UIImage(named: "card.jpg")
        imageView.layer.cornerRadius = 5.0
        imageView.clipsToBounds = true

        let titleA = self.addLabelWithTag(tag: labelsTag, font: Theme.shared.fonts.regularFont, textColor: .black, container: container)
        titleA.text = "Afiliación " + (Theme.shared.user.affiliateNumber as! String)
        titleA.frame = CGRect(origin: CGPoint(x: 25, y: 100), size: CGSize(width: 200, height: 20))

        let titleB = self.addLabelWithTag(tag: labelsTag + 1, font: Theme.shared.fonts.regularFont, textColor: .black, container: container)
        titleB.text = "Vence      " + (Theme.shared.user.expiresAt)
        titleB.frame = CGRect(origin: CGPoint(x: 25, y: 120), size: CGSize(width: container.frame.width - 50, height: 20))

        let titleC = self.addLabelWithTag(tag: labelsTag + 2, font: Theme.shared.fonts.boldFont.withSize(18.0), textColor: .black, container: container)
        titleC.text = Theme.shared.user.name
        titleC.frame = CGRect(origin: CGPoint(x: 25, y: imageView.frame.origin.y + imageView.frame.height - 30), size: CGSize(width: container.frame.width - 50, height: 30))
        titleC.fitFontForSize(constrainedSize: titleC.frame.size)
    }

    func setupAppointmentCustomStyle(container:UIView) {
        container.frame = CGRect(origin: container.frame.origin, size: self.cellSize)
        if self.backgroundColor != nil {
            container.backgroundColor = self.backgroundColor
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let vIndent: CGFloat = 20
        let startAt = self.addLabelWithTag(tag: labelsTag, font: Theme.shared.fonts.regularFont, textColor: .black, container: container)
        startAt.text = formatter.string(from: self.appointment.startD)
        startAt.frame = CGRect(origin: CGPoint(x: 5, y: vIndent), size: CGSize(width: 40, height: (container.frame.height * 0.5) - vIndent))
        
        let endAt = self.addLabelWithTag(tag: labelsTag + 1, font: Theme.shared.fonts.regularFont, textColor: .black, container: container)
        endAt.text = formatter.string(from: self.appointment.endD)
        endAt.frame = CGRect(origin: CGPoint(x: 5, y: container.frame.height * 0.5), size: CGSize(width: 40, height: (container.frame.height * 0.5) - vIndent))

        let titleC = self.addLabelWithTag(tag: labelsTag + 2, font: Theme.shared.fonts.regularFont.withSize(16.0), textColor: .black, container: container)
        titleC.numberOfLines = 0
        titleC.lineBreakMode = .byWordWrapping
        titleC.text = self.appointment.providerName
        titleC.frame = CGRect(origin: CGPoint(x: 50, y: 0), size: CGSize(width: container.frame.width - (self.appointment.older ? 120 : 60), height: container.frame.height))
        titleC.fitFontForSize(constrainedSize: titleC.frame.size, maxFontSize: 18.0, onlyWidth: true)

        let button = self.addLabelWithTag(tag: labelsTag + 3, font: Theme.shared.fonts.regularFont.withSize(16.0), textColor: Theme.shared.colors.textBrightBlue, container: container)
        button.text = self.appointment.older ? "Califica" : ""
        button.textAlignment = .right
        button.frame = CGRect(origin: CGPoint(x: container.frame.width - 100, y: 0), size: CGSize(width: 90, height: container.frame.height))
        if self.appointment.isRated {
            let imageAttachment =  NSTextAttachment()
            imageAttachment.image = UIImage(named:"bg")
            let imageOffsetY:CGFloat = 0.0;
            imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
            let attachmentString = NSAttributedString(attachment: imageAttachment)
            let completeText = NSMutableAttributedString(string: "")
            completeText.append(attachmentString)
            let  textAfterIcon = NSMutableAttributedString(string: " \(self.appointment.starred)")
            completeText.append(textAfterIcon)
            button.attributedText = completeText
            button.textColor = .white
            button.backgroundColor = Theme.shared.colors.navigationBarColor1
            let buttonH: CGFloat = 30
            button.textAlignment = .center
            button.frame = CGRect(origin: CGPoint(x: container.frame.width - 60, y: (container.frame.height - buttonH) * 0.5), size: CGSize(width: 45, height: buttonH))
            button.layer.cornerRadius = 15
            button.layer.masksToBounds = true
        }

        let view = UIView(frame: CGRect(x: 42, y: 5, width: 2, height: container.frame.height - 10))
        view.backgroundColor = self.appointment.older ? .red : Theme.shared.colors.blueButton
        container.addSubview(view)

        addSeparator(container: container)
    }

    func setupChatCustomStyle(container:UIView) {
        container.frame = CGRect(origin: container.frame.origin, size: self.cellSize)
        if self.backgroundColor != nil {
            container.backgroundColor = self.backgroundColor
        }

        let title = self.addLabelWithTag(tag: labelsTag, font: Theme.shared.fonts.regularFont.withSize(Theme.shared.fonts.chatFontSize), textColor: .black, container: container)
        title.numberOfLines = 0
        title.lineBreakMode = .byWordWrapping
        title.text = self.title
        title.frame = CGRect(origin: CGPoint(), size: CGSize(width: self.cellSize.width * 0.65, height: self.cellSize.height - 10))
        title.sizeToFit()
        let date = self.addLabelWithTag(tag: labelsTag + 1, font: Theme.shared.fonts.regularFont.withSize(12), textColor: .darkGray, container: container)
        date.text = self.desc
        date.sizeToFit()
        container.viewWithTag(321321)?.removeFromSuperview()
        let bWidth = max(title.frame.width, date.frame.width)
        let background = UIView(frame: CGRect(x: isClient ? self.cellSize.width - bWidth - 25 : 15, y: 5, width: bWidth + 10, height: title.frame.height + date.frame.height + 15))
        background.tag = 321321
        background.backgroundColor = self.descTextColor
        background.layer.cornerRadius = 10
        background.layer.masksToBounds = true
        background.dropShadow()
        title.frame = CGRect(origin: CGPoint(x: background.frame.minX + 5, y: 10), size: title.frame.size)
        date.frame = CGRect(origin: CGPoint(x: isClient ? title.frame.minX : background.frame.origin.x + background.frame.width - date.frame.width - 5, y: title.frame.maxY + 5), size: date.frame.size)
        container.addSubview(background)
        container.addSubview(title)
        container.addSubview(date)
        cellSize.height = background.frame.height + 10
    }

    func addSectionLabel(origin:CGPoint, container:UIView) -> UILabel {
        let section = self.addLabelWithTag(tag: labelsTag, font: Theme.shared.fonts.cellSectionFont, textColor: Theme.shared.colors.cellSectionColor, container: container)
        section.frame = CGRect(origin: origin, size: CGSize(width: getTextContainerWidth(container: container), height:100))
        section.text = self.sectionTitle.uppercased()
        if self.specialTextColor != nil {
            section.textColor = self.specialTextColor
        }
        section.sizeToFit()
        section.frame = CGRect(x: section.frame.origin.x, y: section.frame.origin.y, width: getTextContainerWidth(container: container), height: section.frame.size.height)
        if section.frame.height == 0 {
            section.frame = CGRect(x: section.frame.origin.x, y: section.frame.origin.y - textMargin.y, width: section.frame.size.width, height: section.frame.size.height)
        }
        return section
    }

    func getTextContainerWidth(container:UIView) -> CGFloat {
        if contentStyle != nil && contentStyle == .horizontal {
            return (container.frame.size.width * (1.0 - imagePercent)) - (textMargin.x * 2)
        }
        return container.frame.size.width - (textMargin.x * 2)
    }

    func getTitleContainerHeight(container:UIView) -> CGFloat {
        if contentStyle != nil && contentStyle == .horizontal {
            return container.frame.size.height - (textMargin.y * 2)
        } else if contentStyle != nil && contentStyle == .fullImage {
            return ((container.frame.size.height * textPercent) * titleHeightPercent) - (textMargin.y * 2)
        }
        return ((container.frame.size.height - (container.frame.size.height * imagePercent)) * titleHeightPercent) - (textMargin.y * 2)
    }

    func addLabelWithTag(tag:Int, font:UIFont, textColor:UIColor, container:UIView, multiline:Bool = false) -> UILabel {
        container.viewWithTag(tag)?.removeFromSuperview()
        let label = UILabel.init()
        label.tag = tag
        label.font = font
        label.textColor = textColor
        label.textAlignment = self.textAlignment
        if multiline {
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping
        }
        if textDropShadow {
            label.layer.shadowColor = UIColor.black.cgColor
            label.layer.shadowRadius = 3.0
            label.layer.shadowOpacity = 1.0
            label.layer.shadowOffset = CGSize(width: 1, height: 1)
            label.layer.masksToBounds = false
        }
        container.addSubview(label)
        return label
    }

    func addTitleLabel(container:UIView, bigFontSize:Bool = true, under:CGRect? = nil, indent:CGFloat = 0, center:Bool = true) -> UILabel {
        let title = self.addLabelWithTag(tag: labelsTag + 1, font: titleFont, textColor: titleTextColor, container: container, multiline:true)
        if under != nil {
            title.text = self.title
            let finalFrame = CGRect(origin: CGPoint(x: (under?.origin.x)!, y: (under?.origin.y)! + (under?.size.height)! + indent), size: CGSize(width: getTextContainerWidth(container: container), height: getTitleContainerHeight(container: container)))
            title.frame = finalFrame
            if adjustFontSize {
                title.fitFontForSize(constrainedSize: title.frame.size, maxFontSize: 40.0, minFontSize: 11.0)
                if center {
                    title.frame = finalFrame
                }
            }
        }
        return title
    }

    func setupBasicExtraCell(container:UIView) {
        let section:UILabel = self.addSectionLabel(origin: marginB, container: container)
        let title = self.addTitleLabel(container: container, under: section.frame, indent: 3 * BasicCellData.sizeFactor, center: false)
        let author = self.addLabelWithTag(tag: self.labelsTag + 2, font: UIFont.detailDesc, textColor: UIColor.red, container: container)
//        author.text = self.author
        author.frame = CGRect(x: title.frame.origin.x, y: title.frame.origin.y + title.frame.size.height - 2, width: title.frame.size.width, height: 20)
        self.addSeparator(container: container)
    }

    func addSeparator(container: UIView) {
        container.viewWithTag(separatorTag)?.removeFromSuperview()
        if isSeparatorHidden {
            return
        }
        let separator = UIView.init(frame: CGRect(x:separatorMargin.x, y:container.frame.size.height - 1, width:container.frame.size.width - (separatorMargin.x * 2), height:1))
        separator.tag = separatorTag
        separator.backgroundColor = Theme.shared.colors.cellSeparatorColor
        container.addSubview(separator)
    }

    func setupHighlightedCell(container:UIView) {
        container.frame = CGRect(origin: container.frame.origin, size: self.cellSize)
        var titleRect = CGPoint()
        var imageView:UIImageView!
        if self.images.count != 0 {
            imageView = self.addImageView(container: container, under:CGRect(), heightPercent: 0.67)
            titleRect = CGPoint(x: marginA.x, y: imageView.frame.origin.y + imageView.frame.size.height + 5)
        }
        let section:UILabel = self.addSectionLabel(origin: titleRect, container: container)
        let title = self.addTitleLabel(container: container, under: section.frame, indent: marginA.y, center:false)
        if self.publishedAt != nil {
            let since = self.addLabelWithTag(tag: labelsTag + 2, font: UIFont.cellSince, textColor: UIColor.cellSince, container: container)
            since.text = self.getSinceFormattedString(from: self.publishedAt)
            since.frame = CGRect(x: marginA.x, y: title.frame.origin.y + title.frame.size.height - 5, width: title.frame.size.width, height: 30 * BasicCellSize.sizeFactor)
        }
        if self.video != nil && imageView != nil && imageView.viewWithTag(123456) == nil {
            let playImg = UIImage(named: "play-icon")
            let play = UIImageView(image: playImg)
            let iconSize = CGSize(width: (playImg?.size.width)! * 1.5, height: (playImg?.size.height)! * 1.5)
            play.frame = CGRect(origin: CGPoint(x:(imageView.frame.size.width - iconSize.width) * 0.5, y:(imageView.frame.size.height - iconSize.height)  * 0.5), size: iconSize)
            play.tag = 123456
            imageView.addSubview(play)
        }
    }

    func setupSeparatorCell(container:UIView) {
        let title = self.addLabelWithTag(tag: labelsTag, font: UIFont.cellSeparator, textColor: self.specialTextColor != nil ? self.specialTextColor : UIColor.separatorFont, container: container)
        title.frame = CGRect(x: marginA.x, y: 10, width: container.frame.size.width - (marginA.x * 2), height: container.frame.size.height)
        title.text = self.title
        let summary = self.addLabelWithTag(tag: labelsTag + 1, font: UIFont.homeHeader, textColor: UIColor.separatorFont, container: container, multiline: true)
        summary.frame = title.frame
        summary.textAlignment = .right
        summary.text = self.desc
        addSeparator(container: container)
    }

    private func setupSeparatorSectionCell(container:UIView) {
        let title = self.addLabelWithTag(tag: labelsTag, font: self.titleFont, textColor: self.titleTextColor, container: container)
        title.frame = CGRect(x: textMargin.x, y: textOffset + textMargin.y, width: container.frame.size.width - (textMargin.x * 2), height: container.frame.size.height - (textMargin.y * 2))
        title.text = self.title
        let summary = self.addLabelWithTag(tag: labelsTag + 1, font: self.descFont, textColor: self.descTextColor, container: container)
        summary.frame = CGRect(x: container.frame.size.width - (200 + textMargin.x), y: textOffset + textMargin.y, width: 200 - textROffset, height: title.frame.size.height - (textMargin.y * 2))
        summary.textAlignment = .right
        summary.text = self.desc
        setupIcons(container: container)
        addSeparator(container: container)
    }

    private func setupDoubleColumnCell(container:UIView) {
        let title = self.addLabelWithTag(tag: labelsTag, font: self.titleFont, textColor: self.titleTextColor, container: container)
        title.frame = CGRect(x: textMargin.x, y: 0, width: container.frame.size.width * 0.5, height: container.frame.size.height)
        title.text = self.title
        let t1 = self.addLabelWithTag(tag: labelsTag + 1, font: self.descFont, textColor: self.descTextColor, container: container, multiline: true)
        let t2 = self.addLabelWithTag(tag: labelsTag + 2, font: self.descFont, textColor: self.descTextColor, container: container, multiline: true)
        t1.text = self.desc
        t2.text = self.extra
        t1.textAlignment = .center
        t2.textAlignment = .center
        t1.sizeToFit()
        t2.sizeToFit()
        t1.center = CGPoint(x: container.frame.size.width * 0.675, y: container.frame.size.height * 0.5)
        t2.center = CGPoint(x: container.frame.size.width * 0.875, y: container.frame.size.height * 0.5)
        addSeparator(container: container)
    }

    private func addImageViewD(container:UIView, widthPercent:CGFloat = 1.0, heightPercent:CGFloat = 1.0) -> UIImageView {
        var imageX = imageMargin.x
        let imageY = imageMargin.y
        let imageW = (container.frame.size.width * widthPercent) - (imageMargin.x * 2)
        let imageH = (container.frame.size.height * heightPercent) - (imageMargin.y * 2)
        if contentStyle != nil {
            switch contentStyle {
            case .vertical:         break
            case .horizontal:       imageX = self.imageFirst ? imageX : container.frame.size.width - container.frame.size.width * widthPercent
            case .none:             break
            case .some(_):          print("configViewCell:  some")
            }
        }
        return self.addImageViewWith(frame: CGRect(x: imageX, y: imageY, width: imageW, height: imageH), tag:imagesTag, container: container)
    }

    private func addImageView(container:UIView, under:CGRect, widthPercent:CGFloat = 1.0, heightPercent:CGFloat = 1.0) -> UIImageView {
        return self.addImageViewWith(frame: CGRect(x: imageMargin.x, y: under.origin.y + under.size.height + imageMargin.y, width: (container.frame.size.width * widthPercent) - (imageMargin.x * 2), height: container.frame.size.height * heightPercent), tag:imagesTag, container: container)
    }

    private func addImageViewWith(frame:CGRect, tag:Int, container:UIView) -> UIImageView {
        container.viewWithTag(tag)?.removeFromSuperview()
        let imageView = UIImageView.init()
        imageView.tag = tag
        imageView.backgroundColor = placeholderBackgroundColor
        imageView.frame = frame
        container.addSubview(imageView)
        return imageView
    }

    private func updateCellSize(container:UIView, final:CGRect) {
        if cellAutoResize {
            cellAutoResize = false
            cellSize = CGSize(width: cellSize.width, height: final.origin.y + final.size.height + (marginB.y * 1.5))
            container.frame = CGRect(origin: container.frame.origin, size: cellSize)
        }
    }

    private func setupFullImageCell(container:UIView) {
        container.frame = CGRect(origin: container.frame.origin, size: self.cellSize)
        let imageHeight = container.frame.height - (marginA.y * 2)
        let imageView = self.addImageViewWith(frame: CGRect(x:marginA.x, y: marginB.y * 0.6, width: container.frame.size.width - (marginA.x * 2), height: imageHeight), tag:self.imagesTag, container: container)
        let title = self.addLabelWithTag(tag: labelsTag, font: UIFont.homeHeader, textColor: .white, container: container, multiline: true)
        let titleH = container.frame.size.height * 0.33
        let size = CGSize(width:imageView.frame.size.width - (marginA.x), height:titleH)
        title.text = self.title
        title.textAlignment = .center
        title.fitFontForSize(constrainedSize: size, maxFontSize: 40, minFontSize: 12)
        title.sizeToFit()
        title.frame = CGRect(x: marginA.x * 1.5, y: imageView.frame.origin.y + imageView.frame.size.height - (title.frame.size.height + 5), width:imageView.frame.size.width - (marginA.x), height:title.frame.size.height)
        title.layer.shadowOffset = CGSize(width: 0, height: 0)
        title.layer.shadowOpacity = 1
        title.layer.shadowRadius = 6
        if self.video != nil && imageView.viewWithTag(123456) == nil {
            let gradientImg = UIImage(named: "gradient")
            let gradient = UIImageView(image: gradientImg)
            gradient.layer.cornerRadius = imageView.layer.cornerRadius + 2
            gradient.clipsToBounds = true
            gradient.frame = CGRect(origin: CGPoint(x: -18, y: -17), size: CGSize(width: imageView.frame.size.width * 1.18, height: imageView.frame.size.height * 1.25))
            gradient.tag = 123456
            imageView.addSubview(gradient)
            let playImg = UIImage(named: "play-icon")
            let play = UIImageView(image: playImg)
            play.frame = CGRect(origin: CGPoint(x:(imageView.frame.size.width - (playImg?.size.width)!) * 0.5, y:title.frame.origin.y - (playImg?.size.height)! - 20), size: (playImg?.size)!)
            play.tag = 123456
            imageView.addSubview(play)
        }
    }

    private func setupCollectionCell(container:UIView) {
        let collectionView:UICollectionView = container.viewWithTag(10) as! UICollectionView
        collectionView.frame = CGRect(x: 0, y: 0, width: container.frame.width, height:  self.cellSize.height)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.itemSize = self.hCollection[0].cellSize
        layout.scrollDirection = .horizontal
        
        collectionView.collectionViewLayout = layout
        addSeparator(container: container)
    }

    private func getSinceFormattedString(from date:Date) -> String {
        if Bundle.main.infoDictionary?["TargetName"] as! String == "teleSUR" {
            let currentDate = Date()
            let daysBefore = currentDate.days(from: date)
            if daysBefore == 0 {
                let hoursBefore = currentDate.hours(from: date)
                if hoursBefore == 0 {
                    return "Hace unos minutos"
                }
                return "Hace " + String(hoursBefore) + " h"
            } else if daysBefore > 30 {
                let monthsTo = floor(Double(daysBefore / 30))
                return "Hace " + String(monthsTo) + " mes" + (monthsTo != 1 ? "es" : "")
            }
            return "Hace " + String(daysBefore) + " día" + (daysBefore != 1 ? "s" : "")
        } else {
            let formatter = DateFormatter()
            formatter.dateStyle = DateFormatter.Style.long
            formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
            return "Published " + formatter.string(from: date)
        }
    }

}
