//
//  RateAppointmentViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 19/02/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import StarryStars
import RoundedSwitch

class RateAppointmentViewController: DasBasicViewController {

    let formatter = DateFormatter()

    let stars = RatingView()
    let switchA = Switch()
    let switchB = Switch()

    var startAtA: UILabel!
    var endAtA: UILabel!
    var alertA: UILabel!
    var placeAt: UILabel!
    var selected: UILabel!
    var activityIndicator: NVActivityIndicatorView!
    var item: BasicCellData!
    let notificationOptions = ["Elige una opción", "Página de internet", "Call Center DAS", "App iOS/Android", "Recomendación"]
    var notification: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        formatter.dateFormat = "dd/MM/yyyy HH:mm"
        var colors = [UIColor]()
        colors.append(Theme.shared.colors.navigationBarColor1)
        colors.append(Theme.shared.colors.navigationBarColor2)
        colors.append(Theme.shared.colors.navigationBarColor3)
        navigationController?.navigationBar.setGradientBackground(colors: colors)

        let rowH:CGFloat = 60.0
        let rowPercent:CGFloat = 0.45
        self.navigationController?.navigationBar.tintColor = Theme.shared.colors.blueButton
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Calificar", style: .plain, target: self, action: #selector(addButtonAction))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(cancelButtonAction))
        //        self.navigationItem.rightBarButtonItem?.isEnabled = false

        let backTop = UIView()
        backTop.backgroundColor = Theme.shared.colors.graySeparator.withAlphaComponent(0.5)
        view.addSubview(backTop)
        let serviceName = self.addLabelWithTag(font: Theme.shared.fonts.boldFont.withSize(16.0), textColor: .black, container: view)
        serviceName.frame = CGRect(x: 15, y: (self.navigationController?.navigationBar.frame.height)! + 25, width: self.view.frame.size.width - 30, height: 50)
        serviceName.numberOfLines = 0
        serviceName.lineBreakMode = .byWordWrapping
        serviceName.text = item.appointment.providerName

        let appointmentDate = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: Theme.shared.colors.textGray, container: view)
        appointmentDate.frame = CGRect(x: 15, y: serviceName.frame.origin.y + serviceName.frame.height, width: self.view.frame.size.width - 30, height: 20)
        let dFormat = DateFormatter()
        dFormat.dateStyle = .long
        dFormat.timeStyle = .short
        dFormat.locale = Locale(identifier: "es_MX")
        appointmentDate.text = dFormat.string(from: item.appointment.startD)

        backTop.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: appointmentDate.frame.origin.y + appointmentDate.frame.height + 5)
        addSeparator(margin: CGPoint(x: 0.0, y: appointmentDate.frame.origin.y + appointmentDate.frame.height + 5), at: view)

        let startAt = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        startAt.frame = CGRect(x: 15.0, y: appointmentDate.frame.origin.y + appointmentDate.frame.height + 5, width: view.frame.size.width * rowPercent, height: rowH)
        startAt.lineBreakMode = .byWordWrapping
        startAt.numberOfLines = 0
        startAt.text = "Satisfacción del servicio"

        stars.frame = CGRect(x: view.frame.width - 140, y: startAt.frame.origin.y + 15, width: 130, height: 30)
        stars.rating = item.appointment.starred.floatValue 
        view.addSubview(stars)
        addSeparator(margin: CGPoint(x: 0.0, y: startAt.frame.origin.y + startAt.frame.height), at: view)

        let endAt = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        endAt.frame = CGRect(x: 15.0, y: startAt.frame.origin.y + startAt.frame.height, width: view.frame.size.width * rowPercent, height: rowH)
        endAt.text = "¿Lo volvería a utilizar?"

        switchA.leftText = "No"
        switchA.rightText = "Si"
        switchA.rightSelected = item.appointment.useItAgain.intValue == 1
        switchA.tintColor = .black
        switchA.disabledColor = switchA.tintColor.withAlphaComponent(0.4)
        switchA.backColor = UIColor.lightGray.withAlphaComponent(0.4)
        switchA.frame = CGRect(x: view.frame.width - 110, y: endAt.frame.origin.y + 15, width: 80, height: 30)
        view.addSubview(switchA)
/*
        let switchA = UISwitch(frame: CGRect(x: view.frame.width - 80, y: endAt.frame.origin.y + 10, width: 100, height: 20))
        switchA.
        view.addSubview(switchA)
*/
        addSeparator(margin: CGPoint(x: 0.0, y: endAt.frame.origin.y + endAt.frame.height), at: view)

        let alert = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        alert.frame = CGRect(x: 15.0, y: endAt.frame.origin.y + endAt.frame.height, width: view.frame.size.width * rowPercent, height: rowH)
        alert.text = "¿Por qué medio se enteró de este proveedor?"
        alert.numberOfLines = 0
        alert.lineBreakMode = .byWordWrapping
        alertA = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: Theme.shared.colors.textGray, container: view)
        alertA.frame = CGRect(x: 45.0, y: alert.frame.origin.y, width: view.frame.size.width - 65, height: rowH)
        alertA.textAlignment = .right
        print(item.appointment.whereuFindOut.intValue)
        notification = item.appointment.whereuFindOut.intValue
        alertA.text = notificationOptions[notification]

        let alertButton = UIButton(type: .custom)
        alertButton.frame = CGRect(x: 15, y: alert.frame.origin.y, width: self.view.frame.size.width - 30, height: rowH)
        alertButton.clipsToBounds = true
        alertButton.addTarget(self, action: #selector(alertButtonAction), for: .touchUpInside)
        view.addSubview(alertButton)

        addSeparator(margin: CGPoint(x: 0.0, y: alert.frame.origin.y + alert.frame.height), at: view)

        let recommend = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        recommend.frame = CGRect(x: 15.0, y: alert.frame.origin.y + alert.frame.height + 10, width: view.frame.size.width * rowPercent, height: rowH)
        recommend.numberOfLines = 0
        recommend.lineBreakMode = .byWordWrapping
        recommend.text = "¿Recomendaría nuestra membresía?"

        switchB.leftText = "No"
        switchB.rightText = "Si"
        switchB.rightSelected = !(item.appointment.recommend != nil && item.appointment.recommend == 0)
        switchB.tintColor = .black
        switchB.disabledColor = switchB.tintColor.withAlphaComponent(0.4)
        switchB.backColor = UIColor.lightGray.withAlphaComponent(0.4)
        switchB.frame = CGRect(x: view.frame.width - 110, y: recommend.frame.origin.y + 10, width: 80, height: 30)
        view.addSubview(switchB)
/*
        let switchB = UISwitch(frame: CGRect(x: view.frame.width - 80, y: recommend.frame.origin.y + 10, width: 100, height: 20))
        view.addSubview(switchB)
*/
        addSeparator(margin: CGPoint(x: 0.0, y: recommend.frame.origin.y + recommend.frame.height), at: view)
/*
        let comments = self.addLabelWithTag(font: Theme.shared.fonts.regularFont.withSize(14.0), textColor: .black, container: view)
        comments.frame = CGRect(x: 15.0, y: recommend.frame.origin.y + recommend.frame.height, width: view.frame.size.width - 30, height: 50)
        comments.text = "Comentarios"

        let commentTxf = UITextView(frame: CGRect(x: 15, y: comments.frame.origin.y + comments.frame, width: <#T##CGFloat#>, height: <#T##CGFloat#>))
*/
//        addSeparator(margin: CGPoint(x: 0.0, y: comments.frame.origin.y + comments.frame.height), at: view)

        setupActivityIndicator()

    }
    
    /*
     "idAfiliacion": 1,
     "id_proveedor": 1,
     "hora_inicio": "01\/01\/2018 10:00",
     "hora_final": "01\/01\/2018 20:00",
     */

    func addAppointment() {
        let formParams = ["idAfiliacion": Theme.shared.user.affiliateNumber!, "id_proveedor": item.appointment.providerId, "hora_inicio": item.appointment.startTime, "hora_final": item.appointment.endTime, "calificacion": Int(stars.rating), "autoevaluacion" : switchA.rightSelected ? "1" : "0", "recomendarias": switchB.rightSelected ? "1" : "0", "comentarios": "", "comoSeEntero": [notification]] as [String : Any]
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        
        let uString = "\(Urls.base)actualizarCita?id=\(item.appointment.id!)&idAfiliacion=\(Theme.shared.user.affiliateNumber!)"
        let url =  NSURL(string:uString)
        var request = URLRequest(url: url! as URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let data = try! JSONSerialization.data(withJSONObject: formParams, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        Alamofire.request(request).responseJSON { response in
            if response.result.value != nil {
                print(response.result.value)
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }

    @objc func addButtonAction() {
        addAppointment()
    }
    
    @objc func cancelButtonAction() {
        dismiss(animated: true, completion: nil)
    }

    @objc func alertButtonAction() {
        let actionSheetController: UIAlertController = UIAlertController(title: "¿Por qué medio se enteró de este proveedor?", message: "", preferredStyle: .actionSheet)
        for (index, option) in notificationOptions.enumerated() {
            let actionButton = UIAlertAction(title: option, style: .default) { _ in
                self.alertA.text = option
                self.notification = index
            }
            actionSheetController.addAction(actionButton)
        }
        present(actionSheetController, animated: true, completion: nil)
    }

    func setupActivityIndicator() {
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 10, y: (self.navigationController?.navigationBar.frame.height)! + 30, width: 60, height: 60), type: NVActivityIndicatorType.ballClipRotate, color: Theme.shared.colors.navigationBarColor3, padding: 0)
        activityIndicator.center = view.center
        activityIndicator.isHidden = true
        self.view.addSubview(activityIndicator)
    }

}
