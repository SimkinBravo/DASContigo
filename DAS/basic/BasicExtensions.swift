//
//  Extensions.swift
//  PrototypeOM
//
//  Created by Patricio Bravo Cisneros on 13/04/17.
//  Copyright © 2017 OpenMultimedia. All rights reserved.
//

import UIKit

extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    var iPhone5: Bool {
        return UIDevice.current.screenType == .iPhones_5_5s_5c_SE
    }
    enum ScreenType: String {
        case iPhone4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneX = "iPhone X"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX
        default:
            return .unknown
        }
    }
}

extension UIViewController {

    func printAvailableFonts() {
        for family: String in UIFont.familyNames {
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family) {
                print("== \(names)")
            }
        }
    }

    func getAdCellData() -> BasicCellData {
        let adData = BasicCellData(slug: "banner", title: "Publicidad")
        adData.adId = "/18910235/app_top_banner"
        //adData.adId = "ca-app-pub-3940256099942544/2934735716"
        adData.cellReuseID = BasicCellID.empty
        adData.cellSize = BasicCellSize.ad
        return adData
    }

}

extension UIView {

    func dropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = CGSize(width: -2, height: 1)
        self.layer.shadowRadius = 2
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }

}
extension UILabel {

    public func configHeader() {
        self.font = UIFont.header
        self.textColor = .white
    }

}

extension UIFont {

    public class var header: UIFont         {   return UIFont(name: UIFont.robotoBlack, size: 20.0)!             }

    public class var homeHeader: UIFont     {   return systemFont(ofSize: 12.0, weight: .heavy)            }
    public class var videoDesc: UIFont      {   return cellSince.withSize(15.0)                                  }
    public class var homeHeaderSub: UIFont  {   return UIFont(name: UIFont.displayLight, size: 14.0)!            }
    public class var cellSince: UIFont      {   return systemFont(ofSize: 16.0, weight: .medium)           }

    public class var cellSeparator: UIFont  {   return UIFont.homeHeader.withSize(24.0)                          }
    public class var subSection: UIFont     {   return UIFont.homeHeader.withSize(20.0)                          }
    public class var cellSection: UIFont    {   return UIFont.homeHeader.withSize(12.0)                          }

    public class var detailHTML: UIFont     {   return systemFont(ofSize: 14.0, weight: .medium)                 }
    public class var detailDesc: UIFont     {   return systemFont(ofSize: 15.0, weight: .medium)                 }

    public class var defaultRegular: UIFont      {   return systemFont(ofSize: 12.0)          }
    public class var defaultBold: UIFont         {   return boldSystemFont(ofSize: 12.0)             }
    public class var defaultHeavy: UIFont         {   return systemFont(ofSize: 12.0, weight: .heavy)             }

    internal class var defaultRegularName: String  {    return "OpenSans"                  }
    internal class var defaultBoldName: String     {    return "OpenSans-Bold"             }
    internal class var defaultHeavyName: String        {    return "OpenSans-ExtraBold"        }

    public class var robotoBlack: String    {           return "OpenSans-Bold"           }

    public class var display: String        {           return "OpenSans"     }
    public class var displayHeavy: String   {           return "OpenSans-ExtraBold"     }
    public class var displayLight: String   {           return "OpenSans-Regular"     }
    public class var displayMedium: String   {          return "OpenSans-Semibold"     }

}

extension UIColor {

    public class var headerBackground: UIColor {        return UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1.0)         }
    public class var tabBarTint: UIColor {              return UIColor(red: 255/255, green: 226/255, blue: 0/255, alpha: 1.0)            }
    public class var headerFont: UIColor {              return UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)            }
    public class var separatorFont: UIColor {           return UIColor(red: 221/255, green: 23/255, blue: 19/255, alpha: 1.0)           }
    public class var cellSince: UIColor {               return UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0)         }
    //rgb 25 31 67
    public class var sectionLA: UIColor {               return UIColor(red: 25/255, green: 31/255, blue: 67/255, alpha: 1.0)            }
    //rgb 250 60 48
    public class var sectionW: UIColor {                return UIColor(red: 250/255, green: 60/255, blue: 48/255, alpha: 1.0)           }
    //rgb 106 190 60
    public class var sectionS: UIColor {                return UIColor(red: 106/255, green: 190/255, blue: 60/255, alpha: 1.0)          }
    //rgb 0 127 185
    public class var sectionC: UIColor {                return UIColor(red: 0/255, green: 127/255, blue: 185/255, alpha: 1.0)           }
    //rgb 161 0 185
    public class var sectionT: UIColor {                return UIColor(red: 161/255, green: 0/255, blue: 185/255, alpha: 1.0)           }

    public class var videosHomeLabel: UIColor {         return UIColor(red: 144/255, green: 19/255, blue: 254/255, alpha: 1.0)          }
    //41 41 41
    public class var videoViewBackgroundColor: UIColor {return UIColor(red: 41/255, green: 41/255, blue: 41/255, alpha: 1.0)            }
    //210 210 210
    public class var videoViewText: UIColor        {    return UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1.0)         }

    public class var pageMenuItem: UIColor {          return UIColor(red: 195/255, green: 195/255, blue: 195/255, alpha: 1.0)           }
    public class var pageMenuSelectedItem: UIColor {  return UIColor(red: 0, green: 1/255, blue: 143/255, alpha: 1.0)                   }

    public class var highlightedCellTitleA: UIColor {   return UIColor.white                                                            }
    public class var highlightedCellAuthorA: UIColor {  return UIColor.white                                                            }
    public class var highlightedCellAuthorB: UIColor {  return UIColor.headerBackground                                                 }

    public class var doubleCellTypeA: UIColor {         return UIColor.headerBackground                                                 }
    public class var doubleCellTypeB: UIColor {         return UIColor.black                                                            }
    public class var doubleCellTitleA: UIColor {        return UIColor.white                                                            }
    public class var doubleCellAuthorAA: UIColor {      return UIColor.white                                                            }
    public class var doubleCellAuthorAB: UIColor {      return UIColor.headerBackground                                                 }
    public class var doubleCellAuthorBA: UIColor {      return UIColor.white                                                            }
    public class var doubleCellAuthorBB: UIColor {      return UIColor.black                                                            }
    public class var doubleCellDate: UIColor {          return UIColor.white                                                            }
    public class var doubleCellBackground: UIColor {    return UIColor.headerBackground                                                 }

    public class var cellTypeA: UIColor {               return UIColor.headerBackground                                                 }
    public class var cellTitleA: UIColor {              return UIColor.black                                                            }
    public class var cellAuthorA: UIColor {             return UIColor.black                                                            }
    public class var cellAuthorB: UIColor {             return UIColor.headerBackground                                                 }
    public class var cellDate: UIColor {                return UIColor.black                                                            }

    public class var galleryCellType: UIColor {              return UIColor.headerBackground                                                            }
    public class var galleryCellTitle: UIColor {              return UIColor.white                                                            }

}

extension String {

    func convertHtmlSymbols() throws -> String? {
        return try NSAttributedString(data: Data(utf8),
                                      options: [.documentType: NSAttributedString.DocumentType.html,
                                                .characterEncoding: String.Encoding.utf8.rawValue],
                                      documentAttributes: nil).string
    }

    /// Percent escapes values to be added to a URL query as specified in RFC 3986
    ///
    /// This percent-escapes all characters besides the alphanumeric character set and "-", ".", "_", and "~".
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: Returns percent-escaped string.
    
    func addingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }
    
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    var lines: [String] {
        var result: [String] = []
        enumerateLines { line, _ in result.append(line) }
        return result
    }

    func range(nsRange: NSRange) -> NSRange? {
        guard
            let from16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location, limitedBy: utf16.endIndex),
            let to16 = utf16.index(utf16.startIndex, offsetBy: nsRange.length, limitedBy: utf16.endIndex),
            let from = from16.samePosition(in: self),
            let to = to16.samePosition(in: self)
            else { return nil }
        return NSMakeRange(from.encodedOffset, to.encodedOffset)
    }

    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }

    func index(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.lowerBound
    }
    func endIndex(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.upperBound
    }
    func indexes(of string: String, options: CompareOptions = .literal) -> [Index] {
        var result: [Index] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range.lowerBound)
            start = range.upperBound
        }
        return result
    }
    func ranges(of string: String, options: CompareOptions = .literal) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range)
            start = range.upperBound
        }
        return result
    }

}

extension UILabel {
    
    func fitFontForSize(constrainedSize : CGSize, maxFontSize : CGFloat = 300.0, minFontSize : CGFloat = 5.0, accuracy : CGFloat = 1.0, onlyWidth:Bool = false) {
        let original = CGRect(origin: frame.origin, size: constrainedSize)
        var minFontSize = minFontSize
        var maxFontSize = maxFontSize
        assert(maxFontSize > minFontSize)
        while maxFontSize - minFontSize > accuracy {
            let midFontSize : CGFloat = ((minFontSize + maxFontSize) / 2)
            frame = original
            font = font.withSize(midFontSize)
            sizeToFit()
            let checkSize : CGSize = bounds.size
            if  checkSize.height < constrainedSize.height && checkSize.width < constrainedSize.width {
                minFontSize = midFontSize
            } else {
                maxFontSize = midFontSize
            }
        }
        font = font.withSize(minFontSize)
        if !onlyWidth {
            sizeToFit()
        } else {
            frame = original
        }
    }
    
}

extension BasicCellData {

    func getColorFrom(slug: String!) -> UIColor {
        if slug == nil {
            return UIColor.separatorFont
        }
        switch slug {
            case "latinoamerica":   return UIColor.sectionLA
            case "mundo":           return UIColor.sectionW
            case "deportes":        return UIColor.sectionS
            case "cultura":         return UIColor.sectionC
            case "tecnologia":         return UIColor.sectionC
            default:                return UIColor.separatorFont
        }
    }

    func getSectionSlugFrom(title: String) -> String {
        switch title {
            case "América Latina":          return "latinoamerica"
            case "Mundo":                   return "mundo"
            case "Deportes":                return "deportes"
            case "Cultura":                 return "cultura"
            case "Tecnología":              return "tecnologia"
            default:                        return ""
        }
    }

    func getSectionTitleFrom(slug: String) -> String {
        switch slug {
        case "latinoamerica":           return "América Latina"
        case "mundo":                   return "Mundo"
        case "deportes":                return "Deportes"
        case "cultura":                 return "Cultura"
        case "tecnologia":              return "Tecnología"
        default:                        return ""
        }
    }

}

extension Dictionary {
    
    /// Build string representation of HTTP parameter dictionary of keys and objects
    ///
    /// This percent escapes in compliance with RFC 3986
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped
    
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).addingPercentEncodingForURLQueryValue()!
            let percentEscapedValue = (value as! String).addingPercentEncodingForURLQueryValue()!
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joined(separator: "&")
    }
    
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}

class RFC912DateFormatter: DateFormatter {
    
    let dateFormats = [
        //2017-Jun-24  14:25:00
        "yyyy-MM-d HH:mm:ss"
    ]
    
    override init() {
        super.init()
        self.timeZone = TimeZone(secondsFromGMT: 0)
        self.locale = Locale(identifier: "en_US_POSIX")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) not supported")
    }
    
    override func date(from string: String) -> Date? {
        for dateFormat in self.dateFormats {
            self.dateFormat = dateFormat
            if let date = super.date(from: string) {
                return date
            }
        }
        return nil
    }
    
}

extension UIImage {
    
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }

    func addShadow(blurSize: CGFloat = 6.0) -> UIImage {
        
        let shadowColor = UIColor(white:0.0, alpha:0.8).cgColor
        
        let context = CGContext(data: nil,
                                width: Int(self.size.width + blurSize),
                                height: Int(self.size.height + blurSize),
                                bitsPerComponent: self.cgImage!.bitsPerComponent,
                                bytesPerRow: 0,
                                space: CGColorSpaceCreateDeviceRGB(),
                                bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        context.setShadow(offset: CGSize(width: blurSize/2,height: -blurSize/2),
                          blur: blurSize,
                          color: shadowColor)
        context.draw(self.cgImage!,
                     in: CGRect(x: 0, y: blurSize, width: self.size.width, height: self.size.height),
                     byTiling:false)
        
        return UIImage(cgImage: context.makeImage()!)

    }

    func convertToGrayImage() -> UIImage? {
        let width = self.size.width
        let height = self.size.height
        let rect = CGRect(x: 0.0, y: 0.0, width: width, height: height)
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)
        
        guard let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue) else {
            return nil
        }
        guard let cgImage = cgImage else { return nil }
        
        context.draw(cgImage, in: rect)
        guard let imageRef = context.makeImage() else { return nil }
        let newImage = UIImage(cgImage: imageRef.copy()!)
        
        return newImage
    }

}
