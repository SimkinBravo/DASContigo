//
//  BasicDoubleCellData.swift
//  lajornada
//
//  Created by Simkin Bravo on 30/11/16.
//  Copyright © 2016 La Jornada. All rights reserved.
//

import UIKit

class BasicDoubleCellData: BasicCellData {

    var a:BasicCellData!
    var b:BasicCellData!
    var aButton:BasicDataButton!
    var bButton:BasicDataButton!
    var selectedIndex:Int = 0

    override var indexPath: IndexPath! {
        get {
            return super.indexPath
        }
        set {
            super.indexPath = newValue
            self.a.indexPath = newValue
            if self.b != nil {
                self.b.indexPath = newValue
            }
        }
    }

    init (slug: String, maintitle: String, secondaryTitle: String, a: BasicCellData, b: BasicCellData!) {
        super.init(slug: slug, title: maintitle, subtitle: secondaryTitle)
        self.a = a
        self.b = b
        if b != nil && !b.images.isEmpty {
            self.images = [a.images.first!, b.images.first!]
        } else {
            self.images = [a.images.first!]
        }
        self.cellReuseID = BasicCellID.double
        self.cellSize = a.sectionTitle == "" ? BasicCellSize.doubleS : BasicCellSize.double
        aButton = BasicDataButton()
        bButton = BasicDataButton()
        aButton.data = a
        bButton.data = b
        aButton.parentData = self
        bButton.parentData = self
    }

    override func configCell(cell:UITableViewCell) -> UITableViewCell {
        let container = cell.contentView.viewWithTag(self.viewsTag)!
        container.frame = CGRect(origin:CGPoint(), size:cell.frame.size)
        setupDoubleCell(cell: cell)
        return cell;
    }

    func setupDoubleCell(cell:UITableViewCell) {
        cell.selectionStyle = .none
        let halfW = cellSize.width * 0.5
        let container = cell.contentView.viewWithTag(self.viewsTag)!
        container.subviews.forEach({ $0.removeFromSuperview() })
        _ = container.subviews.map({ $0.removeFromSuperview() })
        let viewA:UIView = UIView(frame: CGRect(x: 0, y: 0, width: b == nil ? cellSize.width : halfW, height: cellSize.height))
        viewA.backgroundColor = cell.backgroundColor
        viewA.tag = self.viewsTag + 1
        container.addSubview(viewA)
        a.images.first?.imageTag = self.imagesTag + 1
//        a.configViewCell(container: viewA)
        a.configColumnCell(container: viewA, atIndex: 0)
        aButton.frame = viewA.frame
        aButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        viewA.addSubview(aButton)
        if b != nil {
            let viewB:UIView = UIView(frame: CGRect(x: halfW, y: 0, width: halfW, height: cellSize.height))
            viewB.tag = self.viewsTag + 2
            viewB.backgroundColor = cell.backgroundColor
            container.addSubview(viewB)
            b.images.first?.imageTag = self.imagesTag + 2
//            b.configViewCell(container: viewB)
            b.configColumnCell(container: viewB, atIndex: 1)
            bButton.frame = viewA.frame
            bButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            viewB.addSubview(bButton)
        }
    }

    @objc func buttonAction(sender: UIButton!) {
        selectedIndex = aButton == sender ? 1 : 2
        updateViews()
    }

    func updateViews() {
        let viewA = aButton.superview
        let viewB = bButton.superview
        viewA?.backgroundColor = selectedIndex == 1 ? .gray : .white
        viewB?.backgroundColor = selectedIndex == 2 ? .gray : .white
    }

    func deselect() {
        selectedIndex = 0
        updateViews()
    }

    override func addSeparator(container: UIView) {
        container.viewWithTag(separatorTag)?.removeFromSuperview()
        let separator = UIView.init(frame: CGRect(x:marginA.x, y:container.frame.size.height - 1, width:container.frame.size.width - (marginA.x * 2), height:1))
        separator.tag = separatorTag
        separator.backgroundColor = UIColor.sectionT
        container.addSubview(separator)
    }

}
