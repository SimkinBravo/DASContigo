//
//  DASChatFViewController.swift
//  DAS
//
//  Created by Patricio Bravo Cisneros on 06/04/18.
//  Copyright © 2018 Last Code. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import AlamofireImage

class DASBasicTViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var activityIndicator:NVActivityIndicatorView!
    var activityIndicatorType:NVActivityIndicatorType = .ballClipRotate
    var activityIndicatorColor:UIColor = Theme.shared.colors.navigationBarColor3
    var activityLabel = UILabel()
    let tableView = UITableView()
    var elements:[[BasicCellData]] = []
    var tableContainer = UIView()
    var cellReusableId: [String: String] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.frame = self.view.frame
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        tableContainer.frame = self.view.frame
        tableContainer.addSubview(tableView)
        self.view.addSubview(tableContainer)
        self.setActivityIndicator()
        self.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.tableView.indexPathForSelectedRow != nil {
            self.tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
        }
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.elements.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.elements[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let element:BasicCellData = self.elements[indexPath.section][indexPath.row]
        return element.cellSize == nil ? 150 : element.cellSize.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = elements[indexPath.section][indexPath.row]
        if cellReusableId[data.cellId.rawValue] == nil {
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: data.cellId.rawValue)
            cellReusableId[data.cellId.rawValue] = data.cellId.rawValue
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: getCellReuseId(data), for: indexPath)
        cell.backgroundColor = self.tableView.backgroundColor
        cell.isUserInteractionEnabled = data.userInteractionEnabled
        let container = UIView(frame: cell.frame)
        container.tag = data.viewsTag
        cell.contentView.addSubview(container)
        data.indexPath = indexPath
        return data.configCell(cell: cell)
    }

    func getCellReuseId(_ data: BasicCellData) -> String {
        if data.cellId != nil {
            return data.cellId.rawValue
        }
        return data.cellReuseID
    }
    
    func isCellACollection(_ data: BasicCellData) -> Bool {
        if data.cellReuseID != nil {
            return data.cellReuseID == BasicCellID.collection
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let data:BasicCellData = elements[indexPath.section][indexPath.row]
        if data.images.count != 0 && !data.isImageHidden {
            for image in data.images {
                if image.imageTag != nil {
                    let imageContainer = cell.contentView.viewWithTag(image.imageTag) as! UIImageView
                    if imageContainer != nil && !image.thumbURL.isEmpty {
                        let url:URL = URL(string: image.thumbURL)!
                        self.loadImage(imageView: imageContainer, url:url, data:image, cellData: data)
                    }
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableViewDidSelectData(data: elements[indexPath.section][indexPath.row])
    }

    func tableViewDidSelectData(data:BasicCellData) {
        print("Select: ", data.indexPath!)
    }

    // MARK: - Custom funcs
    @objc func refresh(sender:AnyObject) {
        self.tableView.alpha = 0.5
        _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.loadData), userInfo: nil, repeats: false)
    }

    @objc func loadData() {
        showLoader()
    }

    func setActivityIndicator() {
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80), type: activityIndicatorType, color: activityIndicatorColor, padding: 0)
        activityIndicator.center = self.view.center
        activityIndicator.frame = CGRect(origin: CGPoint(x: activityIndicator.frame.origin.x, y: activityIndicator.frame.origin.y - (self.view.frame.height * 0.2)), size: activityIndicator.frame.size)
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        activityLabel.textAlignment = .center
        activityLabel.numberOfLines = 0
        activityLabel.lineBreakMode = .byWordWrapping
        activityLabel.textColor = activityIndicatorColor
        activityLabel.frame = CGRect(x: 50, y: activityIndicator.frame.origin.y + 100, width: self.view.frame.width - 100, height: 50)
        self.view.addSubview(activityLabel)
    }

    func onLoadImageResponse(imageVw: UIImageView, image: UIImage, imageData: BasicImageData, cellData: BasicCellData) {
        imageVw.backgroundColor = .clear
    }

    //let bannerView = DFPBannerView(adSize: kGADAdSizeSmartBannerPortrait)

    func loadImage(imageView:UIImageView, url:URL, data:BasicImageData, cellData: BasicCellData) {
        let currentPlaceholder: String = "placeholder"
        let placeholderImage = UIImage(named: currentPlaceholder)
        let currentContentMode = imageView.contentMode
        imageView.contentMode = .scaleAspectFit
        if !data.adjustImageViewSize {
            imageView.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: data.roundedCourners ? AspectScaledToFillSizeWithRoundedCornersFilter (size: imageView.frame.size, radius:6.0) : AspectScaledToFillSizeFilter (size: imageView.frame.size), imageTransition: .crossDissolve(0.3), completion: { response in
                imageView.contentMode = currentContentMode
                if response.result.error != nil {
                    print(response.result.error ?? "Sin mensaje de error")
                } else {
                    self.onLoadImageResponse(imageVw: imageView, image: response.result.value!, imageData: data, cellData: cellData)
                }
            })
        } else {
            let filter = RoundedCornersFilter (radius: 4.0)
            imageView.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.3),  completion: { response in
                self.onLoadImageResponse(imageVw: imageView, image: response.result.value!, imageData: data, cellData: cellData)
            })
        }
    }

    func loadDataFrom(url:String) {
        print("->", url)
        Alamofire.request(url).responseJSON { response in
            if response.result.value as? [AnyObject] != nil {
                self.setDataResponse(responseData: response.result.value as! [AnyObject])
            } else if response.result.value as AnyObject! != nil {
                self.setDataResponse(responseData: [response.result.value as AnyObject])
            } else {
                print(response)
                print(response.request ?? "")  // original URL request
                print(response.response ?? "") // HTTP URL response
                print(response.data ?? "")     // server data
                print(response.result)
            }
            self.onLoadDataResponse()
        }
    }

    func loadXMLFrom(url: String) {
        print("->", url)
        Alamofire.request(url).response { response in
            if let data = response.data {
                self.onLoadXMLResponse(response: data)
            }
        }
    }

    func showLoader(isHidden:Bool = false, text: String! = "") {
        if !isHidden {
            activityIndicator.startAnimating()
            activityLabel.text = text
        } else {
            activityIndicator.stopAnimating()
        }
        activityIndicator.isHidden = isHidden
        activityLabel.isHidden = isHidden
    }

    func setDataResponse(responseData:[AnyObject]) {
        self.elements.append(self.getParsedElements(array: responseData as! [[String : AnyObject]]))
    }

    func onLoadDataResponse() {
        self.showLoader(isHidden: true)
        self.tableView.alpha = 1.0
        self.tableView.reloadData()
    }
    
    func onLoadXMLResponse(response: Data) {
        self.showLoader(isHidden: true)
    }
    
    func getParsedElements(array:[[String:AnyObject]]) -> [BasicCellData] {
        var tempElements:[BasicCellData] = []
        for element in array {
            tempElements.append(self.getParsedElement(index: tempElements.count, element: element))
        }
        return tempElements
    }
    
    func getParsedElement(index:Int, element:[String:AnyObject]) -> BasicCellData {
        return BasicCellData.init(index: index, rawJSON: element)
    }

    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }

}
